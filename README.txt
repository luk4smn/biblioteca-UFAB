.:::BIBLIOTEA UFAB - SPRINT 3
================ INSTRU��ES ================

Como Executar o Projeto:

1) Crie uma nova conex�o no MySQL Workbench com o nome Biblioteca (Nome de Usu�rio: root / Senha: root)

2) Execute o arquivo "SQL BASE.sql" (localizado em "biblioteca-UFAB\src\main\resources") para criar as tabelas necess�rias. Com isso seu banco j� est� configurado e pronto para guardar as informa��es do projeto.

3) Importe o projeto para dentro do Eclipse com o comando import.

4) Ap�s a importa��o do projeto, clique com o bot�o direito na pasta do projeto dentro do Eclipse. Selecione o menu Maven > Update Project para adicionar as dependencias do maven ao projeto. Em seguida, pressione F5 para atualizar o projeto.

6) Clique com o bot�o direito no projeto > Run As > 5 Maven Build. Na janela que abrir digite "tomcat7:run" (sem as aspas) em Goals para dizer que iremos utilizar o tomcat7 como servidor de aplica��o. N�o precisa instalar o tomcat7, o maven faz isso automaticamente. Clique em run.

7) Pronto. O projeto j� est� rodando na porta 8080. Abra o navegador e na barra de endere�os digite: "localhost:8080" (sem as aspas).


============================================================================================

OBS1: Os arquivos referentes aos logs encontram-se na pasta Default do Sistema Operacional - Exemplo: "BIBLIOTECA UFAB\logs"

OBS2: As informa��es t�cnicas referentes ao Projeto, encontram-se na pasta: "biblioteca-UFAB\src\main\resources"

OBS3: O arquivo UML pode ser encontrado na pasta: "biblioteca-UFAB\UML"

OBS4: As configura��es do sistema de envio para e-mails est� na pasta: "biblioteca-UFAB\src\main\resources\env"
