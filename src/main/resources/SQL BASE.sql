UNLOCK TABLES;

drop database if exists biblioteca;

create database if not exists biblioteca;

use biblioteca;

CREATE TABLE cursos (
	id int NOT NULL AUTO_INCREMENT,
    nome varchar(255),
    area varchar(255),
    tipo varchar(255),
    PRIMARY KEY (id)
);

CREATE TABLE alunos (
	id int NOT NULL AUTO_INCREMENT,
	matricula varchar(255),
    cpf varchar(255),
    rg varchar(255),
    naturalidade varchar(255),
    nome varchar(255),
    nomemae varchar(255),
    endereco varchar(255),
    telefone varchar(255),
    curso varchar(255),
    ano varchar(255),
    periodo varchar(255),
    nivel varchar(255),
    email varchar(255),
    PRIMARY KEY (id)
);

CREATE TABLE usuarios (
	id int NOT NULL AUTO_INCREMENT,
    usuario varchar(255) NOT NULL,
    senha varchar(255),
    dado_de_referencia_id int,
    role int,
    PRIMARY KEY (id)
);

CREATE TABLE funcionarios (
	id int NOT NULL AUTO_INCREMENT,
    cpf varchar(255),
    rg varchar(255),
    naturalidade varchar(255),
    nome varchar(255),
    nomemae varchar(255),
    endereco varchar(255),
    cargo varchar(255),
    PRIMARY KEY (id)
);

CREATE TABLE `anais_de_congresso` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(40) NOT NULL,
  `edicao` varchar(255) DEFAULT NULL,
  `autores` varchar(255) NOT NULL,
  `orientadores` varchar(255) NOT NULL,
  `nome_congresso` varchar(255) NOT NULL,
  `local` varchar(255) NOT NULL,
  `ano_publicacao` int(255) NOT NULL,
  `tipo_anal` int(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `livros` (
  `id` int NOT NULL AUTO_INCREMENT,
  `editora` varchar(255) NOT NULL,
  `ISBN` varchar(255) NOT NULL,
  `autores` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `edicao` varchar(255) NOT NULL,
  `ano_publicacao` int(20) NOT NULL,
  `numero_paginas` int(20) NOT NULL,
  `area_conhecimento` int(4) NOT NULL,
  `tipo_tema` int(10) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `jornais` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `edicao` varchar(255) NOT NULL,
  `data_publicacao` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `midias_eletronicas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_de_gravacao` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `tipo_de_midia` int(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `revistas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `data_publicacao` varchar(255) NOT NULL,
  `edicao` varchar(255) NOT NULL,
  `editora` varchar(255) NOT NULL,
  `numero_paginas` int(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `trabalhos_de_conclusao` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ano_defesa` int(4) NOT NULL,
  `tipoTcc` int(4) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `edicao` varchar(10) NOT NULL,
  `autores` varchar(255) NOT NULL,
  `orientadores` varchar(255) NOT NULL,
  `local` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `emprestimos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `aluno_id` int(4) NOT NULL,
  `tipo_item` int(4),
  `item_id` int(4) NOT NULL,
  `entregue` int(4) default 0,
  `data_retirada` varchar(255) NOT NULL,
  `data_devolucao` varchar(255) NOT NULL, 
  PRIMARY KEY (`id`)
);

CREATE TABLE `reservas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `aluno_id` int(4) NOT NULL,
  `tipo_item` int(4),
  `item_id` int(4) NOT NULL,
  `sinalizar` int(4) default 0,
  PRIMARY KEY (`id`)
);

LOCK TABLES `alunos` WRITE;
/*!40000 ALTER TABLE `alunos` DISABLE KEYS */;
INSERT INTO `alunos` VALUES (1,'GCP-101000','02618984501','1560211539','Jacobina','Lucas Martins Nunes','Jucileide Cavalcante Martind','Rua Gilo Guedes, 17','(83) 98606-8698','Ciência da Computação','2010','1','G','lucas.mc.martins@gmail.com'),(2,'GVE-153000','14563278901','32132132131','Campina Grande','Aline Pereira Silva','Rosangela Maria Silva','José Pereira','(83) 3621-1460','Veterinária','2015','3','G',null),(3,'MCP-171000','14895670114','549586','Curralinho','Adolfo Abrantes Moura','Josenilda Moura','Pedro Abrantes','(86) 99857-6663','Ciência da Computação','2017','1','M',null);
/*!40000 ALTER TABLE `alunos` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `cursos` WRITE;
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT INTO `cursos` VALUES (1,'Administração','Ciencias Humanas','Graduação'),(2,'Veterinária','Ciencias Da Natureza','Graduação'),(3,'Ciência da Computação','Ciencias Exatas','Graduação'),(4,'Direito','Ciencias Humanas','Graduação'),(5,'Engenharia Elétrica','Ciencias Exatas','Graduação'),(6,'Engenharia Espacial','Ciencias Exatas','Graduação'),(7,'Engenharia Mecatrônica','Ciencias Exatas','Graduação'),(8,'Matemática','Ciencias Exatas','Graduação'),(9,'Medicina','Ciencias Da Natureza','Graduação'),(10,'Nutrição','Ciencias Da Natureza','Graduação'),(11,'Odontologia','Ciencias Da Natureza','Graduação'),(12,'Psicologia','Ciencias Da Natureza','Graduação'),(13,'Ciência da Computação','Ciencias Exatas','Pós-Graduação'),(14,'Medicina','Ciencias Da Natureza','Pós-Graduação');
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `emprestimos` WRITE;
/*!40000 ALTER TABLE `emprestimos` DISABLE KEYS */;
INSERT INTO `emprestimos` VALUES (1,1,1,3,0,'2018-05-17','2018-05-18');
/*!40000 ALTER TABLE `emprestimos` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `funcionarios` WRITE;
/*!40000 ALTER TABLE `funcionarios` DISABLE KEYS */;
INSERT INTO `funcionarios` VALUES (1,'1254522301','55685656','Lagoa Seca','Maria Wiston Silva','Josefina Winston','Rua Pres. João Pessoa, 145','Agente Bibliotecária');
/*!40000 ALTER TABLE `funcionarios` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `jornais` WRITE;
/*!40000 ALTER TABLE `jornais` DISABLE KEYS */;
INSERT INTO `jornais` VALUES (1,'JÁ','55','2000-10-15'),(2,'JORNAL DA PARAÍBA','105','2000-12-29'),(3,'CORREIS','100','2000-01-15'),(4,'TUDO É NOTÍCIA','20','2000-01-10'),(5,'EU QUERO SABER ','19','2011-12-18'),(6,'JÁ SABIA','21','2000-05-25'),(7,'BRASIL ','39','2000-01-19'),(8,'BRA URGENTE ','49 ','1999-05-04'),(9,'FALANDO COM VOCÊ','30','2011-11-11'),(10,'BOM DIA BRASIL','200','2005-04-30');
/*!40000 ALTER TABLE `jornais` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `livros` WRITE;
/*!40000 ALTER TABLE `livros` DISABLE KEYS */;
INSERT INTO `livros` VALUES (1,'IEEE','HG338920','AF. G E AL. CA','Hoje é o Dia','1',2000,709,3,12),(2,'IEEE','ERFO4982748','AF. G E AL. CA','Algoritimos','1º',2000,900,4,3),(3,'IEEE','IEIIO444I9','AF. G E AL. CA','Algoritmos 2 ','2º',2000,598,4,3),(4,'GHS3D','ERFO4982748','AH.JR PPT HY FAY','D2 MATEMATICA','2º',2000,675,1,1),(5,'GHS3D','FKAJFBNNM22','Desconhecidos ','SÓ HOJE ','1ª',2018,15,2,5),(6,'OIKGJE','RTJGJJLLF4867','SSSYOLSR','AC COMP','2º',2000,500,1,1),(7,'Charles Bukowski','ERFO4982748','Charles Bukowski','Amor é tudo que nós dissemos que não era','2',2000,4000,3,12),(8,'GHS3D','HG338920','António Lobo Antunes','Ontem não te vi em Babilónia','3',20000,300,3,12),(9,'Vintages Books-UK','ERFO4982748','Andrew Hodges',' Alan Turing: The Enigma','2',1999,444,1,1),(10,'Plume','ERFO4982748','Tobias Dantzig','Number: the language of Science','3',2000,446,1,1),(11,'GHS3D','HG338920','Steven Johnson','De onde vêm as boas ideias','3',2009,345,2,8);
/*!40000 ALTER TABLE `livros` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `midias_eletronicas` WRITE;
/*!40000 ALTER TABLE `midias_eletronicas` DISABLE KEYS */;
INSERT INTO `midias_eletronicas` VALUES (1,'2000-12-18','BOM D+',1),(2,'2000-12-12','AGORA',2),(3,'2000-12-19','É',1),(4,'2000-10-10','AGORA SÓ ',2),(5,'2013-12-18','FALTA VOCÊ',1),(6,'2009-10-10','FATAL',2);
/*!40000 ALTER TABLE `midias_eletronicas` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `revistas` WRITE;
/*!40000 ALTER TABLE `revistas` DISABLE KEYS */;
INSERT INTO `revistas` VALUES (1,'2000-08-18','1ª','TV PP',25,'TV MANIA'),(2,'1999-12-13','1','PAGMMM',17,'Hoje é o Dia'),(3,'2009-03-31','2','OLB',30,'ENCONTRO FT '),(4,'2018-03-17','5','MOTOS HND',26,'MOTOS ES'),(5,'2010-05-10','55','SBT',30,'SBT INF'),(6,'1999-10-10','1','Veja ',34,'Veja '),(7,'1999-10-10','417.798','Época ',34,'Época '),(8,'1998-12-19','344.273','IstoÉ ',55,'IstoÉ '),(9,'2000-01-23','2','Caras ',65,'Caras '),(10,'2010-10-28','449494','Viva Mais',44,'Viva Mais');
/*!40000 ALTER TABLE `revistas` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `trabalhos_de_conclusao` WRITE;
/*!40000 ALTER TABLE `trabalhos_de_conclusao` DISABLE KEYS */;
INSERT INTO `trabalhos_de_conclusao` VALUES (1,2000,3,'PRONTO','1','DD','AD','UEFF'),(2,2005,1,'FEITO ','1','AD','EALT','UFCR'),(3,2005,3,'THE END','2','HANE','HEYL','UNIPA'),(4,2005,2,'ANIMAIS','1','ZOOL','LOGC','FLL'),(5,2001,3,'BICHO ANIMAL','2','VETER','ADA','UEBF');
/*!40000 ALTER TABLE `trabalhos_de_conclusao` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'lucas','2005',1,3),(2,'maria','123',1,2),(3,'root','321',0,1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
