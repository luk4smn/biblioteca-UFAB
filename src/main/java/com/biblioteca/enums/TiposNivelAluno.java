package com.biblioteca.enums;

public enum TiposNivelAluno {

	GRADUACAO("G"),
	ESPECIALIZACAO("E"),
	MESTRADO("M"),
	DOUTORADO("D")	;
	  
	  private String value;
	  
	  TiposNivelAluno(String value) {
	        this.value = value;
	    }
	 
	    public String getString() {
	        return value;
	    }
	    
	
}
