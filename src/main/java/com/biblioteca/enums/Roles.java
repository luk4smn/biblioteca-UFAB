package com.biblioteca.enums;

/**
 * Class Roles: Define os perfis de usu�rio.
 * 
 * @author Lucas Nunes, Lanmark Rafael, Higor Pereira
 * 
 * @param Valor inteiro 1 para administrador; Valor inteiro 2 para funcionario; Valor inteiro 3 para aluno;
 * 
 * @return Valor (Role do usu�rio).
 */



public enum Roles {
		
	ADMIN(1), FUNCIONARIO(2), ALUNO(3);
    
    private final int valor;
    
    Roles(int valorOpcao){
        valor = valorOpcao;
    }
    
    public int getValor(){
        return valor;
    }

}
