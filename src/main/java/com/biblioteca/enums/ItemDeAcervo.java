package com.biblioteca.enums;

import java.util.ArrayList;

public enum ItemDeAcervo {
	
	LIVRO(1),
	REVISTA(2),
	JORNAL(3),
	ANAL_DE_CONGRESSO(4),
	TCC(5),
	MIDIA_ELETRONICA(6);
    
    private final int valor;
       
    ItemDeAcervo(int valorOpcao){
        valor = valorOpcao;
    }
    
    public int getValor(){
        return valor;
    }
    
    public static ArrayList<Object> getTipos(){
    	
    	ArrayList<Object> tipos = new ArrayList<Object>();

    	tipos.add(new Object[] {	ItemDeAcervo.LIVRO.getValor(), "LIVRO"	});
    	tipos.add(new Object[] {	ItemDeAcervo.REVISTA.getValor(), "REVISTA"	});
    	tipos.add(new Object[] {	ItemDeAcervo.JORNAL.getValor(), "JORNAL"	});
    	tipos.add(new Object[] {	ItemDeAcervo.ANAL_DE_CONGRESSO.getValor(), "ANAL DE CONGRESSO"	});
    	tipos.add(new Object[] {	ItemDeAcervo.TCC.getValor(), "TCC"	});
    	tipos.add(new Object[] {	ItemDeAcervo.MIDIA_ELETRONICA.getValor(), "MIDIA ELETROCICA"	});
    	
			
		return tipos;
    }

}
