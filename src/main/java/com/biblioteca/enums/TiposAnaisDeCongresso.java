package com.biblioteca.enums;

/**
 * Class TiposAnaisDeCongresso: Cria e defini os tipos de anais de congresso que irao compor a biblioteca.
 * 
 * @author Lucas Nunes, Lanmark Rafael, Higor Pereira
 * 
 * @param Valor inteiro 1 para Artigo; Valor inteiro 2 para Poster; Valor inteiro 3 para Resumo.
 * 
 * @return Valor (Tipo dos Anais).
 */
public enum TiposAnaisDeCongresso {
	
	ARTIGO(1), POSTER(2), RESUMO(3);
    
    private final int valor;
    
    TiposAnaisDeCongresso(int valorOpcao){
        valor = valorOpcao;
    }
    public int getValor(){
        return valor;
    }
}