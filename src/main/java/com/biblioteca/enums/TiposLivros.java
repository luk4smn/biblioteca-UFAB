package com.biblioteca.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * Class TiposLivros: Cria e defini os temas e subtemas dos livros que irao compor a biblioteca.
 * 
 * @author Lucas Nunes, Lanmark Rafael, Higor Pereira
 * 
 * @param Tema: Valor inteiro 1 para Ciencias Exatas; Valor inteiro 2 para Ciencias da Natureza; Valor inteiro 3 para Humanas;
 * Valor inteiro 4 para Romance;
 * 
 * @param Subtema: Valor inteiro 1 para Teorema dos Grafos; Valor inteiro 2 para Anatomia; Valor inteiro 3 para Cultivo de Plantas;
 * Valor inteiro 4 para Drama;
 * 
 * @return Valor (Tema e Subtema).
 */
public enum TiposLivros {
	
	//TEMAS
	CIENCIAS_EXATAS(1), 
	CIENCIAS_DA_NATUREZA(2), 
	CIENCIAS_HUMANAS(3), 
	LINGUAGENS_CODIGOS(4),
	
	//SUBTEMAS
	MATEMATICA(1),
	ESTATISTICA(2),
	COMPUTACAO(3),
	ENGENHARIAS(4),
	BIOLOGIA(5),
	FISICA(6),
	QUIMICA(7),
	FILOSOFIA(8),
	SOCIOLOGIA(9),
	HISTORIA(10),
	GEOGRAFIA(11),
	LITERATURA(12),
	LINGUA_ESTRANGEIRA(13),
	GRAMATICA(14),
	NORMAS_TECNICAS(15);	
    
    private final int valor;
    
    TiposLivros(int valorOpcao){
        valor = valorOpcao;
    }
    
    public int getValor(){
        return valor;
    }
    
    public static List<Object> listTemas() {
		
		ArrayList<Object> temas = new ArrayList<Object>();

		temas.add(new Object[] {	TiposLivros.CIENCIAS_EXATAS.getValor(), "CIENCIAS EXATAS"	});
		temas.add(new Object[] {	TiposLivros.CIENCIAS_DA_NATUREZA.getValor(), "CIENCIAS DA NATUREZA"	});
		temas.add(new Object[] {	TiposLivros.CIENCIAS_HUMANAS.getValor(), "CIENCIAS HUMANAS"	});
		temas.add(new Object[] {	TiposLivros.LINGUAGENS_CODIGOS.getValor(), "LINGUAGENS E CODIGOS"	});
			
		return temas;
	}
	
	public static List<Object> listSubtemas() {
		
		ArrayList<Object> subtemas = new ArrayList<Object>();

		subtemas.add(new Object[] {	TiposLivros.MATEMATICA.getValor(), "MATEMATICA"	});
		subtemas.add(new Object[] {	TiposLivros.ESTATISTICA.getValor(), "ESTATISTICA"	});
		subtemas.add(new Object[] {	TiposLivros.COMPUTACAO.getValor(), "COMPUTACAO"	});
		subtemas.add(new Object[] {	TiposLivros.ENGENHARIAS.getValor(), "ENGENHARIAS"	});
		subtemas.add(new Object[] {	TiposLivros.BIOLOGIA.getValor(), "BIOLOGIA"	});
		subtemas.add(new Object[] {	TiposLivros.FISICA.getValor(), "FISICA"	});
		subtemas.add(new Object[] {	TiposLivros.QUIMICA.getValor(), "QUIMICA"	});
		subtemas.add(new Object[] {	TiposLivros.FILOSOFIA.getValor(), "FILOSOFIA"	});
		subtemas.add(new Object[] {	TiposLivros.SOCIOLOGIA.getValor(), "SOCIOLOGIA"	});
		subtemas.add(new Object[] {	TiposLivros.HISTORIA.getValor(), "HISTORIA"	});
		subtemas.add(new Object[] {	TiposLivros.GEOGRAFIA.getValor(), "GEOGRAFIA"	});
		subtemas.add(new Object[] {	TiposLivros.LITERATURA.getValor(), "LITERATURA"	});
		subtemas.add(new Object[] {	TiposLivros.LINGUA_ESTRANGEIRA.getValor(), "LINGUA_ESTRANGEIRA"	});
		subtemas.add(new Object[] {	TiposLivros.GRAMATICA.getValor(), "GRAMATICA"	});
		subtemas.add(new Object[] {	TiposLivros.NORMAS_TECNICAS.getValor(), "NORMAS TECNICAS"	});
			
		return subtemas;
	}
}