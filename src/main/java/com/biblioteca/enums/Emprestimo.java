package com.biblioteca.enums;

public enum Emprestimo {
	ENTREGUE(1), NAO_ENTREGUE(0);
    
    private final int valor;
    
    Emprestimo(int valorOpcao){
        valor = valorOpcao;
    }
    
    public int getValor(){
        return valor;
    }
}
