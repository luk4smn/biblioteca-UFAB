package com.biblioteca.enums;

/**
 * Class TipoMidiaEletronica: Cria e defini os tipos de midias eletronicas que irao compor a biblioteca.
 * 
 * @author Lucas Nunes, Lanmark Rafael, Higor Pereira
 * 
 * @param Valor inteiro 1 para CD; Valor inteiro 2 para DVD.
 * 
 * @return Valor (Tipo de M�dia).
 */

public enum TipoMidiaEletronica {
	
	CD(1), DVD(2);
    
    private final int valor;
    
    TipoMidiaEletronica(int valorOpcao){
        valor = valorOpcao;
    }
    public int getValor(){
        return valor;
    }
}