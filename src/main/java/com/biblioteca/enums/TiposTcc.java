package com.biblioteca.enums;

/**
 * Class TiposTcc: Cria e defini os tipos de TCC que irao compor a biblioteca.
 * 
 * @author Lucas Nunes, Lanmark Rafael, Higor Pereira
 * 
 * @param Valor inteiro 1 para Monografia; Valor inteiro 2 para Tese; Valor inteiro 3 para Dissertacao;
 * 
 * @return Valor (Tipo do TCC).
 */
public enum TiposTcc {
	
	MONOGRAFIA(1), TESE(2), DISSERTACAO(3);
    
    private final int valor;
    
    TiposTcc(int valorOpcao){
        valor = valorOpcao;
    }
    public int getValor(){
        return valor;
    }
}