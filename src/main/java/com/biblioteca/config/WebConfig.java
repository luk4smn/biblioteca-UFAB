/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */
package com.biblioteca.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/***
* 
* @author Lucas Martins, Lanmark Rafael, Higor Pereira
*
**/


@EnableWebMvc
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	/***
	 * Metodo responsavel pela importação de arquivos do webjars (bootstrap, jquery etc..) do SpringMVC
	 * 
	 * @return Registro do /classpath/
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		if (!registry.hasMappingForPattern("/webjars/**")) {
			registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
		}
		registry.setOrder(1);
	}
}