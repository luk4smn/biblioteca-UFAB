package com.biblioteca.service.reservas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.dao.reservas.ReservasDAO;
import com.biblioteca.model.Reservas;


@Service
public class ReservasServiceImpl implements ReservasService {

	@Autowired
	private ReservasDAO reservasDAO;
	
	@Transactional
	public Reservas getReserva(int id) {
		return reservasDAO.getReserva(id);
	}

	@Transactional
	public void create(Reservas reserva) {
		reservasDAO.create(reserva);
	}

	@Transactional
	public void update(Reservas reserva) {
		reservasDAO.update(reserva);
	}

	@Transactional
	public void delete(int id) {
		reservasDAO.delete(id);
	}
	
	@Transactional
	public List<Reservas> listAll() {
		return reservasDAO.listAll();
	}

	@Override
	public Reservas getFirstReserva(int tipoItem, int itemId) {
		return reservasDAO.getFirstReserva(tipoItem, itemId);
	}

	
}
