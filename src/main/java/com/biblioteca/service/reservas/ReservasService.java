package com.biblioteca.service.reservas;

import java.util.List;

import com.biblioteca.model.Reservas;

public interface ReservasService {
	public Reservas getReserva(int id);
	public Reservas getFirstReserva(int tipoItem, int itemId);
	public List<Reservas> listAll();
	public void create(Reservas reserva);
	public void update(Reservas reserva);
	public void delete(int id);
}
