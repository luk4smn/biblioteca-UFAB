package com.biblioteca.service.usuario;

import java.util.List;

import com.biblioteca.model.Aluno;
import com.biblioteca.model.Funcionario;
import com.biblioteca.model.Usuario;

public interface UsuarioService {
	
	public Usuario getUser(String userName);
	public Usuario getUserEdit(int id);
	public List<Usuario> listAll();
	public List<Funcionario> listFuncionarioWithoutUser();
	public List<Aluno> listAlunoWithoutUser();
	public void create(Usuario usuario);
	public void update(Usuario usuario);
	public void delete(int id);
	
}
