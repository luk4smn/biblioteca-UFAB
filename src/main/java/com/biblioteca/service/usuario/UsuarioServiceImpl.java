package com.biblioteca.service.usuario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.dao.usuario.UsuarioDAO;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Funcionario;
import com.biblioteca.model.Usuario;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioDAO userDAO;
	
	@Transactional
	public Usuario getUser(String userName) {
		return userDAO.getUser(userName);
	}

	@Transactional
	public List<Usuario> listAll() {
		return userDAO.listUsers();
	}

	@Transactional
	public void create(Usuario usuario) {
		userDAO.create(usuario);
		
	}

	@Transactional
	public void update(Usuario usuario) {
		userDAO.update(usuario);
		
	}

	@Transactional
	public void delete(int id) {
		userDAO.delete(id);
	}

	@Transactional
	public List<Funcionario> listFuncionarioWithoutUser() {
		return userDAO.listFuncionarioWithoutUser();
	}

	@Transactional
	public List<Aluno> listAlunoWithoutUser() {
		return userDAO.listAlunoWithoutUser();
	}

	@Transactional
	public Usuario getUserEdit(int id) {
		return userDAO.getUserEdit(id);
	}

}
