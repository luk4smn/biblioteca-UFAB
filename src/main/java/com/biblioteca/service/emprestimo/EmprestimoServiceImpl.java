package com.biblioteca.service.emprestimo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.dao.emprestimo.EmprestimoDAO;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Emprestimos;


@Service
public class EmprestimoServiceImpl implements EmprestimoService {

	@Autowired
	private EmprestimoDAO emprestimoDAO;
	
	@Transactional
	public Emprestimos getEmprestimo(int id) {
		return emprestimoDAO.getEmprestimo(id);
	}

	@Transactional
	public void create(Emprestimos emprestimo) {
		emprestimoDAO.create(emprestimo);
	}

	@Transactional
	public void update(Emprestimos emprestimo) {
		emprestimoDAO.update(emprestimo);
	}

	@Transactional
	public void delete(int id) {
		emprestimoDAO.delete(id);
	}
	
	@Transactional
	public List<Emprestimos> listAll() {
		return emprestimoDAO.listAll();
	}

	@Transactional
	public List<Emprestimos> listEmprestimoEmAberto() {
		return emprestimoDAO.listEmprestimoEmAberto();
	}

	@Transactional
	public List<Aluno> listAlunoWithoutPendences() {
		return emprestimoDAO.listAlunoWithoutPendences();
	}

	@Override
	public List<Object> calculatePendences() {
		return emprestimoDAO.calculatePendences();
	}

}
