package com.biblioteca.service.emprestimo;

import java.util.List;

import com.biblioteca.model.Aluno;
import com.biblioteca.model.Emprestimos;

public interface EmprestimoService {
	public Emprestimos getEmprestimo(int id);
	public List<Emprestimos> listEmprestimoEmAberto();
	public List<Emprestimos> listAll();
	public List<Aluno> listAlunoWithoutPendences();
	public List<Object> calculatePendences();
	public void create(Emprestimos emprestimo);
	public void update(Emprestimos emprestimo);
	public void delete(int id);
}
