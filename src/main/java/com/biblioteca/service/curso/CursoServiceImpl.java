package com.biblioteca.service.curso;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.dao.curso.CursoDAO;
import com.biblioteca.model.Curso;

@Service
public class CursoServiceImpl implements CursoService {

	@Autowired
	   private CursoDAO cursoDAO;
	
	@Transactional
	public Curso getCurso(int id) {
		return cursoDAO.getCurso(id);
	}

	@Transactional
	public List<Curso> listAll() {
		return cursoDAO.listAll();
	}

	@Transactional
	public void create(Curso curso) {
		cursoDAO.create(curso);
		
	}

	@Transactional
	public void update(Curso curso) {
		cursoDAO.update(curso);
		
	}

	@Transactional
	public void delete(int id) {
		cursoDAO.delete(id);
		
	}

	@Transactional
	public Curso validate(String nome, String tipo) {
		return cursoDAO.validate(nome, tipo);
		 
	}

}
