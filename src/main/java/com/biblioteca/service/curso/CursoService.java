package com.biblioteca.service.curso;

import java.util.List;

import com.biblioteca.model.Curso;

public interface CursoService {
	public Curso getCurso(int id);
	public Curso validate(String nome, String tipo);
	public List<Curso> listAll();
	public void create(Curso curso);
	public void update(Curso curso);
	public void delete(int id);
}
