package com.biblioteca.service.aluno;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.dao.aluno.AlunoDAO;
import com.biblioteca.model.Aluno;

@Service
public class AlunoServiceImpl implements AlunoService {
	
	@Autowired
	private AlunoDAO alunoDAO;

	@Transactional
	public Aluno getAluno(int id) {
		return alunoDAO.getAluno(id);
	}

	@Transactional
	public List<Aluno> listAll() {
		return alunoDAO.listAll();
	}

	@Transactional
	public void create(Aluno aluno) {
		alunoDAO.create(aluno);	
	}

	@Transactional
	public void update(Aluno aluno) {
		alunoDAO.update(aluno);	
		
	}

	@Transactional
	public void delete(int id) {
		alunoDAO.delete(id);	
		
	}

	@Transactional
	public int count() {
		return alunoDAO.count();
	}
	
	@Transactional
	public Aluno validate(String cpf) {
		return alunoDAO.validate(cpf);
		 
	}

}
