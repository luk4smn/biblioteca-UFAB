package com.biblioteca.service.aluno;

import java.util.List;

import com.biblioteca.model.Aluno;
import com.biblioteca.model.Funcionario;

public interface AlunoService {
	
	public Aluno getAluno(int id);
	public List<Aluno> listAll();
	public void create(Aluno aluno);
	public void update(Aluno aluno);
	public void delete(int id);
	public int count();
	public Aluno validate(String cpf);
	
}
