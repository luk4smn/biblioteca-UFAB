package com.biblioteca.service.acervo.tccs;

import java.util.List;

import com.biblioteca.model.TrabalhosDeConclusao;

public interface TCCsService {
	
	public TrabalhosDeConclusao getTcc(int id);
	public List<TrabalhosDeConclusao> listAll();
	public void create(TrabalhosDeConclusao tcc);
	public void update(TrabalhosDeConclusao tcc);
	public void delete(int id);

}
