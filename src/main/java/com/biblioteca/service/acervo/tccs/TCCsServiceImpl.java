package com.biblioteca.service.acervo.tccs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import  com.biblioteca.dao.acervo.tccs.TCCsDAO;

import com.biblioteca.model.TrabalhosDeConclusao;


@Service
public class TCCsServiceImpl implements TCCsService {

	@Autowired
	   private TCCsDAO TCCsDAO;
	
	@Transactional
	public TrabalhosDeConclusao getTcc(int id) {
		return TCCsDAO.getTcc(id);
	}

	@Transactional
	public List<TrabalhosDeConclusao> listAll() {
		return TCCsDAO.listAll();
	}

	@Transactional
	public void create(TrabalhosDeConclusao tcc) {
		TCCsDAO.create(tcc);
		
	}

	@Transactional
	public void update(TrabalhosDeConclusao tcc) {
		TCCsDAO.update(tcc);
	}

	@Transactional
	public void delete(int id) {
		TCCsDAO.delete(id);
		
	}


}
