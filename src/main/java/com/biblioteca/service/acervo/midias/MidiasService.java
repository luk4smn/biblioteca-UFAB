package com.biblioteca.service.acervo.midias;

import java.util.List;

import com.biblioteca.model.MidiasEletronicas;

public interface MidiasService {
	
	public MidiasEletronicas getMidia(int id);
	public List<MidiasEletronicas> listAll();
	public void create(MidiasEletronicas midia);
	public void update(MidiasEletronicas midia);
	public void delete(int id);

}
