package com.biblioteca.service.acervo.midias;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.dao.acervo.midias.MidiasDAO;
import com.biblioteca.model.MidiasEletronicas;


@Service
public class MidiasServiceImpl implements MidiasService {

	@Autowired
	   private MidiasDAO midiasDAO;
	
	@Transactional
	public MidiasEletronicas getMidia(int id) {
		return midiasDAO.getMidia(id);
	}

	@Transactional
	public List<MidiasEletronicas> listAll() {
		return midiasDAO.listAll();
	}

	@Transactional
	public void create(MidiasEletronicas midia) {
		midiasDAO.create(midia);
		
	}

	@Transactional
	public void update(MidiasEletronicas midia) {
		midiasDAO.update(midia);
	
	}

	@Transactional
	public void delete(int id) {
		midiasDAO.delete(id);
		
	}


}
