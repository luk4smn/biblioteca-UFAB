package com.biblioteca.service.acervo.jornais;

import java.util.List;

import com.biblioteca.model.Jornais;

public interface JornaisService {
	

	public Jornais getJornal(int id);
	public List<Jornais> listAll();
	public void create(Jornais jornal);
	public void update(Jornais jornal);
	public void delete(int id);

}
