package com.biblioteca.service.acervo.jornais;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.dao.acervo.jornais.JornaisDAO;
import com.biblioteca.model.Jornais;

@Service
public class JornaisServiceImpl implements JornaisService {

	@Autowired
	   private JornaisDAO livroDAO;
	
	@Transactional
	public Jornais getJornal(int id) {
		return livroDAO.getJornal(id);
	}

	@Transactional
	public List<Jornais> listAll() {
		return livroDAO.listAll();
	}

	@Transactional
	public void create(Jornais jornal) {
		livroDAO.create(jornal);
		
	}

	@Transactional
	public void update(Jornais jornal) {
		livroDAO.update(jornal);
	
	}

	@Transactional
	public void delete(int id) {
		livroDAO.delete(id);
		
	}


}
