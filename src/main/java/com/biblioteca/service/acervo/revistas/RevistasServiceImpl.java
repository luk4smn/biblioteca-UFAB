package com.biblioteca.service.acervo.revistas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.dao.acervo.revistas.RevistasDAO;
import com.biblioteca.model.Revistas;


@Service
public class RevistasServiceImpl implements RevistasService {

	@Autowired
	   private RevistasDAO revistaDAO;
	
	@Transactional
	public Revistas getRevista(int id) {
		return revistaDAO.getRevista(id);
	}

	@Transactional
	public List<Revistas> listAll() {
		return revistaDAO.listAll();
	}

	@Transactional
	public void create(Revistas revista) {
		revistaDAO.create(revista);
		
	}

	@Transactional
	public void update(Revistas revista) {
		revistaDAO.update(revista);
	
	}

	@Transactional
	public void delete(int id) {
		revistaDAO.delete(id);
		
	}


}
