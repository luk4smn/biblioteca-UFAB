package com.biblioteca.service.acervo.revistas;

import java.util.List;

import com.biblioteca.model.Revistas;

public interface RevistasService {
	
	public Revistas getRevista(int id);
	public List<Revistas> listAll();
	public void create(Revistas livro);
	public void update(Revistas livro);
	public void delete(int id);

}
