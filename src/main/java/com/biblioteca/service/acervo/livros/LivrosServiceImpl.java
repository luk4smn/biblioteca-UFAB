package com.biblioteca.service.acervo.livros;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.dao.acervo.livros.LivrosDAO;
import com.biblioteca.model.Livros;


@Service
public class LivrosServiceImpl implements LivrosService {

	@Autowired
	   private LivrosDAO livroDAO;
	
	@Transactional
	public Livros getLivro(int id) {
		return livroDAO.getLivro(id);
	}

	@Transactional
	public List<Livros> listAll() {
		return livroDAO.listAll();
	}

	@Transactional
	public void create(Livros livro) {
		livroDAO.create(livro);
		
	}

	@Transactional
	public void update(Livros livro) {
		livroDAO.update(livro);
	
	}

	@Transactional
	public void delete(int id) {
		livroDAO.delete(id);
		
	}


}
