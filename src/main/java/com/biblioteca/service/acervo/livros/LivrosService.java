package com.biblioteca.service.acervo.livros;

import java.util.List;

import com.biblioteca.model.Livros;

public interface LivrosService {
	
	public Livros getLivro(int id);
	public List<Livros> listAll();
	public void create(Livros livro);
	public void update(Livros livro);
	public void delete(int id);

}
