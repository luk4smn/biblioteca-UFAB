package com.biblioteca.service.acervo.anais;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.dao.acervo.anais.AnaisDeCongressoDAO;
import com.biblioteca.model.AnaisDeCongresso;

@Service
public class AnaisDeCongressoServiceImpl implements AnaisDeCongressoService {

	@Autowired
	   private AnaisDeCongressoDAO anaisDAO;
	
	@Transactional
	public AnaisDeCongresso getAnal(int id) {
		return anaisDAO.getAnal(id);
	}

	@Transactional
	public List<AnaisDeCongresso> listAll() {
		return anaisDAO.listAll();
	}

	@Transactional
	public void create(AnaisDeCongresso anal) {
		anaisDAO.create(anal);
		
	}

	@Transactional
	public void update(AnaisDeCongresso anal) {
		anaisDAO.update(anal);
		
	}

	@Transactional
	public void delete(int id) {
		anaisDAO.delete(id);
		
	}

}
