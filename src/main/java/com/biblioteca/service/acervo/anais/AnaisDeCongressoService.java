package com.biblioteca.service.acervo.anais;

import java.util.List;

import com.biblioteca.model.AnaisDeCongresso;

public interface AnaisDeCongressoService {
	
	public AnaisDeCongresso getAnal(int id);
	public List<AnaisDeCongresso> listAll();
	public void create(AnaisDeCongresso anal);
	public void update(AnaisDeCongresso anal);
	public void delete(int id);

}
