package com.biblioteca.service.funcionario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.dao.funcionario.FuncionarioDAO;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Funcionario;

@Service
public class FuncionarioServiceImpl implements FuncionarioService {

	@Autowired
	private FuncionarioDAO funcionarioDAO;
	
	@Transactional
	public Funcionario getFuncionario(int id) {
		return funcionarioDAO.getFuncionario(id);
	}

	@Transactional
	public List<Funcionario> listAll() {
		return funcionarioDAO.listAll();
	}

	@Transactional
	public void create(Funcionario funcionario) {
		funcionarioDAO.create(funcionario);
	}

	@Transactional
	public void update(Funcionario funcionario) {
		funcionarioDAO.update(funcionario);
	}

	@Transactional
	public void delete(int id) {
		funcionarioDAO.delete(id);
	}
	
	@Transactional
	public Funcionario validate(String cpf) {
		return funcionarioDAO.validate(cpf);
		 
	}

}
