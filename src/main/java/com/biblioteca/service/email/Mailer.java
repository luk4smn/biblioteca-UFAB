package com.biblioteca.service.email;

 
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;


@Component
public class Mailer {
	
	@Autowired
	private JavaMailSender javaMailSender;
	
	private final static Logger logger = Logger.getLogger(Mailer.class);

	
	public void enviar(Mensagem mensagem) {
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		
		simpleMailMessage.setFrom(mensagem.getRemetente());
		simpleMailMessage.setTo(mensagem.getDestinatarios()
				.toArray(new String[mensagem.getDestinatarios().size()]));
		simpleMailMessage.setSubject(mensagem.getAssunto());
		simpleMailMessage.setText(mensagem.getCorpo());
		
		try {
			
			javaMailSender.send(simpleMailMessage);
			
		}catch (Exception ex) {
			logger.error("erro ao enviar email", ex);
		}
		
	}

}