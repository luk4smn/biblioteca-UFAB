package com.biblioteca.service.relatorios;

import java.util.List;

import com.biblioteca.model.Aluno;
import com.biblioteca.model.Emprestimos;

public interface RelatorioService {
	
	public List<Emprestimos> listAllLivros();
	public List<Aluno> listAlunoWithPendences();
	
	
}
