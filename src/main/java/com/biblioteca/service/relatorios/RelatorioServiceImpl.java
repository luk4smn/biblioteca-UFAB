package com.biblioteca.service.relatorios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.biblioteca.dao.relatorios.RelatorioDAO;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Emprestimos;

@Service
public class RelatorioServiceImpl implements RelatorioService {
	
	@Autowired RelatorioDAO relatorioDAO;

	@Override
	public List<Emprestimos> listAllLivros() {
		
		return relatorioDAO.listAllLivros();
	}

	@Override
	public List<Aluno> listAlunoWithPendences() {
		
		return relatorioDAO.listAlunoWithPendences();
	}

}
