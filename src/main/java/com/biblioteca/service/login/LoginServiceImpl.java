package com.biblioteca.service.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.biblioteca.dao.login.LoginDAO;


@Service("loginService")
public class LoginServiceImpl implements LoginService {

	 @Autowired
	 private LoginDAO loginDAO;

	   public void setLoginDAO(LoginDAO loginDAO) {
             this.loginDAO = loginDAO;
      }
     
      public boolean checkLogin(String usuario, String senha){
             System.out.println("In Service class...Check Login");
             return loginDAO.checkLogin(usuario, senha);
      }
}
