package com.biblioteca.service.login;

public interface LoginService {
    
	public boolean checkLogin(String usuario, String senha);

}
