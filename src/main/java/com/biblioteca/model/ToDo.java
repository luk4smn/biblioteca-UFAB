package com.biblioteca.model;

/***
 * Classe de exemplo para novos CRUDs.
 * @author fedispato
 *
 */
public class ToDo {
	
	private String name;
	private String category;

	/***
	 * M�todo Construtor da classe Todo
	 * @param name Nome do produto
	 * @param category Categoria do produto
	 */
	public ToDo(String name, String category) {
		super();
		this.name = name;
		this.setCategory(category);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return String.format("Todo [name=%s, category=%s]", name, category);
	}
	

}
