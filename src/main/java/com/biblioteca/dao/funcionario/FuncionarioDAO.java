package com.biblioteca.dao.funcionario;

import java.util.List;

import com.biblioteca.model.Funcionario;

public interface FuncionarioDAO {
	public Funcionario getFuncionario(int id);
	public List<Funcionario> listAll();
	public void create(Funcionario funcionario);
	public void update(Funcionario funcionario);
	public void delete(int id);
	public Funcionario validate(String cpf);
}
