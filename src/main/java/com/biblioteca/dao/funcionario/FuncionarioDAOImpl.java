package com.biblioteca.dao.funcionario;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biblioteca.model.Aluno;
import com.biblioteca.model.Funcionario;

@Repository
public class FuncionarioDAOImpl implements FuncionarioDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Funcionario getFuncionario(int id) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Funcionario funcionario where funcionario.id=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,id);
		
		Funcionario funcionario = (Funcionario) query.uniqueResult();  
		
		session.close();
		  
		return funcionario; 
	}

	@Override
	public List<Funcionario> listAll() {
		Session session = sessionFactory.openSession();
		
		List list = session.createQuery("from Funcionario").list();
		
		session.close();
		  
		return list; 
	}

	@Override
	public void create(Funcionario funcionario) {
		Session session = sessionFactory.openSession();
		session.save(funcionario);
		session.close();
	}

	@Override
	public void update(Funcionario funcionario) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		if (null != funcionario) {
			session.update(funcionario);
		}
		
		tr.commit();
		session.close();
		
	}

	@Override
	public void delete(int id) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		Funcionario funcionario = (Funcionario) session.load(Funcionario.class, id);
		
		if (null != funcionario) {
			session.delete(funcionario);
		}
		
		tr.commit();
		session.close();
		
	}
	
	@Override
	public Funcionario validate(String cpf) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Funcionario func where func.cpf=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,cpf);
		
		
		Funcionario funcionario = (Funcionario) query.uniqueResult();  
		
		session.close();
		  
		return funcionario; 
		
	}

}
