package com.biblioteca.dao.emprestimo;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biblioteca.enums.Emprestimo;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Emprestimos;

@Repository
public class EmprestimoDAOImpl implements EmprestimoDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Emprestimos getEmprestimo(int id) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Emprestimos emprestimo where emprestimo.id=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,id);
		
		Emprestimos emprestimos = (Emprestimos) query.uniqueResult();  
		
		session.close();
		  
		return emprestimos; 
	}

	@Override
	public void create(Emprestimos emprestimo) {
		Session session = sessionFactory.openSession();
		session.save(emprestimo);
		session.close();
	}

	@Override
	public void update(Emprestimos emprestimo) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		if (null != emprestimo) {
			session.update(emprestimo);
		}
		
		tr.commit();
		session.close();
		
	}

	@Override
	public void delete(int id) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		Emprestimos emprestimo = (Emprestimos) session.load(Emprestimos.class, id);
		
		if (null != emprestimo) {
			session.delete(emprestimo);
		}
		
		tr.commit();
		session.close();
		
	}

	@Override
	public List<Emprestimos> listAll() {
		Session session = sessionFactory.openSession();
		
		List list = session.createQuery("from Emprestimos emp order by emp.id DESC").list();
		
		session.close();
		  
		return list; 
	}
	
	@Override
	public List<Emprestimos> listEmprestimoEmAberto() {
		Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from Emprestimos emprestimo where emprestimo.entregue=?");
		query.setParameter(0,Emprestimo.NAO_ENTREGUE.getValor());
		
		List list = query.list();
		 
		session.close();
		  
		return list; 
	}
	
	@Override
	public List<Aluno> listAlunoWithoutPendences() {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Aluno where id not in "
				+ "(select alunoId from Emprestimos emp "
				+ "where emp.entregue=? "
				+ "and cast(emp.dataDevolucao as date) < current_date())";
		
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,Emprestimo.NAO_ENTREGUE.getValor());
		
		List<Aluno> list = query.list();
		
		session.close();
				  
		return list; 
	}
	
	@Override
	public List<Object> calculatePendences(){
		Session session = sessionFactory.openSession();
		
		SQLQuery query = session.createSQLQuery(
				"SELECT emprestimos.id as id , " +
				"datediff(date(now()), emprestimos.data_devolucao) as dias, alunos.nivel as nivel " + 
				"from emprestimos " + 
				"join alunos on alunos.id = emprestimos.aluno_id ");

		List<Object> results = query.list();

		session.close();
		
		return results;
	}

}
