package com.biblioteca.dao.curso;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biblioteca.model.Curso;

@Repository
public class CursoDAOImpl implements CursoDAO {
	
	@Autowired
	   private SessionFactory sessionFactory;


	@Override
	public Curso getCurso(int id) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Curso curso where curso.id=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,id);
		
		Curso curso = (Curso) query.uniqueResult();  
		
		session.close();
		  
		return curso; 

	}

	@Override
	public List<Curso> listAll() {
		
		Session session = sessionFactory.openSession();
		
		List list = session.createQuery("from Curso").list();
		
		session.close();
		
		return list; 

	}

	@Override
	public void create(Curso curso) {
		Session session = sessionFactory.openSession();
		session.save(curso);
		session.close();
	}

	@Override
	public void update(Curso curso) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		if (null != curso) {
			session.update(curso);
		}
		
		tr.commit();
		session.close();

	}

	@Override
	public void delete(int id) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		Curso curso = (Curso) session.load(Curso.class, id);
		
		if (null != curso) {
			session.delete(curso);
		}
		
		tr.commit();
		session.close();
	}

	@Override
	public Curso validate(String nome, String tipo) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Curso curso where curso.nome=? and curso.tipo=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,nome);
		query.setParameter(1,tipo);
		
		Curso curso = (Curso) query.uniqueResult();  
		
		session.close();
		  
		return curso; 
		
	}

}
