package com.biblioteca.dao.relatorios;

import java.util.List;

import com.biblioteca.model.Aluno;
import com.biblioteca.model.Emprestimos;

public interface RelatorioDAO {
	public List<Emprestimos> listAllLivros();
	public List<Aluno> listAlunoWithPendences();
}
