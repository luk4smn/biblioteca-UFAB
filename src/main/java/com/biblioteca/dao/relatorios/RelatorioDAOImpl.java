package com.biblioteca.dao.relatorios;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biblioteca.enums.Emprestimo;
import com.biblioteca.enums.ItemDeAcervo;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Emprestimos;

@Repository
public class RelatorioDAOImpl implements RelatorioDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Emprestimos> listAllLivros() {
	Session session = sessionFactory.openSession();
		
		Query query = session.createQuery("from Emprestimos emp where emp.tipoItem=?");
		query.setParameter(0,ItemDeAcervo.LIVRO.getValor());
		List list = query.list();
		session.close();
		  
		return list; 
	}

	@Override
	public List<Aluno> listAlunoWithPendences() {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY =
				"from Aluno where id in "
				+ "(select alunoId from Emprestimos emp where emp.entregue=? "
				+ "and cast(emp.dataDevolucao as date) < current_date())";
		
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,Emprestimo.NAO_ENTREGUE.getValor());
		
		List<Aluno> list = query.list();
		
		session.close();
				  
		return list; 
	}
	
	
	
	
	
	
}
