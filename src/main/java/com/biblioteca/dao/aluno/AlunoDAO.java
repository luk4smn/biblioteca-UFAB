/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */


package com.biblioteca.dao.aluno;

import java.util.List;

import com.biblioteca.model.Aluno;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

/***
 *interface de implementacao para o repository Alunos
 */
public interface AlunoDAO {
	public Aluno getAluno(int id);
	public List<Aluno> listAll();
	public void create(Aluno aluno);
	public void update(Aluno aluno);
	public void delete(int id);
	public int count();
	public Aluno validate(String cpf);
}
