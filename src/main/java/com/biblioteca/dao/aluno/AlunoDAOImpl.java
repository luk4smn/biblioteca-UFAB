/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */


package com.biblioteca.dao.aluno;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.biblioteca.enums.Emprestimo;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Curso;

@Repository
public class AlunoDAOImpl implements AlunoDAO {
	
	@Autowired
	   private SessionFactory sessionFactory;

	/***
	 * retorna do banco de dados um dado do tipo Aluno
	 * 
	 * @param id
	 * @return aluno
	 *
	 */
	public Aluno getAluno(int id) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Aluno aluno where aluno.id=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,id);
		
		Aluno aluno = (Aluno) query.uniqueResult();  
		
		session.close();
		  
		return aluno; 	
	}


	/***
	 * Lista os Aluno de acordo com os dados do BD
	 * 
	 * @return list
	 *
	 */
	public List<Aluno> listAll() {
		Session session = sessionFactory.openSession();
		
		List list = session.createQuery("from Aluno").list();
		
		session.close();
		
	  return list; 
	}

	/***
	 * Cria os Alunos no banco de dados
	 * 
	 * 
	 *
	 */
	public void create(Aluno aluno) {
		Session session = sessionFactory.openSession();
		session.save(aluno);
		session.close();
	}

	/***
	 * Atualiza os Alunos no banco de dados
	 * 
	 * 
	 *
	 */
	public void update(Aluno aluno) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		if (null != aluno) {
			session.update(aluno);
		}
		
		tr.commit();
		session.close();	
	}

	/***
	 * Deleta os Alunos no banco de dados
	 * e todos os dados relcionados a ele
	 * 
	 *
	 */
	public void delete(int id) {
		Session session = sessionFactory.openSession();
		
		Query query = session.createSQLQuery(
				"DELETE "+
				"FROM emprestimos " + 
				"WHERE emprestimos.aluno_id = ?");
		
		query.setParameter(0,id);
		query.executeUpdate();
		
		Query query2 = session.createSQLQuery(
				"DELETE "+
				"FROM reservas " + 
				"WHERE reservas.aluno_id = ?");
		
		query2.setParameter(0,id);
		query2.executeUpdate();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		Aluno aluno = (Aluno) session.load(Aluno.class, id);
		
		if (null != aluno) {
			session.delete(aluno);
		}
		
		tr.commit();
		session.close();
	}

	/***
	 * Retorna a quantidade de alunos cadastrados no BD
	 * 
	 *
	 */
	public int count() {
		
		int count = ((Long)sessionFactory.getCurrentSession()
				.createQuery("select count(*) from Aluno")
				.uniqueResult())
				.intValue();

		return 0;
	}
	
	/***
	 * Valida o cadastro do aluno 
	 *   @param cpf
	 *
	 */
	@Override
	public Aluno validate(String cpf) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Aluno aluno where aluno.cpf=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,cpf);
		
		
		Aluno aluno = (Aluno) query.uniqueResult();  
		
		session.close();
		  
		return aluno; 
		
	}

}
