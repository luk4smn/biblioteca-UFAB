package com.biblioteca.dao.reservas;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.biblioteca.model.Reservas;

@Repository
public class ReservasDAOImpl implements ReservasDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Reservas getReserva(int id) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Reservas reserva where reserva.id=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,id);
		
		Reservas reserva = (Reservas) query.uniqueResult();  
		
		session.close();
		  
		return reserva; 
	}
	
	@Override
	public Reservas getFirstReserva(int tipoItem, int itemId) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Reservas reserva where reserva.tipoItem=? and reserva.itemId=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,tipoItem);
		query.setParameter(1,itemId);
		
		Reservas reserva = (Reservas) query.uniqueResult();  
		
		session.close();
		  
		return reserva; 
	}

	@Override
	public void create(Reservas reserva) {
		Session session = sessionFactory.openSession();
		session.save(reserva);
		session.close();
	}

	@Override
	public void update(Reservas reserva) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		if (null != reserva) {
			session.update(reserva);
		}
		
		tr.commit();
		session.close();
		
	}

	@Override
	public void delete(int id) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		Reservas reserva = (Reservas) session.load(Reservas.class, id);
		
		if (null != reserva) {
			session.delete(reserva);
		}
		
		tr.commit();
		session.close();
		
	}

	@Override
	public List<Reservas> listAll() {
		Session session = sessionFactory.openSession();
		
		List list = session.createQuery("from Reservas emp order by emp.id DESC").list();
		
		session.close();
		  
		return list; 
	}
	
	
}
