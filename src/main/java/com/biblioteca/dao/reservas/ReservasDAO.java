package com.biblioteca.dao.reservas;

import java.util.List;

import com.biblioteca.model.Reservas;


public interface ReservasDAO {
	public Reservas getReserva(int id);
	public Reservas getFirstReserva(int tipoItem, int itemId);
	public List<Reservas> listAll();
	public void create(Reservas reservas);
	public void update(Reservas reservas);
	public void delete(int id);
}
