package com.biblioteca.dao.login;

public interface LoginDAO {
    
	public boolean checkLogin(String usuario, String senha);

}
