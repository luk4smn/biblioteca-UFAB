package com.biblioteca.dao.login;

import org.springframework.stereotype.Repository;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Query;
import java.util.List;

import javax.annotation.Resource;

@Repository
public class LoginDAOImpl implements LoginDAO{
    
	 
    @Resource
    protected SessionFactory sessionFactory;

    
    public boolean checkLogin(String usuario, String senha){
			Session session = sessionFactory.openSession();
			
			boolean userFound = false;
			
			String SQL_QUERY =" from Usuario as user where user.usuario=? and user.senha=?";
			Query query = session.createQuery(SQL_QUERY);
			query.setParameter(0,usuario);
			query.setParameter(1,senha);
			List list = query.list();

			if ((list != null) && (list.size() > 0)) {
				userFound= true;
			}

			session.close();
			return userFound;              
    }

}
