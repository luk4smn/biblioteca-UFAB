/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */


package com.biblioteca.dao.acervo.anais;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biblioteca.model.AnaisDeCongresso;

@Repository
public class AnaisDeCongresoDAOImpl implements AnaisDeCongressoDAO {
	
	
	@Autowired
	   private SessionFactory sessionFactory;

	/***
	 * retorna do banco de dados um dado do tipo AnaisDeCongresso
	 * 
	 * @param id
	 * @return anal
	 *
	 */
	@Override
	public AnaisDeCongresso getAnal(int id) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from AnaisDeCongresso anal where anal.id=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,id);
		
		AnaisDeCongresso anal = (AnaisDeCongresso) query.uniqueResult();  
		
		session.close();
		  
		return anal; 
	}

	/***
	 * Lista os AnaisDeCongresso de acordo com os dados do BD
	 * 
	 * @return list
	 *
	 */
	@Override
	public List<AnaisDeCongresso> listAll() {
		Session session = sessionFactory.openSession();
		
		List list = session.createQuery("from AnaisDeCongresso").list();
		
		session.close();
		
		return list; 
	}

	/***
	 * Cria os AnaisDeCongresso no banco de dados
	 * 
	 * @return anal
	 *
	 */
	@Override
	public void create(AnaisDeCongresso anal) {
		Session session = sessionFactory.openSession();
		session.save(anal);
		session.close();
		
	}
	
	/***
	 * Atualiza os AnaisDeCongresso no banco de dados
	 * 
	 * @return anal
	 *
	 */
	@Override
	public void update(AnaisDeCongresso anal) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		if (null != anal) {
			session.update(anal);
		}
		
		tr.commit();
		session.close();
		
	}

	/***
	 * Deleta os AnaisDeCongresso no banco de dados
	 * 
	 * @return anal
	 *
	 */
	@Override
	public void delete(int id) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		AnaisDeCongresso anal = (AnaisDeCongresso) session.load(AnaisDeCongresso.class, id);
		
		if (null != anal) {
			session.delete(anal);
		}
		
		tr.commit();
		session.close();
		
	}

}
