/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.dao.acervo.anais;

import java.util.List;

import com.biblioteca.model.AnaisDeCongresso;
/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

/***
 *interface de implementacao para o repository AnaisDeCongresso
 */
public interface AnaisDeCongressoDAO {

	public AnaisDeCongresso getAnal(int id);
	public List<AnaisDeCongresso> listAll();
	public void create(AnaisDeCongresso anal);
	public void update(AnaisDeCongresso anal);
	public void delete(int id);

}
