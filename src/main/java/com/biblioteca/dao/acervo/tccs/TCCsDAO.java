/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */


package com.biblioteca.dao.acervo.tccs;

import java.util.List;

import com.biblioteca.model.TrabalhosDeConclusao;


/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

/***
 *interface de implementacao para o repository Tcc
 */
public interface TCCsDAO {

	public TrabalhosDeConclusao getTcc(int id);
	public List<TrabalhosDeConclusao> listAll();
	public void create(TrabalhosDeConclusao tcc);
	public void update(TrabalhosDeConclusao tcc);
	public void delete(int id);

}
