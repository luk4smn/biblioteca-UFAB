/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.dao.acervo.tccs;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biblioteca.model.TrabalhosDeConclusao;

@Repository
public class TCCsDAOImpl implements TCCsDAO {
	
	@Autowired
	   private SessionFactory sessionFactory;

	/***
	 * retorna do banco de dados um dado do tipo TrabalhosDeConclusao
	 * 
	 * @param id
	 * @return anal
	 *
	 */
	@Override
	public TrabalhosDeConclusao getTcc(int id) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from TrabalhosDeConclusao tcc where tcc.id=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,id);
		
		TrabalhosDeConclusao tcc = (TrabalhosDeConclusao) query.uniqueResult();  
		
		session.close();
		  
		return tcc; 
	}

	/***
	 * Lista os TrabalhosDeConclusao de acordo com os dados do BD
	 * 
	 * @return list
	 *
	 */
	@Override
	public List<TrabalhosDeConclusao> listAll() {
		Session session = sessionFactory.openSession();
		
		List list = session.createQuery("from TrabalhosDeConclusao").list();
		
		session.close();
		
		return list; 
	}

	/***
	 * Cria os TrabalhosDeConclusao no banco de dados
	 * 
	 * @return anal
	 *
	 */
	@Override
	public void create(TrabalhosDeConclusao tcc) {
		Session session = sessionFactory.openSession();
		session.save(tcc);
		session.close();
		
	}

	/***
	 * Atualiza os TrabalhosDeConclusao no banco de dados
	 * 
	 * @return anal
	 *
	 */
	@Override
	public void update(TrabalhosDeConclusao tcc) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		if (null != tcc) {
			session.update(tcc);
		}
		
		tr.commit();
		session.close();
		
	}

	/***
	 * Deleta os TrabalhosDeConclusao no banco de dados
	 * 
	 * @return anal
	 *
	 */
	@Override
	public void delete(int id) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		TrabalhosDeConclusao tcc = (TrabalhosDeConclusao) session.load(TrabalhosDeConclusao.class, id);
		
		if (null != tcc) {
			session.delete(tcc);
		}
		
		tr.commit();
		session.close();
		
	}

}
