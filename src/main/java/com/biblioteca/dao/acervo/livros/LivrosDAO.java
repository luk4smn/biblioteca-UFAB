/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.dao.acervo.livros;

import java.util.List;

import com.biblioteca.model.Livros;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

/***
 *interface de implementacao para o repository Livros
 */

public interface LivrosDAO {

	public Livros getLivro(int id);
	public List<Livros> listAll();
	public void create(Livros anal);
	public void update(Livros anal);
	public void delete(int id);

}
