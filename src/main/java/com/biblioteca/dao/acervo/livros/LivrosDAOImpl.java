/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.dao.acervo.livros;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biblioteca.model.Livros;


@Repository
public class LivrosDAOImpl implements LivrosDAO {
	
	@Autowired
	   private SessionFactory sessionFactory;

	/***
	 * retorna do banco de dados um dado do tipo Livros
	 * 
	 * @param id
	 * @return livro
	 *
	 */
	@Override
	public Livros getLivro(int id) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Livros livro where livro.id=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,id);
		
		Livros livro = (Livros) query.uniqueResult();  
		
		session.close();
		  
		return livro; 
	}

	/***
	 * Lista os Livros de acordo com os dados do BD
	 * 
	 * @return list
	 *
	 */
	@Override
	public List<Livros> listAll() {
		Session session = sessionFactory.openSession();
		
		List list = session.createQuery("from Livros").list();
		
		session.close();
		
		return list; 
	}
	
	/***
	 * Cria os Livros no banco de dados
	 * 
	 *
	 */
	@Override
	public void create(Livros livro) {
		Session session = sessionFactory.openSession();
		session.save(livro);
		session.close();
		
	}
	/***
	 * Atualiza os Livros no banco de dados
	 * 
	 *
	 */
	@Override
	public void update(Livros livro) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		if (null != livro) {
			session.update(livro);
		}
		
		tr.commit();
		session.close();
		
	}

	/***
	 * Deleta os Livros no banco de dados
	 * 
	 *
	 */
	@Override
	public void delete(int id) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		Livros livro = (Livros) session.load(Livros.class, id);
		
		if (null != livro) {
			session.delete(livro);
		}
		
		tr.commit();
		session.close();
		
	}

}
