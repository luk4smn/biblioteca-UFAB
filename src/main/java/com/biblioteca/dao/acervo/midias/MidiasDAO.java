/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.dao.acervo.midias;

import java.util.List;

import com.biblioteca.model.MidiasEletronicas;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

/***
 *interface de implementacao para o repository MidiasEletronicas 
 */
public interface MidiasDAO {
	
	public MidiasEletronicas getMidia(int id);
	public List<MidiasEletronicas> listAll();
	public void create(MidiasEletronicas midia);
	public void update(MidiasEletronicas midia);
	public void delete(int id);
}
