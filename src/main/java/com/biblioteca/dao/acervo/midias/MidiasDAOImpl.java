/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.dao.acervo.midias;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biblioteca.model.MidiasEletronicas;

@Repository
public class MidiasDAOImpl implements MidiasDAO {
		
		@Autowired
		   private SessionFactory sessionFactory;
		
		/***
		 * retorna do banco de dados um dado do tipo MidiasEletronicas
		 * 
		 * @param id
		 * @return midia
		 *
		 */
		@Override
		public MidiasEletronicas getMidia(int id) {
			Session session = sessionFactory.openSession();
			
			String SQL_QUERY ="from MidiasEletronicas midia where midia.id=?";
			Query query = session.createQuery(SQL_QUERY);
			query.setParameter(0,id);
			
			MidiasEletronicas midia = (MidiasEletronicas) query.uniqueResult();  
			
			session.close();
			  
			return midia; 
		}

		/***
		 * Lista as MidiasEletronicas de acordo com os dados do BD
		 * 
		 * @return list
		 *
		 */
		@Override
		public List<MidiasEletronicas> listAll() {
			Session session = sessionFactory.openSession();
			
			List list = session.createQuery("from MidiasEletronicas").list();
			
			session.close();
			
			return list; 
		}

		/***
		 * Cria os MidiasEletronicas no banco de dados
		 * 
		 */
		@Override
		public void create(MidiasEletronicas midia) {
			Session session = sessionFactory.openSession();
			session.save(midia);
			session.close();
			
		}

		/***
		 * Atualiza os MidiasEletronicas no banco de dados
		 * 
		 *
		 */
		@Override
		public void update(MidiasEletronicas midia) {
			Session session = sessionFactory.openSession();
			
			org.hibernate.Transaction tr = session.beginTransaction();
			
			if (null != midia) {
				session.update(midia);
			}
			
			tr.commit();
			session.close();
			
		}

		
		/***
		 * Deleta os MidiasEletronicas no banco de dados
		 *
		 */
		@Override
		public void delete(int id) {
			Session session = sessionFactory.openSession();
			
			org.hibernate.Transaction tr = session.beginTransaction();
			
			MidiasEletronicas midia = (MidiasEletronicas) session.load(MidiasEletronicas.class, id);
			
			if (null != midia) {
				session.delete(midia);
			}
			
			tr.commit();
			session.close();
			
		}
}
