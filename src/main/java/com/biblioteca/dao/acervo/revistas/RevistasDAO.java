/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.dao.acervo.revistas;

import java.util.List;

import com.biblioteca.model.Revistas;
/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

/***
 *interface de implementacao para o repository AnaisDeCongresso
 */
public interface RevistasDAO {

	public Revistas getRevista(int id);
	public List<Revistas> listAll();
	public void create(Revistas livro);
	public void update(Revistas livro);
	public void delete(int id);

}
