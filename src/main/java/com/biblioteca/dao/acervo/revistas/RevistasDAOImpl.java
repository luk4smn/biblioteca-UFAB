/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.dao.acervo.revistas;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biblioteca.model.Revistas;

@Repository
public class RevistasDAOImpl implements RevistasDAO {
	
	@Autowired
	   private SessionFactory sessionFactory;

	/***
	 * retorna do banco de dados um dado do tipo Revistas
	 * 
	 * @param id
	 * @return revistas
	 *
	 */
	@Override
	public Revistas getRevista(int id) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Revistas revista where revista.id=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,id);
		
		Revistas revista = (Revistas) query.uniqueResult();  
		
		session.close();
		  
		return revista; 
	}

	
	/***
	 * Lista os AnaisDeCongresso de acordo com os dados do BD
	 * 
	 * @return revista
	 *
	 */
	@Override
	public List<Revistas> listAll() {
		Session session = sessionFactory.openSession();
		
		List list = session.createQuery("from Revistas").list();
		
		session.close();
		
		return list; 
	}
	
	/***
	 * Cria os Revistas no banco de dados
	 * 
	 * @return revista
	 *
	 */
	@Override
	public void create(Revistas revista) {
		Session session = sessionFactory.openSession();
		session.save(revista);
		session.close();
		
	}

	/***
	 * Atualiza os Revistas no banco de dados
	 * 
	 * @return revista
	 *
	 */
	@Override
	public void update(Revistas revistas) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		if (null != revistas) {
			session.update(revistas);
		}
		
		tr.commit();
		session.close();
		
	}

	/***
	 * deleta os Revistas no banco de dados
	 * 
	 * @return revista
	 *
	 */
	@Override
	public void delete(int id) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		Revistas revista = (Revistas) session.load(Revistas.class, id);
		
		if (null != revista) {
			session.delete(revista);
		}
		
		tr.commit();
		session.close();
		
	}

}
