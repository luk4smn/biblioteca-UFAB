/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */


package com.biblioteca.dao.acervo.jornais;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biblioteca.model.Jornais;

@Repository
public class JornaisDAOImpl implements JornaisDAO {
	
	@Autowired
	   private SessionFactory sessionFactory;

	/***
	 * retorna do banco de dados um dado do tipo Jornais
	 * 
	 * @param id
	 * @return jornal
	 *
	 */
	@Override
	public Jornais getJornal(int id) {
		Session session = sessionFactory.openSession();
		
		String SQL_QUERY ="from Jornais jornal where jornal.id=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,id);
		
		Jornais jornal = (Jornais) query.uniqueResult();  
		
		session.close();
		  
		return jornal; 
	}

	/***
	 * Lista os Jornais de acordo com os dados do BD
	 * 
	 * @return list
	 */
	@Override
	public List<Jornais> listAll() {
		Session session = sessionFactory.openSession();
		
		List list = session.createQuery("from Jornais").list();
		
		session.close();
		
		return list; 
	}

	/***
	 * Cria os Jornais no banco de dados
	 * 
	 * 
	 */
	@Override
	public void create(Jornais jornal) {
		Session session = sessionFactory.openSession();
		session.save(jornal);
		session.close();
		
	}
	/***
	 * Atualiza os Jornais no banco de dados
	 * 
	 *
	 */
	@Override
	public void update(Jornais jornal) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		if (null != jornal) {
			session.update(jornal);
		}
		
		tr.commit();
		session.close();
		
	}

	/***
	 * Deleta os Jornais no banco de dados
	 * 
	 */
	@Override
	public void delete(int id) {
		Session session = sessionFactory.openSession();
		
		org.hibernate.Transaction tr = session.beginTransaction();
		
		Jornais jornal = (Jornais) session.load(Jornais.class, id);
		
		if (null != jornal) {
			session.delete(jornal);
		}
		
		tr.commit();
		session.close();
		
	}

}
