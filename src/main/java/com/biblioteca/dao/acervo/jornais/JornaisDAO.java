/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */


package com.biblioteca.dao.acervo.jornais;

import java.util.List;

import com.biblioteca.model.Jornais;


/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

/***
 *interface de implementacao para o repository Jornais
 */


public interface JornaisDAO {

	public Jornais getJornal(int id);
	public List<Jornais> listAll();
	public void create(Jornais jornal);
	public void update(Jornais jornal);
	public void delete(int id);

}
