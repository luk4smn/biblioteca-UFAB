package com.biblioteca.dao.usuario;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.biblioteca.enums.Roles;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Funcionario;
import com.biblioteca.model.Usuario;

@Repository
public class UsuarioDAOImpl implements UsuarioDAO {

	   @Autowired
	   private SessionFactory sessionFactory;
	   
	   
		public void create(Usuario user) {
			Session session = sessionFactory.openSession();
			session.save(user);
			session.close();
		}

		
		public void update(Usuario user) {
			Session session = sessionFactory.openSession();
			
			org.hibernate.Transaction tr = session.beginTransaction();
			
			if (null != user) {
				session.update(user);
			}
			
			tr.commit();
			session.close();
		}

		
		public void delete(Integer id) {
			Session session = sessionFactory.openSession();
			
			org.hibernate.Transaction tr = session.beginTransaction();
			
			Usuario user = (Usuario) session.load(Usuario.class, id);
			
			if (null != user) {
				session.delete(user);
			}
			
			tr.commit();
			session.close();
		}

		public List<Usuario> listUsers() {		
		  List list = sessionFactory.getCurrentSession().createQuery("from Usuario").list();
		  
		  return list; 
		}


		public Usuario getUser(String userName) {
			Session session = sessionFactory.openSession();
			
			String SQL_QUERY ="from Usuario user where user.usuario=?";
			Query query = session.createQuery(SQL_QUERY);
			query.setParameter(0,userName);
			
			Usuario usuario = (Usuario) query.uniqueResult();  
			
			session.close();
			  
			return usuario; 
		}
		
		
		public Usuario getUserEdit(int id) {
			
			Session session = sessionFactory.openSession();
			
			String SQL_QUERY ="from Usuario user where user.id=?";
			Query query = session.createQuery(SQL_QUERY);
			query.setParameter(0,id);
			
			Usuario usuario = (Usuario) query.uniqueResult();  
			
			session.close();
			  
			return usuario; 
			  
		}

		public List<Funcionario> listFuncionarioWithoutUser() {
			Session session = sessionFactory.openSession();
			
			String SQL_QUERY ="from Funcionario where id not in (select dadoDeReferenciaId from Usuario user where user.role=?)";
			
			Query query = session.createQuery(SQL_QUERY);
			query.setParameter(0,Roles.FUNCIONARIO.getValor());
			
			List<Funcionario> list = query.list();
			
			session.close();
					  
			return list; 
		}

		public List<Aluno> listAlunoWithoutUser() {
			Session session = sessionFactory.openSession();
			
			String SQL_QUERY ="from Aluno where id not in (select dadoDeReferenciaId from Usuario user where user.role=?)";
			
			Query query = session.createQuery(SQL_QUERY);
			query.setParameter(0,Roles.ALUNO.getValor());
			
			List<Aluno> list = query.list();
			
			session.close();
					  
			return list; 
		}
		
		
}
