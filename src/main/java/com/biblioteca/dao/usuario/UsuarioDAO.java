package com.biblioteca.dao.usuario;

import java.util.List;

import com.biblioteca.model.Aluno;
import com.biblioteca.model.Funcionario;
import com.biblioteca.model.Usuario;

public interface UsuarioDAO {

   public void create(Usuario user);
   public void update(Usuario user);
   public void delete(Integer id);
   public List<Usuario> listUsers();
   public Usuario getUser(String user);
   public List<Funcionario> listFuncionarioWithoutUser();
   public List<Aluno> listAlunoWithoutUser();
   public Usuario getUserEdit(int id);

}
