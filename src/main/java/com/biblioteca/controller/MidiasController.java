/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.ItemDeAcervo;
import com.biblioteca.enums.Roles;
import com.biblioteca.enums.TipoMidiaEletronica;
import com.biblioteca.model.MidiasEletronicas;
import com.biblioteca.service.acervo.midias.MidiasService;
import com.biblioteca.service.emprestimo.EmprestimoService;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/
@Controller
public class MidiasController {

	
	@Autowired
	private MidiasService midiasService;
	
	@Autowired
	private EmprestimoService emprestimoService;
	
	/***
	 * Adiciona um novo item a Midias
	 * 
	 * @param midia
	 * 
	 * @return "redirect:/midias/list"
	 *
	 */
	@RequestMapping(value = "/midias/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("midia") MidiasEletronicas midia, BindingResult result,
			HttpServletRequest request) {
		
		midiasService.create(midia);

		return "redirect:/midias/list";
	}
	
	/***
	 * Atualiza um item de Midias
	 * 
	 * @param midia
	 * 
	 * @return "redirect:/midias/list"
	 *
	 */
	@RequestMapping(value = "/midias/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("midia") MidiasEletronicas midia, BindingResult result,
			HttpServletRequest request) {
		
		midiasService.update(midia);

		return "redirect:/midias/list";
	}
	
	/***
	 * Deleta um item de Midias
	 * 
	 * @param midia
	 * 
	 * @return "redirect:/midias/list"
	 *
	 */
	@RequestMapping("/midias/delete/{id}")
	public String delete(@PathVariable("id") Integer id,
			HttpServletRequest request, ModelMap model) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			midiasService.delete(id);
			return "redirect:/midias/list";
		}
		
		model.addAttribute("errorMessage", "Permissão negada");
		return "redirect:/midias/list";
	}
	
	/***
	 * Metodo que retorna a view as Midias de acordo com o nivel de usuario
	 * 
	 * @return acervo/midias/list_admin; ou acervo/midias/list_funcionario; ou
	 *         acervo/midias/list_aluno;
	 * 
	 *
	 */
	@RequestMapping(value = "/midias/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map, HttpServletRequest request) {
		return  this.getView(map, request);
	}
	
	
	public String  getView(Map<String, Object> map, HttpServletRequest request) {
		
		String role = request.getSession().getAttribute("role").toString();
		map.put("midias", midiasService.listAll());
		map.put("tipos", this.listTipos());
		
		map.put("emprestimos", emprestimoService.listEmprestimoEmAberto());
		map.put("alunos", emprestimoService.listAlunoWithoutPendences());
		map.put("tipoItemAcervo", ItemDeAcervo.MIDIA_ELETRONICA.getValor());
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return "acervo/midias/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			return "acervo/midias/list_funcionario";
		}
		else {	
			return "acervo/midias/list_aluno";
		}
	}
	
	public List<Object> listTipos() {
		
		ArrayList<Object> tipos = new ArrayList<Object>();

		tipos.add(new Object[] {	TipoMidiaEletronica.CD.getValor(), "CD"	});
		tipos.add(new Object[] {	TipoMidiaEletronica.DVD.getValor(), "DVD"	});
			
		return tipos;
	}
}
