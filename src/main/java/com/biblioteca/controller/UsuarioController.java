/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.Roles;
import com.biblioteca.model.Usuario;
import com.biblioteca.service.usuario.UsuarioService;

/***
 * 
 * @author Lucas Nunes
 *
 */
@Controller
public class UsuarioController {
		
	@Autowired
	private UsuarioService userService;
	
	/***
	 * Adiciona um novo usurio a base de dados
	 * as variáveis aluno e funcionario vem do formulário para indicar o id de referência
	 * id_referencia indica o id do aluno ou funcionário o usuário pertence
	 * userValidator valida o nome de usuário, para que não existam logins com o mesmo nome
	 * @param usuario Classe Usuario
	 */
	@RequestMapping(value = "/users/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("usuario") Usuario usuario, BindingResult result,
			HttpServletRequest request, ModelMap model) {
		
		int aluno 			= Integer.parseInt(request.getParameter("aluno"));
		int funcionario 	= Integer.parseInt(request.getParameter("funcionario"));
		int id_referencia 	= 0;
	
		Usuario userValidator = userService.getUser(usuario.getUsuario());
		
		if (aluno != 0) {
			id_referencia = aluno;
			usuario.setDadoDeReferenciaId(id_referencia);
		}
		else if(funcionario != 0) {
			id_referencia = funcionario;
			usuario.setDadoDeReferenciaId(id_referencia);
		}	
		else {
			model.addAttribute("errorMessage", "Selecione um aluno ou funcionário.");
			
			return this.getUserView(model, request);
		}
		

		if(userValidator != null && userValidator.getUsuario().equals(usuario.getUsuario())) {
			model.addAttribute("errorMessage", "O usuário já existe.");
			
			return this.getUserView(model, request);
		}	
			
		userService.create(usuario);

		return "redirect:/users/list";
	}
	
	@RequestMapping(value = "/users/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("usuario") Usuario usuario, BindingResult result,
			HttpServletRequest request, ModelMap model) {
					
		userService.update(usuario);
		
		return "redirect:/users/list";
	}
	

	/***
	 * Método que lista as informações dos usuários
	 * @return Dados do usurio: Classe Usuario
	 */
	@RequestMapping(value = "/users/list", method = RequestMethod.GET)
	public String listUsers(Map<String, Object> map, HttpServletRequest request) {
		return  this.getUserView(map, request);
	}
	
	/***
	 * Método que deleta as informações dos usuários
	 *
	 */
	@RequestMapping("/users/delete/{id}")
	public String delete(@PathVariable("id") Integer id) {
		
		userService.delete(id);

		return "redirect:/users/list";
	}
	
	/***
	 * Metodo que retorna a view para editar as informacoes dos usuarios
	 * 
	 * @return Dados do usuario: Classe Usuario
	 */
	@RequestMapping(value = "/users/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id")	Integer id,  Map<String, Object> map, HttpServletRequest request) {
		return  this.getUserEditView(map, request, id);
	}
		

	public String  getUserView(Map<String, Object> map, HttpServletRequest request) {
		String role 	= request.getSession().getAttribute("role").toString();
		String userName = request.getSession().getAttribute("name").toString();
		
		map.put("funcionarios", userService.listFuncionarioWithoutUser());
		map.put("alunos", userService.listAlunoWithoutUser());
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			map.put("usuarios", userService.listAll());
			return "users/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			map.put("usuario", userService.getUser(userName));
			return "users/list_funcionario";
		}
		else {	
			map.put("usuario", userService.getUser(userName));
			return "users/list_aluno";
		}
	}
	

	public String  getUserEditView(Map<String, Object> map, HttpServletRequest request, int id) {
		
		String role 	= request.getSession().getAttribute("role").toString();
		String userName = request.getSession().getAttribute("name").toString();
		
		map.put("usuario", userService.getUserEdit(id));
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			map.put("usuarios", userService.listAll());
			return "users/edit_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			map.put("usuario", userService.getUser(userName));
			return "users/edit_funcionario";
		}
		else {	
			map.put("usuario", userService.getUser(userName));
			return "users/edit_aluno";
		}
	}
		
	

	
	
	
	

}
