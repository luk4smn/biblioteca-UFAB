/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.biblioteca.enums.Roles;


/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

@Controller
public class AcervoController {
	
	/***
	 * Metodo retorna opcoes do acervo de acordo com a permissibilidade do tipo de
	 * usuario.
	 * 
	 * @param Role
	 *            (Rege o tipo de acervo de acordo com o usuario)
	 * @return Dados do acervo
	 * 
	 **/	
	@RequestMapping(value = "/acervo")
	public String index(HttpServletRequest request) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return "acervo/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			return "acervo/list_funcionario";
		}
		
		return "redirect:/home";
		
	}
		

}
