/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.Roles;
import com.biblioteca.model.Usuario;
import com.biblioteca.service.login.LoginService;
import com.biblioteca.service.usuario.UsuarioService;


/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/
@Controller
public class LoginController {
	
	static String view;


	@Autowired
	public LoginService loginService;
	
	@Autowired
	public UsuarioService userService;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String auth() {
				
		return "auth/login";
	}


	/***
	 * Metodo que verifica as informações dos usuarios e realiza o login
	 * 
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String makeAuth(@ModelAttribute("userAuthenticated") Usuario user, BindingResult result, HttpServletRequest request) {

		if (result.hasErrors()) {
			return "auth/login";
		}			
	
		boolean userExists = loginService.checkLogin(user.getUsuario(), user.getSenha());
		
		if(userExists){
			
			user = userService.getUser(user.getUsuario());
					
			request.getSession().setAttribute("name", user.getUsuario());
			request.getSession().setAttribute("role", user.getRole());
			request.getSession().setAttribute("reference", user.getDadoDeReferenciaId());		
			
			this.verifyRole(user.getRole());			
						
			return "redirect:home";
			
		}else{
			result.rejectValue("usuario","usuario.notvalid","Credenciais Inválidas");
			return "auth/login";
		}

	}
	

	/***
	 * Metodo que rediciociona os usuarios autenticados para a pagina inicial
	 * 
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(HttpServletRequest request) {
		String role = request.getSession().getAttribute("role").toString();
		
		int value = Integer.parseInt(role);
		
		this.verifyRole(value);
		
		return view;
	}
	
	/***
	 * Metodo que realiza o logout
	 * 
	 */
	@RequestMapping(value = "/logout")
	public String logout(HttpServletRequest request) {
		
		request.getSession().invalidate();
		return "redirect:/login";
	}
	
	
	private void verifyRole(int role) {
		if (role == Roles.ADMIN.getValor()) {
			view = "home/admin";
		}			
		else if(role == Roles.FUNCIONARIO.getValor()){
			 view = "home/funcionario"; 
		}
		else {	
			view = "home/aluno";
		}		
	}
	
	
	
	
	
	
}
