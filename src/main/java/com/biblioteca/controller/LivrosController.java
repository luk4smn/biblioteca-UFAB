/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.ItemDeAcervo;
import com.biblioteca.enums.Roles;
import com.biblioteca.enums.TiposLivros;
import com.biblioteca.model.Livros;
import com.biblioteca.service.acervo.livros.LivrosService;
import com.biblioteca.service.emprestimo.EmprestimoService;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/
@Controller
public class LivrosController {

	@Autowired
	private LivrosService livrosService;
	
	@Autowired
	private EmprestimoService emprestimoService;
	
	/***
	 * Adiciona um novo item a Livros
	 * 
	 * @param livro
	 * 
	 * @return "redirect:/livros/list"
	 *
	 */
	@RequestMapping(value = "/livros/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("livro") Livros livro, BindingResult result,
			HttpServletRequest request) {
		
		livrosService.create(livro);

		return "redirect:/livros/list";
	}
	
	/***
	 * Atualiza um item de Livros
	 * 
	 * @param livro
	 * 
	 * @return "redirect:/livros/list"
	 *
	 */
	@RequestMapping(value = "/livros/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("livro") Livros livro, BindingResult result,
			HttpServletRequest request) {
			
		livrosService.update(livro);

		return "redirect:/livros/list";
	}
	
	/***
	 * Remove um item de Livros
	 * 
	 * @param livro
	 * 
	 * @return "redirect:/livros/list"
	 *
	 */
	@RequestMapping("/livros/delete/{id}")
	public String delete(@PathVariable("id") Integer id,
			HttpServletRequest request, ModelMap model) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			livrosService.delete(id);
			return "redirect:/livros/list";
		}
		
		model.addAttribute("errorMessage", "Permissão negada");
		return "redirect:/livros/list";
	}
	
	/***
	 * Metodo que retorna a view os Livros de acordo com o nivel de usuario
	 * 
	 * @return acervo/livros/list_admin; ou acervo/livros/list_funcionario; ou
	 *         acervo/livros/list_aluno;
	 * 
	 *
	 */
	@RequestMapping(value = "/livros/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map, HttpServletRequest request) {
		return  this.getView(map, request);
	}
	
	
	public String  getView(Map<String, Object> map, HttpServletRequest request) {
		
		String role = request.getSession().getAttribute("role").toString();
		map.put("livros", livrosService.listAll());
		map.put("temas", TiposLivros.listTemas());
		map.put("subtemas", TiposLivros.listSubtemas());
		
		map.put("emprestimos", emprestimoService.listEmprestimoEmAberto());
		map.put("alunos", emprestimoService.listAlunoWithoutPendences());
		map.put("tipoItemAcervo", ItemDeAcervo.LIVRO.getValor());
		
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return "acervo/livros/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			return "acervo/livros/list_funcionario";
		}
		else {	
			return "acervo/livros/list_aluno";
		}
	}

	
	
}
