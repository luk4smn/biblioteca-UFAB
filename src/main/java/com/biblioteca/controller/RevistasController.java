/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.ItemDeAcervo;
import com.biblioteca.enums.Roles;
import com.biblioteca.model.Revistas;
import com.biblioteca.service.acervo.revistas.RevistasService;
import com.biblioteca.service.emprestimo.EmprestimoService;


/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/
@Controller
public class RevistasController {
	
	@Autowired
	private RevistasService revistasService;
	
	@Autowired
	private EmprestimoService emprestimoService;
	
	/***
	 * Adiciona um novo item a Revistas
	 * 
	 * @param revista
	 * 
	 * @return "redirect:/revistas/list"
	 *
	 */
	@RequestMapping(value = "/revistas/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("revista") Revistas revista, BindingResult result,
			HttpServletRequest request) {
		
		revistasService.create(revista);

		return "redirect:/revistas/list";
	}
	
	/***
	 * Atualiza um item de Revistas
	 * 
	 * @param revista
	 * 
	 * @return "redirect:/revistas/list"
	 *
	 */
	@RequestMapping(value = "/revistas/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("revista") Revistas revista, BindingResult result,
			HttpServletRequest request) {
		
		revistasService.update(revista);

		return "redirect:/revistas/list";
	}
	

	/***
	 * Remove um item de Revistas
	 * 
	 * @param revista
	 * 
	 * @return "redirect:/revistas/list"
	 *
	 */
	@RequestMapping("/revistas/delete/{id}")
	public String delete(@PathVariable("id") Integer id,
			HttpServletRequest request, ModelMap model) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			revistasService.delete(id);
			return "redirect:/revistas/list";
		}
		
		model.addAttribute("errorMessage", "Permissão negada");
		return "redirect:/revistas/list";
	}
	
	/***
	 * Metodo que retorna a view as Revistas de acordo com o nivel de usuario
	 * 
	 * @return acervo/revistas/list_admin; ou acervo/revistas/list_funcionario; ou
	 *         acervo/revistas/list_aluno;
	 * 
	 *
	 */
	@RequestMapping(value = "/revistas/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map, HttpServletRequest request) {
		return  this.getView(map, request);
	}
	
	
	public String  getView(Map<String, Object> map, HttpServletRequest request) {
		
		String role = request.getSession().getAttribute("role").toString();
		map.put("revistas", revistasService.listAll());
		
		map.put("emprestimos", emprestimoService.listEmprestimoEmAberto());
		map.put("alunos", emprestimoService.listAlunoWithoutPendences());
		map.put("tipoItemAcervo", ItemDeAcervo.REVISTA.getValor());
		
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return "acervo/revistas/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			return "acervo/revistas/list_funcionario";
		}
		else {	
			return "acervo/revistas/list_aluno";
		}
	}
	

}
