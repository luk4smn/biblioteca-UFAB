/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */


package com.biblioteca.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.Roles;
import com.biblioteca.model.ToDo;

@Controller
public class ToDoController {
	
	private static List<ToDo> todos = new ArrayList<ToDo>();
	static {	
		
	}
	
	/***
	 * Listar todos os item na lista de tarefas
	 * 
	 * @return List(Todo)
	 */
	@RequestMapping(value = "/tarefas/list", method = RequestMethod.GET)
	public String retrieveToDos(Map<String, Object> map ,HttpServletRequest request) {	
		
		if(request.getSession().getAttribute("haveToDo") == null) {
			this.reset();
		}	
		
		map.put("todos", todos);
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ALUNO.getValor()))) {
			
			return "/to-do/list-todo-aluno";
			
		}else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))) {
			return "to-do/list-todo";
		}
		
		return "redirect:/home";
	}
	
	/***
	 * Adicionar novo item na lista de tarefas
	 * @param todo Dados do produto: Todo
	 */
	@RequestMapping(value = "/tarefas/create", method = RequestMethod.POST)
	public String addToDo(HttpServletRequest request) {
		
		String name = request.getParameter("name").toString();
		String category = request.getParameter("category").toString();
		
		ToDo todo = new ToDo(name,category);
		
		request.getSession().setAttribute("todos", todos); 
		
		todos.add(todo);
		
		request.getSession().setAttribute("haveToDo","yes");
		
		return "redirect:/tarefas/list";
	}
	
	/***
	 * Deletar produto da item na lista de tarefas
	 * 
	 * @param todo Item a ser deletado: Item
	 */
	@RequestMapping(value = "/tarefas/delete/{index}")
	public String deleteToDo(@PathVariable("index") Integer index) {
			
		this.delete(index);
		
		return "redirect:/tarefas/list";
	}
	
	public void delete(int index) {
		todos.remove(index);
	}
	
	
	private void reset() {
		todos.clear();
	}

}
