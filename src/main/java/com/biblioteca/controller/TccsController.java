/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.ItemDeAcervo;
import com.biblioteca.enums.Roles;
import com.biblioteca.enums.TiposTcc;
import com.biblioteca.model.TrabalhosDeConclusao;
import com.biblioteca.service.acervo.tccs.TCCsService;
import com.biblioteca.service.emprestimo.EmprestimoService;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

@Controller
public class TccsController {
	
	@Autowired
	private TCCsService tccService;
	
	@Autowired
	private EmprestimoService emprestimoService;
	
	/***
	 * Adiciona um novo item a TCCs
	 * 
	 * @param tcc
	 * 
	 * @return "redirect:/tccs/list"
	 *
	 */
	@RequestMapping(value = "/tccs/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("tcc") TrabalhosDeConclusao tcc, BindingResult result,
			HttpServletRequest request) {
		
		tccService.create(tcc);

		return "redirect:/tccs/list";
	}
	
	/***
	 * Adiciona um novo item a TCCs
	 * 
	 * @param tcc
	 * 
	 * @return "redirect:/tccs/list"
	 *
	 */
	@RequestMapping(value = "/tccs/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("tcc") TrabalhosDeConclusao tcc, BindingResult result,
			HttpServletRequest request) {
		
		tccService.update(tcc);

		return "redirect:/tccs/list";
	}
	
	/***
	 * Remove um item de TCCs
	 * 
	 * @param tcc
	 * 
	 * @return "redirect:/tccs/list"
	 *
	 */
	@RequestMapping("/tccs/delete/{id}")
	public String delete(@PathVariable("id") Integer id,
			HttpServletRequest request, ModelMap model) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			tccService.delete(id);
			return "redirect:/tccs/list";
		}
		
		model.addAttribute("errorMessage", "Permissão negada");
		return "redirect:/tccs/list";
	}
	
	/***
	 * Metodo que retorna a view os Livros de acordo com o nivel de usuario
	 * 
	 * @return acervo/tccs/list_admin; ou acervo/tccs/list_funcionario; ou
	 *         acervo/tccs/list_aluno;
	 * 
	 *
	 */
	@RequestMapping(value = "/tccs/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map, HttpServletRequest request) {
		return  this.getView(map, request);
	}
	
	
	public String  getView(Map<String, Object> map, HttpServletRequest request) {
		
		String role = request.getSession().getAttribute("role").toString();
		map.put("tccs", tccService.listAll());
		map.put("tipos", this.listTipos());
		
		
		map.put("emprestimos", emprestimoService.listEmprestimoEmAberto());
		map.put("alunos", emprestimoService.listAlunoWithoutPendences());
		map.put("tipoItemAcervo", ItemDeAcervo.TCC.getValor());
		
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return "acervo/tccs/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			return "acervo/tccs/list_funcionario";
		}
		else {	
			return "acervo/tccs/list_aluno";
		}
	}


	private  List<Object> listTipos() {
		ArrayList<Object> tipos = new ArrayList<Object>();

		tipos.add(new Object[] {	TiposTcc.DISSERTACAO.getValor(), "DISSERTACAO"	});
		tipos.add(new Object[] {	TiposTcc.MONOGRAFIA.getValor(), "MONOGRAFIA"	});
		tipos.add(new Object[] {	TiposTcc.TESE.getValor(), "TESE"	});
			
		return tipos;
	}
}
