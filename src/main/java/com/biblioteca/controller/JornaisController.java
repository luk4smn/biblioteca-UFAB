/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.ItemDeAcervo;
import com.biblioteca.enums.Roles;
import com.biblioteca.model.Jornais;
import com.biblioteca.service.acervo.jornais.JornaisService;
import com.biblioteca.service.emprestimo.EmprestimoService;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

@Controller
public class JornaisController {
	
	@Autowired
	private JornaisService jornaisService;
	
	@Autowired
	private EmprestimoService emprestimoService;
		
	/***
	 * Adiciona um novo item a Jornais
	 * 
	 * @param jornal
	 * 
	 * @return "redirect:/jornais/list"
	 *
	 */
	@RequestMapping(value = "/jornais/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("jornal") Jornais jornal, BindingResult result,
			HttpServletRequest request) {
		
		jornaisService.create(jornal);

		return "redirect:/jornais/list";
	}
	

	/***
	 * Atualiza um item de Jornais
	 * 
	 * @param jornal
	 * 
	 * @return "redirect:/jornais/list"
	 *
	 */
	@RequestMapping(value = "/jornais/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("jornal") Jornais jornal, BindingResult result,
			HttpServletRequest request) {
		
		jornaisService.update(jornal);

		return "redirect:/jornais/list";
	}
	
	/***
	 * Remove um item de Jornais
	 * 
	 * @param jornal
	 * 
	 * @return "redirect:/jornais/list"
	 *
	 */
	@RequestMapping("/jornais/delete/{id}")
	public String delete(@PathVariable("id") Integer id,
			HttpServletRequest request, ModelMap model) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			jornaisService.delete(id);
			return "redirect:/jornais/list";
		}
		
		model.addAttribute("errorMessage", "Permissão negada");
		return "redirect:/jornais/list";
	}
	
	/***
	 * Metodo que retorna a view os Jornais de acordo com o nivel de usuario
	 * 
	 * @return acervo/jornais/list_admin; ou acervo/jornais/list_funcionario; ou
	 *         acervo/jornais/list_aluno;
	 *
	 */
	@RequestMapping(value = "/jornais/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map, HttpServletRequest request) {
		return  this.getView(map, request);
	}
	
	
	public String  getView(Map<String, Object> map, HttpServletRequest request) {
		
		String role = request.getSession().getAttribute("role").toString();
		map.put("jornais", jornaisService.listAll());
		
		map.put("emprestimos", emprestimoService.listEmprestimoEmAberto());
		map.put("alunos", emprestimoService.listAlunoWithoutPendences());
		map.put("tipoItemAcervo", ItemDeAcervo.JORNAL.getValor());
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return "acervo/jornais/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			return "acervo/jornais/list_funcionario";
		}
		else {	
			return "acervo/jornais/list_aluno";
		}
	}
}
