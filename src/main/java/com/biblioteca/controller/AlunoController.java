/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.Roles;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Curso;
import com.biblioteca.service.aluno.AlunoService;
import com.biblioteca.service.curso.CursoService;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

@Controller
public class AlunoController {
	
	@Autowired
	private AlunoService alunoService;
	
	@Autowired
	private CursoService cusoService;
	
	/***
	 * Adiciona um novo aluno a base de dados
	 * 
	 * @param aluno
	 *            Classe Aluno
	 */
	@RequestMapping(value = "/alunos/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("aluno") Aluno aluno, BindingResult result,
			HttpServletRequest request,  ModelMap model) {
		
		Aluno validator = alunoService.validate(aluno.getCpf());
		
		if (validator != null) {
			model.addAttribute("errorMessage", "Esse CPF já está cadastrado.");
			
			return this.getAlunosView(model, request);
		}
		
		Integer qtdAlunos = alunoService.count();
		
		String stringCodigo = this.generateCodAluno(qtdAlunos);
			
		String codigoCurso = this.getCodigoCurso(aluno.getCurso());
		
		String nivel = "X";
		
		if(aluno.getNivel() != null) {
			nivel = aluno.getNivel();
		}
		
		String matricula = nivel + codigoCurso + "-" +
						   aluno.getAno().substring(2, 4) + 
						   aluno.getPeriodo() + 
						   stringCodigo;
		
		aluno.setMatricula(matricula);
		
		alunoService.create(aluno);

		return "redirect:/alunos/list";
}
	
	/***
	 * Metodo para editar os dados do aluno
	 * 
	 * @param aluno
	 *            Aluno editado: Classe Aluno
	 */
	@RequestMapping(value = "/alunos/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("aluno") Aluno aluno, BindingResult result,
			HttpServletRequest request) {
						
		alunoService.update(aluno);

		return "redirect:/alunos/list";
	}

	
	/***
	 * Deleta um aluno da base de dados (e todos os dados relacionados a ele)
	 * 
	 * @param aluno
	 *            Classe Aluno
	 */
	@RequestMapping("/alunos/delete/{id}")
	public String delete(@PathVariable("id") Integer id,
			HttpServletRequest request, ModelMap model) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			alunoService.delete(id);
			return "redirect:/alunos/list";
		}
		
		model.addAttribute("errorMessage", "Permissão negada");
		return "redirect:/alunos/list";
	}
	
	/***
	 * Metodo que lista as informações dos alunos
	 * 
	 * @return Dados do aluno: Classe Aluno
	 */
	@RequestMapping(value = "/alunos/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map, HttpServletRequest request) {		
		return  this.getAlunosView(map, request);
	}

	
	/***
	 * Método que edita as informações dos alunos
	 *
	 */
	@RequestMapping(value = "/alunos/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id")	Integer id,  Map<String, Object> map, HttpServletRequest request) {
		return  this.getAlunosEditView(map, request, id);
	}
	
	/***
	 * Método que retorna a view para listar das informações dos alunos
	 *
	 */
	public String  getAlunosView(Map<String, Object> map, HttpServletRequest request) {
		
		String role 	= request.getSession().getAttribute("role").toString();
		int aluno_id 	= Integer.parseInt(request.getSession().getAttribute("reference").toString());
				
		map.put("cursos", cusoService.listAll());
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			map.put("alunos", alunoService.listAll());
			return "alunos/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			map.put("alunos", alunoService.listAll());
			return "alunos/list_funcionario";
		}
		else {	
			map.put("aluno", alunoService.getAluno(aluno_id));
			return "alunos/list_aluno";
		}
	}
	
	/***
	 * Método que retorna a view para edição das informações dos alunos
	 *
	 */
	public String  getAlunosEditView(Map<String, Object> map, HttpServletRequest request, int id) {
		
		String role	= request.getSession().getAttribute("role").toString();
		
		map.put("aluno", alunoService.getAluno(id));
		map.put("cursos", cusoService.listAll());
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return "alunos/edit";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			return "alunos/edit";
		}
		else {	
			return "alunos/edit_aluno";
		}
	}

		
	/***
	 * Define o codigo do curso para ser utilizado na matricula do aluno.
	 * se o curso não existir retorna XX.
	 * @param curso Curso do aluno: String
	 * @return Código do curso: String
	 */
	public String getCodigoCurso(String curso) {
		switch(curso) {
		case "Administração":
			return "AD";
		case "Ciência da Computação":
			return "CP";
		case "Direito":
			return "DI";
		case "Engenharia Elétrica":
			return "EE";
		case "Engenharia Espacial":
			return "EP";
		case "Engenharia Mecatrônica":
			return "EM";
		case "Matemática":
			return "MA";
		case "Medicina":
			return "ME";
		case "Nutrição":
			return "NU";
		case "Odontologia":
			return "OD";
		case "Psicologia":
			return "PS";
		case "Veterinária":
			return "VE";
		}
		return "XX";		
	}
	
	/***
	 * M�todo para gerar o código do aluno
	 * 
	 */
	private String generateCodAluno(Integer qtdAlunos) {
		
		String codigo = "";
		
		if(qtdAlunos >= 100) {	
			codigo = qtdAlunos.toString();
		}
		else if(qtdAlunos < 10) {
			codigo = "00" + qtdAlunos.toString();
		}		
		else {
			codigo = "0" + qtdAlunos.toString();
		}
				
		return codigo;
	}
	


}
