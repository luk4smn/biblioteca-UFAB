/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.dao.acervo.jornais.JornaisDAO;
import com.biblioteca.enums.ItemDeAcervo;
import com.biblioteca.enums.Roles;
import com.biblioteca.enums.TiposNivelAluno;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Emprestimos;
import com.biblioteca.model.Reservas;
import com.biblioteca.service.acervo.anais.AnaisDeCongressoService;
import com.biblioteca.service.acervo.livros.LivrosService;
import com.biblioteca.service.acervo.midias.MidiasService;
import com.biblioteca.service.acervo.revistas.RevistasService;
import com.biblioteca.service.acervo.tccs.TCCsService;
import com.biblioteca.service.aluno.AlunoService;
import com.biblioteca.service.email.Mailer;
import com.biblioteca.service.email.Mensagem;
import com.biblioteca.service.emprestimo.EmprestimoService;
import com.biblioteca.service.reservas.ReservasService;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

@Controller
public class EmprestimoController {
	
	@Autowired
	private EmprestimoService emprestimoService;
	@Autowired
	private ReservasService reservaService;
	@Autowired
	private Mailer mailService;
	
	
	@Autowired
	private AlunoService alunoService;
	@Autowired
	private LivrosService livroService;
	@Autowired
	private RevistasService revistaService;
	@Autowired
	private JornaisDAO jornalService;
	@Autowired
	private AnaisDeCongressoService analService;
	@Autowired
	private TCCsService tccService;
	@Autowired
	private MidiasService midiaService;
	
	/***
	 * Cria uma nova solicitacao de emprestimo
	 * 
	 * @param dias,
	 *            alunoId
	 * 
	 * @return "redirect:/emprestimos/list"
	 *
	 */
	@RequestMapping(value = "/emprestimos/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("emprestimo") Emprestimos emprestimo, 
			BindingResult result, HttpServletRequest request) {
		
		int dias 		= Integer.parseInt(request.getParameter("dias"));
		int alunoId 	= Integer.parseInt(request.getParameter("alunoId"));
		
		Aluno aluno = alunoService.getAluno(alunoId);
						
		String nivel = aluno.getNivel();
		
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    
	    Date currentDate = new Date();
	    	    
	    Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, dias);
        
        Date currentDatePlusOne = c.getTime();
        
        emprestimo.setDataRetirada(dateFormat.format(currentDate));
        emprestimo.setDataDevolucao(dateFormat.format(currentDatePlusOne));
  

		if((nivel.equals(TiposNivelAluno.GRADUACAO.getString())) && dias >15) {
		
			String referer = request.getHeader("Referer");
			request.setAttribute("errorMessage", "O aluno não pode ter um empestimo que dure mais que 15 dias");
		    return "redirect:"+ referer;
		}
		
		emprestimoService.create(emprestimo);
		

		return "redirect:/emprestimos/list";
	}
	
	/***
	 * Renova uma solicitacao de emprestimo
	 * 
	 * @param dias,
	 *            alunoId
	 * 
	 * @return "redirect:/emprestimos/list"
	 *
	 */
	@RequestMapping(value = "/emprestimos/renovar", method = RequestMethod.POST)
	public String renovar(@ModelAttribute("emprestimo") Emprestimos emprestimo, BindingResult result,
			HttpServletRequest request) {
		
		Reservas reserva = reservaService.getFirstReserva(emprestimo.getTipoItem(), emprestimo.getItemId());
		
		if(reserva == null) {
			int dias   = Integer.parseInt(request.getParameter("dias"));
			
			Aluno aluno = alunoService.getAluno(emprestimo.getId());
			
			String nivel = aluno.getNivel();
			
		    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		    
		    Date currentDate = new Date();
		    	    
		    Calendar c = Calendar.getInstance();
	        c.setTime(currentDate);
	        c.add(Calendar.DATE, dias);
	        
	        Date currentDatePlusOne = c.getTime();
	                
	        emprestimo.setDataDevolucao(dateFormat.format(currentDatePlusOne));
	  
			if((nivel.equals(TiposNivelAluno.GRADUACAO.getString())) && dias >15) {
				String referer = request.getHeader("Referer");
				request.setAttribute("errorMessage", "O aluno não pode renovar por mais que 15 dias");
			    return "redirect:"+ referer;
			}
			
			emprestimoService.update(emprestimo);

			return "redirect:/emprestimos/list";
		}
			
		String referer = request.getHeader("Referer");
		request.setAttribute("errorMessage", "Existe uma reserva para esse item, não é possivel renovar");
	    return "redirect:"+ referer;
	
	}
	
	/***
	 * Encerra um emprestimo com a devolucao do item
	 * 
	 * @param dias,
	 *            alunoId
	 * 
	 * @return "redirect:/emprestimos/list"
	 *
	 */
	@RequestMapping(value = "/emprestimos/devolver", method = RequestMethod.POST)
	public String devolver(@ModelAttribute("emprestimo") Emprestimos emprestimo, BindingResult result,
			HttpServletRequest request) {
			
		Emprestimos updatedEmp = emprestimoService.getEmprestimo(emprestimo.getId());
		
		updatedEmp.setEntregue(1);
		
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    
	    Date currentDate = new Date();
	    
	    updatedEmp.setDataDevolucao(dateFormat.format(currentDate));
		
	    emprestimoService.update(updatedEmp);
	    
	    this.notifyAluno(updatedEmp.getTipoItem(), updatedEmp.getItemId());

		return "redirect:/emprestimos/list";
	}
		
	/***
	 * Remove um emprestimo
	 * 
	 * @return "redirect:/emprestimos/list"
	 *
	 */
	@RequestMapping("/emprestimos/delete/{id}")
	public String delete(@PathVariable("id") Integer id,
			HttpServletRequest request, ModelMap model) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			emprestimoService.delete(id);
			return "redirect:/emprestimos/list";
		}
		
		model.addAttribute("errorMessage", "Permissão negada");
		return "redirect:/emprestimos/list";
	}
	
	/***
	 * Metodo que retorna a view os emprestimos de acordo com o nivel de usuario
	 * 
	 * @return emprestimos/list_admin; ou emprestimos/list_funcionario; ou
	 *         emprestimos/list_aluno;
	 *
	 */
	@RequestMapping(value = "/emprestimos/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map, HttpServletRequest request) {
		return  this.getView(map, request);
	}
	
	
	public String  getView(Map<String, Object> map, HttpServletRequest request) {
		
		String role = request.getSession().getAttribute("role").toString();
		map.put("emprestimos", emprestimoService.listAll());
		map.put("tipos", ItemDeAcervo.getTipos());
		map.put("pendencias", emprestimoService.calculatePendences());
		
		map.put("livros", livroService.listAll());
		map.put("revistas", revistaService.listAll());
		map.put("jornais", jornalService.listAll());
		map.put("anais", analService.listAll());
		map.put("tccs", tccService.listAll());
		map.put("midias", midiaService.listAll());
		
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			map.put("alunos", alunoService.listAll());
			
			return "emprestimos/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			map.put("alunos", alunoService.listAll());
			
			return "emprestimos/list_funcionario";
		}
		else {	
			int aluno_id = Integer.parseInt(request.getSession().getAttribute("reference").toString());
			
			map.put("aluno", alunoService.getAluno(aluno_id));
			
			return "emprestimos/list_aluno";
		}
	}
	
	public void notifyAluno(int tipoItem, int itemId) {
		Reservas reserva = reservaService.getFirstReserva(tipoItem, itemId);
		
		if(reserva != null && reserva.getSinalizar() == 1) {
			Aluno alunoReserva = alunoService.getAluno(reserva.getAlunoId());
			
			if(alunoReserva.getEmail() != null) {
				mailService.enviar(new Mensagem("UFAB <contact.ufab@gmail.com>", 
						Arrays.asList("ALUNO <"+alunoReserva.getEmail()+">")
						, "Reserva de item", "Olá! \n\n O seu item que foi reservado já está disponível"));
				
		    }
		}
	}
	
}
