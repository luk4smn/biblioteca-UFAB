/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;

import java.util.Map;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.biblioteca.enums.ItemDeAcervo;
import com.biblioteca.enums.Roles;
import com.biblioteca.service.acervo.livros.LivrosService;
import com.biblioteca.service.aluno.AlunoService;
import com.biblioteca.service.emprestimo.EmprestimoService;
import com.biblioteca.service.relatorios.RelatorioService;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/
@Controller
public class RelatorioController {

	@Autowired
	private RelatorioService relatorioService;
	
	@Autowired
	private EmprestimoService emprestimoService;
	@Autowired
	private LivrosService livroService;
	@Autowired
	private AlunoService alunoService;
	
	/***
	 * Metodo para requisicao de relatorios
	 * 
	 * @return relatorios
	 *
	 */
	@RequestMapping(value = "/relatorios")
	public String index(HttpServletRequest request) {
		return this.getView(request, "relatorios");
		
	}

	/***
	 * Metodo para requisicao de relatorios de alunos
	 * 
	 * @return relatorios/alunos
	 *
	 */
	@RequestMapping(value = "/relatorios/alunos/list")
	public String relatorioAluno(HttpServletRequest request, Map<String, Object> map) {
		map.put("alunos", relatorioService.listAlunoWithPendences());
		
		return this.getView(request, "relatorios/alunos");
		
	}
	
	/***
	 * Metodo para requisicao de relatorios de livros
	 * 
	 * @return relatorios/livros
	 *
	 */
	@RequestMapping(value = "/relatorios/livros/list")
	public String relatorioLivros(HttpServletRequest request, Map<String, Object> map) {
		
		map.put("emprestimos", relatorioService.listAllLivros());
		map.put("tipos", ItemDeAcervo.getTipos());
		map.put("pendencias", emprestimoService.calculatePendences());
		
		map.put("livros", livroService.listAll());
		map.put("alunos", alunoService.listAll());
		
		return this.getView(request, "relatorios/livros");
		
	}
	
	/***
	 * Metodo que obtem a view de acordo com a role de ADMIN
	 * 
	 * @return view ou redirect:/home
	 *
	 */
	public String getView(HttpServletRequest request, String viewType) {

		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return viewType+"/list";
		}
		
		return "redirect:/home";
	}
}
