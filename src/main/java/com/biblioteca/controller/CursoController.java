/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.Roles;
import com.biblioteca.model.Curso;
import com.biblioteca.service.curso.CursoService;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

@Controller
public class CursoController {
		
	@Autowired
	private CursoService cursoService;
	
	/***
	 * Adiciona um novo curso a base de dados
	 * @param curso Classe Curso
	 */
	@RequestMapping(value = "/cursos/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("curso") Curso curso, BindingResult result,
			HttpServletRequest request, ModelMap model) {
		
		Curso validator = cursoService.validate(curso.getNome(), curso.getTipo());
		
		if (validator != null) {
			model.addAttribute("errorMessage", "Esse curso já existe.");
			
			return this.getView(model, request);
		}
		
		cursoService.create(curso);

		return "redirect:/cursos/list";
	}	
	
	
	/***
	 * Método para editar os dados do curso
	 * @param curso Curso editado: Classe Curso
	 */
	@RequestMapping(value = "/cursos/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("curso") Curso curso, BindingResult result,
			HttpServletRequest request) {
						
		cursoService.update(curso);

		return "redirect:/cursos/list";
	}

	
	/***
	 * Deleta um aluno da base de dados
	 * @param aluno Classe Aluno
	 */
	@RequestMapping("/cursos/delete/{id}")
	public String delete(@PathVariable("id") Integer id,
			HttpServletRequest request, ModelMap model) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			cursoService.delete(id);
			return "redirect:/cursos/list";
		}
		
		model.addAttribute("errorMessage", "Permissão negada");
		return "redirect:/cursos/list";
	}
	
	/***
	 * Método que lista as informações dos alunos
	 * @return Dados do aluno: Classe Aluno
	 */
	@RequestMapping(value = "/cursos/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map, HttpServletRequest request) {
		return  this.getView(map, request);
	}

	
	/***
	 * Método que edita as informações dos alunos
	 *
	 */
	@RequestMapping(value = "/cursos/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id")	Integer id,  Map<String, Object> map, HttpServletRequest request) {
		return  this.getEditView(map, request, id);
	}
	
	/***
	 * Método que retorna a view para listar das informações dos alunos
	 *
	 */
	public String  getView(Map<String, Object> map, HttpServletRequest request) {
		
		String role = request.getSession().getAttribute("role").toString();
		map.put("cursos", cursoService.listAll());
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return "cursos/list";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			return "cursos/list_funcionario";
		}
		else {	
			return "redirect:/home";
		}
	}
	
	/***
	 * Método que retorna a view para edição das informações dos alunos
	 *
	 */
	public String  getEditView(Map<String, Object> map, HttpServletRequest request, int id) {
		
		String role	= request.getSession().getAttribute("role").toString();
		map.put("curso", cursoService.getCurso(id));
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return "cursos/edit_admin";
		}		
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			return "cursos/edit_funcionario";
		}		
		else {	
			return "redirect:/home";
		}
	}
	

}
