/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.Roles;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Funcionario;
import com.biblioteca.service.funcionario.FuncionarioService;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

@Controller
public class FuncionariosController {
	
	@Autowired
	private FuncionarioService funcionarioService;
	
	/***
	 * Adiciona um novo funcionario a base de dados
	 * 
	 * @param funcionario Classe Funcionario
	 */
	@RequestMapping(value = "/funcionarios/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("funcionario") Funcionario funcionario, BindingResult result,
			HttpServletRequest request, ModelMap model) {
		
		Funcionario validator = funcionarioService.validate(funcionario.getCpf());
		
		if (validator != null) {
			model.addAttribute("errorMessage", "Esse CPF já está cadastrado.");
			
			return this.getView(model, request);
		}
		
		funcionarioService.create(funcionario);

		return "redirect:/funcionarios/list";
	}	
	
	
	/***
	 * Método para editar os dados do funcionario
	 * 
	 * @param funcionario Funcionario editado: Classe Funcionario
	 */
	@RequestMapping(value = "/funcionarios/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("funcionario") Funcionario funcionario, BindingResult result,
			HttpServletRequest request) {
						
		funcionarioService.update(funcionario);

		return "redirect:/funcionarios/list";
	}

	
	/***
	 * Deleta um aluno da base de dados
	 * @param id : Classe Funcionario
	 */
	@RequestMapping("/funcionarios/delete/{id}")
	public String delete(@PathVariable("id") Integer id,
			HttpServletRequest request, ModelMap model) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			funcionarioService.delete(id);
			return "redirect:/funcionarios/list";
		}
		
		model.addAttribute("errorMessage", "Permissão negada");
		return "redirect:/funcionarios/list";
	}
	
	/***
	 * Método que lista as informações dos funcionarios
	 * 
	 * @return Dados do funcionario: Classe Funcionario
	 */
	@RequestMapping(value = "/funcionarios/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map, HttpServletRequest request) {
		return  this.getView(map, request);
	}

	
	/***
	 * Método que edita as informações dos funcionarios
	 *
	 */
	@RequestMapping(value = "/funcionarios/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id")	Integer id,  Map<String, Object> map, HttpServletRequest request) {
		return  this.getEditView(map, request, id);
	}
	
	/***
	 * Método que retorna a view para listar das informações dos funcionarios
	 *
	 */
	public String  getView(Map<String, Object> map, HttpServletRequest request) {
		
		String role 		= request.getSession().getAttribute("role").toString();
		int funcionario_id	= Integer.parseInt(request.getSession().getAttribute("reference").toString());
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			map.put("funcionarios", funcionarioService.listAll());
			return "funcionarios/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			map.put("funcionario", funcionarioService.getFuncionario(funcionario_id));
			return "funcionarios/list_funcionario";
		}
		else {	
			return "redirect:/home";
		}
	}
	
	/***
	 * Método que retorna a view para edição das informações dos funcionarios
	 *
	 */
	public String  getEditView(Map<String, Object> map, HttpServletRequest request, int id) {
		
		String role	= request.getSession().getAttribute("role").toString();
		map.put("funcionario", funcionarioService.getFuncionario(id));
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return "funcionarios/edit";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			return "funcionarios/edit_funcionario";
		}
		else {	
			return "redirect:/home";
		}
	}
}
