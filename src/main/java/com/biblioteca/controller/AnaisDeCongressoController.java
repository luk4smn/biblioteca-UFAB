/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */

package com.biblioteca.controller;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.enums.ItemDeAcervo;
import com.biblioteca.enums.Roles;
import com.biblioteca.enums.TiposAnaisDeCongresso;
import com.biblioteca.enums.TiposLivros;
import com.biblioteca.model.AnaisDeCongresso;
import com.biblioteca.service.acervo.anais.AnaisDeCongressoService;
import com.biblioteca.service.emprestimo.EmprestimoService;

/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

@Controller
public class AnaisDeCongressoController {
	
	@Autowired
	private AnaisDeCongressoService anaisService;
	
	@Autowired
	private EmprestimoService emprestimoService;
		
	/***
	 * Adiciona um novo item a Anais de Congresso
	 * 
	 * @param anal
	 * 
	 * @return "redirect:/anais/list"
	 *
	 */
	@RequestMapping(value = "/anais/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("anal") AnaisDeCongresso anal, BindingResult result,
			HttpServletRequest request) {
		
		anaisService.create(anal);

		return "redirect:/anais/list";
	}
	
	
	/***
	 * Atualiza um item de Anais de Congresso
	 * 
	 * @param anal
	 * 
	 * @return "redirect:/anais/list"
	 *
	 */
	@RequestMapping(value = "/anais/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("anal") AnaisDeCongresso anal, BindingResult result,
			HttpServletRequest request) {
						
		anaisService.update(anal);

		return "redirect:/anais/list";
	}
	
	/***
	 * Remove um item de Anais de Congresso
	 * 
	 * @param anal
	 * 
	 * @return "redirect:/anais/list"
	 *
	 */
	@RequestMapping("/anais/delete/{id}")
	public String delete(@PathVariable("id") Integer id,
			HttpServletRequest request, ModelMap model) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			anaisService.delete(id);
			return "redirect:/anais/list";
		}
		
		model.addAttribute("errorMessage", "Permissão negada");
		return "redirect:/anais/list";
	}
	
	/***
	 * Metodo que retorna a view os Anais de Congresso de acordo com o nivel de
	 * usuario
	 * 
	 * @return acervo/anais/list_admin; ou acervo/anais/list_funcionario; ou
	 *         acervo/anais/list_aluno;
	 *
	 */
	@RequestMapping(value = "/anais/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map, HttpServletRequest request) {
		return  this.getView(map, request);
	}
	
	
	public String  getView(Map<String, Object> map, HttpServletRequest request) {
		
		String role = request.getSession().getAttribute("role").toString();
		map.put("anais", anaisService.listAll());
		map.put("tipos", this.listTipos());
		
		map.put("emprestimos", emprestimoService.listEmprestimoEmAberto());
		map.put("alunos", emprestimoService.listAlunoWithoutPendences());
		map.put("tipoItemAcervo", ItemDeAcervo.ANAL_DE_CONGRESSO.getValor());
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			return "acervo/anais/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			return "acervo/anais/list_funcionario";
		}
		else {	
			return "acervo/anais/list_aluno";
		}
	}
	
	public List<Object> listTipos() {
		
		ArrayList<Object> tipos = new ArrayList<Object>();

		tipos.add(new Object[] {	TiposAnaisDeCongresso.ARTIGO.getValor(), "ARTIGO"	});
		tipos.add(new Object[] {	TiposAnaisDeCongresso.POSTER.getValor(), "POSTER"	});
		tipos.add(new Object[] {	TiposAnaisDeCongresso.RESUMO.getValor(), "RESUMO"	});
			
		return tipos;
	}
	
	
}
