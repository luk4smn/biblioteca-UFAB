/* 
 * Componente Curricular: Programação WEB
 * Professor: Thiago Santana Batista
 * Alunos: Lucas Martins, Lanmark Rafael, Higor Pereira
 * Sprint 3
 */
package com.biblioteca.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.biblioteca.dao.acervo.jornais.JornaisDAO;
import com.biblioteca.enums.ItemDeAcervo;
import com.biblioteca.enums.Roles;
import com.biblioteca.enums.TiposNivelAluno;
import com.biblioteca.model.Aluno;
import com.biblioteca.model.Emprestimos;
import com.biblioteca.model.Reservas;
import com.biblioteca.service.acervo.anais.AnaisDeCongressoService;
import com.biblioteca.service.acervo.livros.LivrosService;
import com.biblioteca.service.acervo.midias.MidiasService;
import com.biblioteca.service.acervo.revistas.RevistasService;
import com.biblioteca.service.acervo.tccs.TCCsService;
import com.biblioteca.service.aluno.AlunoService;
import com.biblioteca.service.emprestimo.EmprestimoService;
import com.biblioteca.service.reservas.ReservasService;


/***
 * 
 * @author Lucas Martins, Lanmark Rafael, Higor Pereira
 *
 **/

@Controller
public class ReservasController {

	@Autowired
	private ReservasService reservaService;
	@Autowired
	private EmprestimoService emprestimoService;
	
	@Autowired
	private AlunoService alunoService;
	@Autowired
	private LivrosService livroService;
	@Autowired
	private RevistasService revistaService;
	@Autowired
	private JornaisDAO jornalService;
	@Autowired
	private AnaisDeCongressoService analService;
	@Autowired
	private TCCsService tccService;
	@Autowired
	private MidiasService midiaService;
	
	
	
	
	/***
	 * Adiciona um novo item a Reservas
	 * 
	 * @param reserva
	 * 
	 * @return "redirect:/reservas/list"
	 *
	 */
	@RequestMapping(value = "/reservas/create", method = RequestMethod.POST)
	public String create(@ModelAttribute("reserva") Reservas reserva, 
			BindingResult result, HttpServletRequest request) {
		
		int alunoId = Integer.parseInt(request.getParameter("alunoId"));
		
		Aluno aluno = alunoService.getAluno(alunoId);
							
		reservaService.create(reserva);
		
		return "redirect:/reservas/list";
	}
	
	/***
	 * Transforma a reserva em emprestimo
	 * 
	 * @param revista
	 * 
	 * @return "redirect:/emprestimos/list"
	 *
	 */
	@RequestMapping(value = "/reservas/transform", method = RequestMethod.POST)
	public String renovar(@ModelAttribute("reserva") Reservas reserva, BindingResult result,
			HttpServletRequest request) {
		
		Reservas reservaData = reservaService.getReserva(reserva.getId());
		
		int dias 		= Integer.parseInt(request.getParameter("dias"));
				
		Aluno aluno = alunoService.getAluno(reservaData.getAlunoId());
						
		String nivel = aluno.getNivel();
		
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    
	    Date currentDate = new Date();
	    	    
	    Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, dias);
        
        Date currentDatePlusOne = c.getTime();
        
        Emprestimos emprestimo = new Emprestimos();
        
        emprestimo.setAlunoId(reservaData.getAlunoId());
        emprestimo.setItemId(reservaData.getItemId());
        emprestimo.setTipoItem(reservaData.getTipoItem());
        
        emprestimo.setDataRetirada(dateFormat.format(currentDate));
        emprestimo.setDataDevolucao(dateFormat.format(currentDatePlusOne));
  

		if((nivel.equals(TiposNivelAluno.GRADUACAO.getString())) && dias >15) {
		
			String referer = request.getHeader("Referer");
			request.setAttribute("errorMessage", "O aluno não pode ter um empestimo que dure mais que 15 dias");
		    return "redirect:"+ referer;
		}
		
		emprestimoService.create(emprestimo);
		

		return "redirect:/emprestimos/list";
	}
	
	
	
	/***
	 * Remove um item de Reservas
	 * 
	 * @param reserva
	 * 
	 * @return "redirect:/reservas/list"
	 *
	 */
	@RequestMapping("/reservas/delete/{id}")
	public String delete(@PathVariable("id") Integer id,
			HttpServletRequest request, ModelMap model) {
		
		String role = request.getSession().getAttribute("role").toString();
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor())) 
				|| 	role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))  ) {
			
			reservaService.delete(id);
			
			return "redirect:/reservas/list";
		}
		
		model.addAttribute("errorMessage", "Permissão negada");
		return "redirect:/home";
	}
	
	
	@RequestMapping(value = "/reservas/list", method = RequestMethod.GET)
	public String list(Map<String, Object> map, HttpServletRequest request) {
		return  this.getView(map, request);
	}
	
	
	public String  getView(Map<String, Object> map, HttpServletRequest request) {
		
		String role = request.getSession().getAttribute("role").toString();
		map.put("reservas", reservaService.listAll());
		map.put("tipos", ItemDeAcervo.getTipos());
		map.put("alunos", alunoService.listAll());
		map.put("alunosSemPendencias", emprestimoService.listAlunoWithoutPendences());
		map.put("emprestimosEmAberto", emprestimoService.listEmprestimoEmAberto());
		
		map.put("livros", livroService.listAll());
		map.put("revistas", revistaService.listAll());
		map.put("jornais", jornalService.listAll());
		map.put("anais", analService.listAll());
		map.put("tccs", tccService.listAll());
		map.put("midias", midiaService.listAll());
		
		
		if (role.equals(Integer.toString(Roles.ADMIN.getValor()))) {
			
			
			return "reservas/list_admin";
		}			
		else if(role.equals(Integer.toString(Roles.FUNCIONARIO.getValor()))){
			
			
			return "reservas/list_funcionario";
		}
		
		else return "redirect:/home";
		
	}
	
	
}
