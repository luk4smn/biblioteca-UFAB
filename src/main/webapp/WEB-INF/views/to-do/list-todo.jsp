<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_funcionario.jspf"%>

	<div class="container">
		<H1>Bem vindo, ${name}.</H1>
		
		<div class="alert alert-info">
 			<p> <b>Info:</b> Anote aqui as suas <u>tarefas</u>.</p> 
 			<p>Elas ser�o armazenadas at� que vc realize o logout no sistema</p>
		</div>
		
		<table class="table table-striped">
		<caption>Voc� Possui as Seguintes Tarefas</caption>
			<thead>
				<th>Descri��o</th>
				<th>Categoria</th>
				<th>Ac��es</th>
			</thead>
			<tbody>
			<c:forEach items="${todos}" var="todo" varStatus="loop">
				<tr>
					<td>${todo.name}</td>
					<td>${todo.category}</td>
					<td>
					<a class="btn btn-danger"  
						onclick="return confirm('Confirmar exclus�o?')"
						href="/tarefas/delete/${loop.index}">Deletar
					</a>
					</td> 
				</tr>
			</c:forEach>
			</tbody>
		</table>

		<p>
			<font color="red">${errorMessage}</font>
		</p>
			<button class="btn btn-primary" data-toggle="modal" data-target="#modalToDo"><i class="fa fa-plus-circle"></i> Adicionar Novo </button>
		
	</div>

	<!-- Modal Aluno -->
	<%@ include file="add-todo.jsp" %>
	
<%@ include file="../common/footer.jspf"%>