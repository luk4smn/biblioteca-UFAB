	<div id="modalToDo" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
	
			<form:form method="POST" action="/tarefas/create">
				<div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Cadastro de Tarefas</h4>
			      </div>
			      
			      <div class="modal-body">
	       
				<fieldset class="form-group">
					<label>Descri��o</label>
					<input name="name" type="text" class="form-control"/>
				</fieldset>
				<fieldset class="form-group">
					<label>Categoria</label>
					<input name="category" type="text" class="form-control"/>
				</fieldset> 
				
				</div>
				
				<div class="modal-footer">
				<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		      	</div>
		      	
		     	</div>
			</form:form>
		</div>
	
	</div>
