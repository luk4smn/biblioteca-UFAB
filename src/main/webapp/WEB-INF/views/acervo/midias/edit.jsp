	<div id="modalEdit" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/midias/update" commandName="midia">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de Midias Eletronicas </h4>
		      </div>
		      <div class="modal-body">
				


			<script type="text/javascript">
			$('.edit-from-table').on('click', function () {
			    let id = $(this).attr('data-id');
			    let titulo = $(this).attr('data-titulo');
			    let dataDeGravacao = $(this).attr('data-dataDeGravacao');
			    let tipoMidia = $(this).attr('data-tipoMidia');
			     
			    document.getElementById('id').value = id;
			    document.getElementById('titulo').value = titulo;
			    document.getElementById('dataDeGravacao').value = dataDeGravacao;
			    document.getElementById('tipoMidia').value = tipoMidia;
			   
			});
			
			</script>
   				<input name="id" id="id" type="hidden" class="form-control"/>

					<fieldset class="form-group">
						<label>T�tulo</label>
						<input name="titulo" id="titulo" type="text" class="form-control" required/>
					</fieldset>
					
					<fieldset class="form-group">
						<label>Data De Gravacao</label>
						<input name="dataDeGravacao" id="dataDeGravacao" type="date" class="form-control" required/>
					</fieldset>  
										
					<fieldset>
						<label for="tipoMidia">Area de Conhecimento</label>
						  <select class="form-control" name="tipoMidia" id="tipoMidia" required>
						   	  <c:forEach items="${tipos}" var="tipo">
								<option value="${tipo[0]}">${tipo[1]}</option>
							  </c:forEach>
						  </select>
					</fieldset><br>
																			
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Atualizar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>
  	