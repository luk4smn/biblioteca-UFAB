	<div id="modalCreate" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/midias/create" commandName="midia">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de Midias Eletronicas </h4>
		      </div>
		      
		      
		      <div class="modal-body">
			
					<fieldset class="form-group">
						<label>T�tulo</label>
						<input name="titulo" type="text" class="form-control" required/>
					</fieldset>

					<fieldset class="form-group">
						<label>Data De Publicacao</label>
						<input name="dataDeGravacao" type="date" class="form-control" required/>
					</fieldset> 
					
					<fieldset>
						<label>Tipo</label>
						  <select class="form-control" name="tipoMidia" required>
						  	<c:forEach items="${tipos}" var="tipo">
								<option value="${tipo[0]}">${tipo[1]}</option>
							</c:forEach>
						  </select>
					</fieldset><br>
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>
  	