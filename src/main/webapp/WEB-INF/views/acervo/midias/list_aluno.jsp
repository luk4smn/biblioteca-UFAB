<%@ include file="../../common/header.jspf"%>
<%@ include file="../../common/navigation_aluno.jspf"%>

	<div class="container">
		<H1>Cadastro de Midias Eletronicas</H1>
		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>#</th>
				<th>T�tulo</th>
				<th>Data de Gravacao</th>
				<th>Tipo</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${midias}" var="midia">
				<tr>
					<td>${midia.id}</td>
					<td>${midia.titulo}</td>
					<td>${midia.dataDeGravacao}</td>
					
					<c:forEach items="${tipos}" var="tipo">
						<c:if test="${midia.tipoMidia == tipo[0]}">
					      <td>${tipo[1]}</td>
					   	</c:if>
					</c:forEach>
					
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					  
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		
		</div>
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
	</div>

	
	
	
	
	
	
<%@ include file="../../common/footer.jspf"%>