<%@ include file="../../common/header.jspf"%>
<%@ include file="../../common/navigation_aluno.jspf"%>

	<div class="container">
		<H1>Anais de Congresso</H1>
		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>#</th>
				<th>T�tulo</th>
				<th>Edicao</th>
				<th>Autores</th>
				<th>Orientadores</th>
				<th>Nome Do Congresso</th>
				<th>Local</th>
				<th>Ano De Publicacao</th>
				<th>Tipo</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${anais}" var="anal">
				<tr>
					<td>${anal.id}</td>
					<td>${anal.titulo}</td>
					<td>${anal.edicao}</td>
					<td>${anal.autores}</td>
					<td>${anal.orientadores}</td>
					<td>${anal.nomeCongresso}</td>
					<td>${anal.local}</td>
					<td>${anal.anoPublicacao}</td>
					
					<c:forEach items="${tipos}" var="tipo">
						<c:if test="${anal.tipoAnal == tipo[0]}">
					      <td>${tipo[1]}</td>
					   	</c:if>
					</c:forEach>	
					   
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					    <ul class="dropdown-menu">
							<li>
							
							</li>
					    </ul>
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		
		</div>
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
	</div>

	
<%@ include file="../../common/footer.jspf"%>