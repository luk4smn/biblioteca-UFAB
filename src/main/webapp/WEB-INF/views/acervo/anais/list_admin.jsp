<%@ include file="../../common/header.jspf"%>
<%@ include file="../../common/navigation_admin.jspf"%>

	<div class="container">
		<H1>Anais de Congresso</H1>
		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>#</th>
				<th>T�tulo</th>
				<th>Edicao</th>
				<th>Autores</th>
				<th>Orientadores</th>
				<th>Nome Do Congresso</th>
				<th>Local</th>
				<th>Ano De Publicacao</th>
				<th>Tipo</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${anais}" var="anal">
				<tr>
					<td>${anal.id}</td>
					<td>${anal.titulo}</td>
					<td>${anal.edicao}</td>
					<td>${anal.autores}</td>
					<td>${anal.orientadores}</td>
					<td>${anal.nomeCongresso}</td>
					<td>${anal.local}</td>
					<td>${anal.anoPublicacao}</td>
					
					<c:forEach items="${tipos}" var="tipo">
						<c:if test="${anal.tipoAnal == tipo[0]}">
					      <td>${tipo[1]}</td>
					   	</c:if>
					</c:forEach>	
					
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					    <ul class="dropdown-menu">
				    	   	<c:if test="${emprestimos.isEmpty() == true}">
								<li>
									<a class="btn create-emprestimo" data-toggle="modal" data-target="#modalEmprestimo" 
									data-nome="${anal.titulo}"
									data-itemId="${anal.id}"
									>
									
										Realizar Empr�stimo
									</a>
								</li>
						    </c:if> 
						    
						    <c:if test="${emprestimos.isEmpty() == false}">
						    
						    	<c:set var = "emprestado" value = "${0}" />
						    
							    <c:forEach items="${emprestimos}" var="emprestimo">
									<c:if test="${emprestimo.tipoItem == tipoItemAcervo && anal.id == emprestimo.itemId}">	
										<li>
											<a class="btn create-reserva" data-toggle="modal" data-target="#modalReserva"
												data-nomeReserva="${anal.titulo}"
												data-itemIdReserva="${anal.id}">
												Realizar Reserva
											</a>
										</li>									
									 	<c:set var = "emprestado" value = "${1}" />
								   	</c:if> 	
								</c:forEach>
						    
									<c:if test="${emprestado == 0}">
										<li>
											<a class="btn create-emprestimo" data-toggle="modal" data-target="#modalEmprestimo" 
												data-nome="${anal.titulo}"
												data-itemId="${anal.id}"
												>
									
												Realizar Empr�stimo
											</a>
										</li>
								   	</c:if>
									  	
							</c:if>
						    <li class="divider"></li>
						   
						    <li>
							    <a class="btn edit-from-table" data-toggle="modal" data-target="#modalEdit" 
							    data-id="${anal.id}"
								data-titulo="${anal.titulo}"
								data-edicao="${anal.edicao}"
								data-autores="${anal.autores}"
								data-orientadores="${anal.orientadores}"
								data-nomeCongresso="${anal.nomeCongresso}"
								data-local="${anal.local}"
								data-anoPublicacao="${anal.anoPublicacao}"
								data-tipoAnal="${anal.tipoAnal}"
							    >
									Editar
								</a>
							</li>
							<li class="divider"></li>
							<li>
								<a class="btn" onclick="return confirm('Confirmar exclus�o?')" 
									href="/anais/delete/${anal.id}">
									Deletar
								</a>
							</li>
							
					    </ul>
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		
		</div>
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
		<button class="btn btn-primary" data-toggle="modal" data-target="#modalCreate"><i class="fa fa-plus-circle"></i> Adicionar Novo</button>
		
	</div>

	<!-- Modal Create -->
	<%@ include file="create.jsp" %>
	
	<!-- Modal Edi��o -->
	<%@ include file="edit.jsp" %>
	
	<!-- Modal Empr�stimo -->
	<%@ include file="../../emprestimos/create.jsp" %>
	
	<!-- Modal Reserva -->
	<%@ include file="../../reservas/create.jsp" %>
	
	
<%@ include file="../../common/footer.jspf"%>