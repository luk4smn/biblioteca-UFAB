	<div id="modalCreate" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/anais/create" commandName="anal">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de Anais de Congresso</h4>
		      </div>
		      <div class="modal-body">
				
					<fieldset class="form-group">
						<label>T�tulo</label>
						<input name="titulo" type="text" class="form-control" required/>
					</fieldset>
					<fieldset class="form-group">
						<label>Edi��o</label>
						<input name="edicao" type="text" class="form-control" required/>
					</fieldset>
					<fieldset class="form-group">
						<label>Autores</label>
						<input name="autores" type="text" class="form-control" required/>
					</fieldset>  
					<fieldset class="form-group">
						<label>Orientadores</label>
						<input name="orientadores" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Nome Do Congresso</label>
						<input name="nomeCongresso" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Local</label>
						<input name="local" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Ano De Publicacao</label>
						<input name="anoPublicacao" type="number" class="form-control" required/>
					</fieldset> 
					
					<fieldset>
						<label for="select_tipo">Tipo</label>
						  <select class="form-control" id="select_tipo" name="tipoAnal" required>
						   	 <c:forEach items="${tipos}" var="tipo">
								<option value="${tipo[0]}">${tipo[1]}</option>
							</c:forEach>
						  </select>
					</fieldset><br>
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>