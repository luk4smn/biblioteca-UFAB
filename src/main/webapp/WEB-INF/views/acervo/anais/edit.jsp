	<div id="modalEdit" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/anais/update" commandName="anal">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de Anais de Congresso </h4>
		      </div>
		      <div class="modal-body">
				


			<script type="text/javascript">
			$('.edit-from-table').on('click', function () {
			    let id = $(this).attr('data-id');
			    let titulo = $(this).attr('data-titulo');
			    let edicao = $(this).attr('data-edicao');
			    let autores = $(this).attr('data-autores');
			    let orientadores = $(this).attr('data-orientadores');
			    let nomeCongresso = $(this).attr('data-nomeCongresso');
			    let local = $(this).attr('data-local');
			    let anoPublicacao = $(this).attr('data-anoPublicacao');
			    let tipoAnal = $(this).attr('data-tipoAnal');
 
			    document.getElementById('id').value = id;
			    document.getElementById('titulo').value = titulo;
			    document.getElementById('edicao').value = edicao;
			    document.getElementById('autores').value = autores;
			    document.getElementById('orientadores').value = orientadores;
			    document.getElementById('nomeCongresso').value = nomeCongresso;
			    document.getElementById('local').value = local;
			    document.getElementById('anoPublicacao').value = anoPublicacao;
			    document.getElementById('tipoAnal').value = tipoAnal;
				   
			});
			
			</script>


					<input name="id" id="id" type="hidden" class="form-control"/>


					<fieldset class="form-group">
						<label>T�tulo</label>
						<input name="titulo" id="titulo" type="text" class="form-control" required/>
					</fieldset>
					<fieldset class="form-group">
						<label>Edi��o</label>
						<input name="edicao" id="edicao" type="text" class="form-control" required/>
					</fieldset>
					<fieldset class="form-group">
						<label>Autores</label>
						<input name="autores" id="autores" type="text" class="form-control" required/>
					</fieldset>  
					<fieldset class="form-group">
						<label>Orientadores</label>
						<input name="orientadores" id="orientadores" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Nome Do Congresso</label>
						<input name="nomeCongresso" id="nomeCongresso" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Local</label>
						<input name="local" id="local" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Ano De Publicacao</label>
						<input name="anoPublicacao" id="anoPublicacao" type="number" class="form-control" required/>
					</fieldset> 
					
					<fieldset>
						<label for="select_tipo">Tipo</label>
						  <select class="form-control" name="tipoAnal" id="tipoAnal" required>
						   	  <c:forEach items="${tipos}" var="tipo">
								<option value="${tipo[0]}">${tipo[1]}</option>
							</c:forEach>
						  </select>
					</fieldset><br>
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>

