<%@ include file="../../common/header.jspf"%>
<%@ include file="../../common/navigation_funcionario.jspf"%>

	<div class="container">
		<H1>Trabalhos de Conclus�o</H1>
		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>#</th>
				<th>T�tulo</th>
				<th>Edicao</th>
				<th>Autores</th>
				<th>Orientadores</th>
				<th>Local</th>
				<th>Ano de Defesa</th>
				<th>Tipo</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${tccs}" var="tcc">
				<tr>
					<td>${tcc.id}</td>
					<td>${tcc.titulo}</td>
					<td>${tcc.edicao}</td>
					<td>${tcc.autores}</td>
					<td>${tcc.orientadores}</td>
					<td>${tcc.local}</td>
					<td>${tcc.anoDefesa}</td>
					
					<c:forEach items="${tipos}" var="tipo">
						<c:if test="${tcc.tipoTcc == tipo[0]}">
					      <td>${tipo[1]}</td>
					   	</c:if>
					</c:forEach>
					
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					    <ul class="dropdown-menu">
							<c:if test="${emprestimos.isEmpty() == true}">
								<li>
									<a class="btn create-emprestimo" data-toggle="modal" data-target="#modalEmprestimo" 
									data-nome="${tcc.titulo}"
									data-itemId="${tcc.id}"
									>
									
										Realizar Empr�stimo
									</a>
								</li>
						    </c:if> 
						    
						    <c:if test="${emprestimos.isEmpty() == false}">
						    	<c:set var = "emprestado" value = "${0}" />
						    
							    <c:forEach items="${emprestimos}" var="emprestimo">
									<c:if test="${emprestimo.tipoItem == tipoItemAcervo && tcc.id == emprestimo.itemId}">	
										<li>
											<a class="btn create-reserva" data-toggle="modal" data-target="#modalReserva"
												data-nomeReserva="${tcc.titulo}"
												data-itemIdReserva="${tcc.id}">
												Realizar Reserva
											</a>
										</li>									
									 	<c:set var = "emprestado" value = "${1}" />
								   	</c:if> 	
								</c:forEach>

									<c:if test="${emprestado == 0}">
										<li>
											<a class="btn create-emprestimo" data-toggle="modal" data-target="#modalEmprestimo" 
												data-nome="${tcc.titulo}"
												data-itemId="${tcc.id}"
												>
									
												Realizar Empr�stimo
											</a>
										</li>
								   	</c:if>
									  	
							</c:if>
						    <li class="divider"></li>
					    </ul>
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		
		</div>
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
	</div>

		<!-- Modal Empr�stimo -->
	<%@ include file="../../emprestimos/create.jsp" %>
	
	<!-- Modal Reserva -->
	<%@ include file="../../reservas/create.jsp" %>
	
	
<%@ include file="../../common/footer.jspf"%>