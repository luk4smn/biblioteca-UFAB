	<div id="modalEdit" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/revistas/update" commandName="revista">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de Revistas  </h4>
		      </div>
		      <div class="modal-body">
				


			<script type="text/javascript">
			$('.edit-from-table').on('click', function () {
			    let id = $(this).attr('data-id');
			    let titulo = $(this).attr('data-titulo');
			    let edicao = $(this).attr('data-edicao');
			    let editora = $(this).attr('data-editora');
			    let numeroPaginas = $(this).attr('data-numeroPaginas');
			    let dataPublicacao = $(this).attr('data-dataPublicacao');
 
			    document.getElementById('id').value = id;
			    document.getElementById('titulo').value = titulo;
			    document.getElementById('edicao').value = edicao;
			    document.getElementById('editora').value = editora;
			    document.getElementById('numeroPaginas').value = numeroPaginas;
			    document.getElementById('dataPublicacao').value = dataPublicacao;
				   
			});

			
			</script>
   				<input name="id" id="id" type="hidden" class="form-control"/>

					<fieldset class="form-group">
						<label>T�tulo</label>
						<input name="titulo" id="titulo" type="text" class="form-control" required/>
					</fieldset>
					<fieldset class="form-group">
						<label>Edi��o</label>
						<input name="edicao" id="edicao" type="text" class="form-control" required/>
					</fieldset>  
					<fieldset class="form-group">
						<label>Editora</label>
						<input name="editora" id="editora" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Numero de Paginas</label>
						<input name="numeroPaginas" id="numeroPaginas" type="number" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Data De Publicacao</label>
						<input name="dataPublicacao" id="dataPublicacao" type="date" class="form-control" required/>
					</fieldset> 
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Atualizar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>
  	