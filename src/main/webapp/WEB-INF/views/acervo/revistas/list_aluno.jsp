<%@ include file="../../common/header.jspf"%>
<%@ include file="../../common/navigation_aluno.jspf"%>

	<div class="container">
		<H1>Revistas</H1>
		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>#</th>
				<th>T�tulo</th>
				<th>Edicao</th>
				<th>Editora</th>		
				<th>N�mero de p�ginas</th>
				<th>Data de Publicacao</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${revistas}" var="revista">
				<tr>
					<td>${revista.id}</td>
					<td>${revista.titulo}</td>
					<td>${revista.edicao}</td>
					<td>${revista.editora}</td>
					<td>${revista.numeroPaginas}</td>
					<td>${revista.dataPublicacao}</td>
					
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					   
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		
		</div>
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
	</div>

	
	
	
	
	
	
<%@ include file="../../common/footer.jspf"%>