<%@ include file="../../common/header.jspf"%>
<%@ include file="../../common/navigation_admin.jspf"%>

	<div class="container">
		<H1>Revistas</H1>
		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>#</th>
				<th>T�tulo</th>
				<th>Edicao</th>
				<th>Editora</th>		
				<th>N�mero de p�ginas</th>
				<th>Data de Publicacao</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${revistas}" var="revista">
				<tr>
					<td>${revista.id}</td>
					<td>${revista.titulo}</td>
					<td>${revista.edicao}</td>
					<td>${revista.editora}</td>
					<td>${revista.numeroPaginas}</td>
					<td>${revista.dataPublicacao}</td>
								
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					    <ul class="dropdown-menu">
				    	   	<c:if test="${emprestimos.isEmpty() == true}">
								<li>
									<a class="btn create-emprestimo" data-toggle="modal" data-target="#modalEmprestimo" 
									data-nome="${revista.titulo}"
									data-itemId="${revista.id}"
									>
									
										Realizar Empr�stimo
									</a>
								</li>
						    </c:if> 
						    
						    <c:if test="${emprestimos.isEmpty() == false}">
						    	<c:set var = "emprestado" value = "${0}" />
						    
							    <c:forEach items="${emprestimos}" var="emprestimo">
									<c:if test="${emprestimo.tipoItem == tipoItemAcervo && revista.id == emprestimo.itemId}">
										<li>
											<a class="btn create-reserva" data-toggle="modal" data-target="#modalReserva"
												data-nomeReserva="${revista.titulo}"
												data-itemIdReserva="${revista.id}">
												Realizar Reserva
											</a>
										</li>										
									 	<c:set var = "emprestado" value = "${1}" />
								   	</c:if> 	
								</c:forEach>
						    		
									<c:if test="${emprestado == 0}">
										<li>
											<a class="btn create-emprestimo" data-toggle="modal" data-target="#modalEmprestimo" 
												data-nome="${revista.titulo}"
												data-itemId="${revista.id}"
												>
									
												Realizar Empr�stimo
											</a>
										</li>
								   	</c:if>
									  	
							</c:if>
						   
					    	<li class="divider"></li>
						    <li>
						    
							    <a class="btn edit-from-table" data-toggle="modal" data-target="#modalEdit" 
							    data-id="${revista.id}"
								data-titulo="${revista.titulo}"
								data-edicao="${revista.edicao}"
								data-editora="${revista.editora}"
								data-numeroPaginas="${revista.numeroPaginas}"
								data-dataPublicacao="${revista.dataPublicacao}"
							    >
									Editar
								</a>
							</li>
							<li class="divider"></li>
							<li>
								<a class="btn" onclick="return confirm('Confirmar exclus�o?')" 
									href="/revistas/delete/${revista.id}">
									Deletar
								</a>
							</li>
							
					    </ul>
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		
		</div>
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
		<button class="btn btn-primary" data-toggle="modal" data-target="#modalCreate"><i class="fa fa-plus-circle"></i> Adicionar Novo</button>
		
	</div>

	<!-- Modal Create -->
	<%@ include file="create.jsp" %>
	
	<!-- Modal Edi��o -->
	<%@ include file="edit.jsp" %>
	
	<!-- Modal Empr�stimo -->
	<%@ include file="../../emprestimos/create.jsp" %>
	
	<!-- Modal Reserva -->
	<%@ include file="../../reservas/create.jsp" %>
	
	
<%@ include file="../../common/footer.jspf"%>