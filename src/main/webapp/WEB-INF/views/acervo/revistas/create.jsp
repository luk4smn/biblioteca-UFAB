	<div id="modalCreate" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/revistas/create" commandName="revista">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de Revistas </h4>
		      </div>
		      
		      
		      <div class="modal-body">
			
					<fieldset class="form-group">
						<label>T�tulo</label>
						<input name="titulo" type="text" class="form-control" required/>
					</fieldset>
					<fieldset class="form-group">
						<label>Edi��o</label>
						<input name="edicao" type="text" class="form-control" required/>
					</fieldset>  
					<fieldset class="form-group">
						<label>Editora</label>
						<input name="editora" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Numero de Paginas</label>
						<input name="numeroPaginas" type="number" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Data De Publicacao</label>
						<input name="dataPublicacao" type="date" class="form-control" required/>
					</fieldset> 
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>
  	