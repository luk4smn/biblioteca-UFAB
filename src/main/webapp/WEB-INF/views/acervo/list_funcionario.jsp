<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_funcionario.jspf"%>

	<div class="container">
		<H1>Acervo da Bibioteca</H1>
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
	    <div class="panel panel-default">
	      <div class="panel-heading"align="center">...</div>
	      <div class="panel-body" align="center">
			<br>
	   		
	   		<div class="">
	   			<a class="button" href="/livros/list">
			  		<i class="fa fa-book"></i> Livros
			 	</a>
			  <a class="button" href="/revistas/list">
			  	<i class="fa fa-file-picture-o"></i> Revistas
			  </a>
			  <a class="button" href="/tccs/list">
			  	<i class="fa fa-file-pdf-o"></i> Trabalhos de conclus�o
		  	  </a>
		  	 
	   		</div><br><br>
	   		
	   		
	   		<div class="">
	   		  <a class="button" href="/jornais/list">
			  	<i class="fa fa-file-word-o"></i> Jornais
		  	  </a>
		  	   <a class="button" href="/anais/list">
			  	<i class="fa fa-file-excel-o"></i> Anais de congresso
		  	  </a>
			  
			  <a class="button" href="/midias/list">
			  	<i class="fa fa-film"></i> M�dias eletr�nicas
		  	  </a>
	   		</div><br><br>
	
	      </div>
	      <div class="panel-footer">
	      
		      <div class="">
		      <a href="/emprestimos/list" class="btn btn-primary">
					<span> Listar Empr�stimos</span>
				</a>
				<a href="/reservas/list" class="btn btn-success"><span>Listar Reservas </span></a>
		      </div>
	 	
	    </div>
	  </div>
	
	
	
<%@ include file="../common/footer.jspf"%>