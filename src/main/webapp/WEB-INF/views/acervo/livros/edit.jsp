	<div id="modalEdit" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/livros/update" commandName="livro">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de Livros </h4>
		      </div>
		      <div class="modal-body">
				


			<script type="text/javascript">
			$('.edit-from-table').on('click', function () {
			    let id = $(this).attr('data-id');
			    let titulo = $(this).attr('data-titulo');
			    let edicao = $(this).attr('data-edicao');
			    let autores = $(this).attr('data-autores');
			    let editora = $(this).attr('data-editora');
			    let ISBN = $(this).attr('data-ISBN');
			    let numeroPaginas = $(this).attr('data-numeroPaginas');
			    let anoPublicacao = $(this).attr('data-anoPublicacao');
			    let areaConhecimento = $(this).attr('data-areaConhecimento');
			    let tipoTema = $(this).attr('data-tipoTema');
 
			    document.getElementById('id').value = id;
			    document.getElementById('titulo').value = titulo;
			    document.getElementById('edicao').value = edicao;
			    document.getElementById('autores').value = autores;
			    document.getElementById('editora').value = editora;
			    document.getElementById('ISBN').value = ISBN;
			    document.getElementById('numeroPaginas').value = numeroPaginas;
			    document.getElementById('anoPublicacao').value = anoPublicacao;
			    document.getElementById('areaConhecimento').value = areaConhecimento;
			    document.getElementById('tipoTema').value = tipoTema;
				   
			});
			
			</script>
   				<input name="id" id="id" type="hidden" class="form-control"/>

					<fieldset class="form-group">
						<label>T�tulo</label>
						<input name="titulo" id="titulo" type="text" class="form-control" required/>
					</fieldset>
					<fieldset class="form-group">
						<label>Edi��o</label>
						<input name="edicao" id="edicao" type="text" class="form-control" required/>
					</fieldset>
					<fieldset class="form-group">
						<label>Autores</label>
						<input name="autores" id="autores" type="text" class="form-control" required/>
					</fieldset>  
					<fieldset class="form-group">
						<label>Editora</label>
						<input name="editora" id="editora" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>ISBN</label>
						<input name="ISBN" id="ISBN" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Numero de Paginas</label>
						<input name="numeroPaginas" id="numeroPaginas" type="number" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Ano De Publicacao</label>
						<input name="anoPublicacao" id="anoPublicacao" type="number" class="form-control" required/>
					</fieldset> 
					
					<fieldset>
						<label for="areaConhecimento">Area de Conhecimento</label>
						  <select class="form-control" name="areaConhecimento" id="areaConhecimento" required>
						   	  <c:forEach items="${temas}" var="tema">
								<option value="${tema[0]}">${tema[1]}</option>
							  </c:forEach>
						  </select>
					</fieldset><br>
										
					<fieldset>
						<label for="subtema">Tipo de Tema</label>
						  <select class="form-control" name="tipoTema" id="tipoTema" required>
							  <c:forEach items="${subtemas}" var="subtema">
								<option value="${subtema[0]}">${subtema[1]}</option>
							</c:forEach>
						  </select>
					</fieldset><br>
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Atualizar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>
  	