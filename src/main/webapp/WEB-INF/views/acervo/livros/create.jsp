	<div id="modalCreate" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/livros/create" commandName="livro">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de Livros </h4>
		      </div>
		      <div class="modal-body">
			
					<fieldset class="form-group">
						<label>T�tulo</label>
						<input name="titulo" type="text" class="form-control" required/>
					</fieldset>
					<fieldset class="form-group">
						<label>Edi��o</label>
						<input name="edicao" type="text" class="form-control" required/>
					</fieldset>
					<fieldset class="form-group">
						<label>Autores</label>
						<input name="autores" type="text" class="form-control" required/>
					</fieldset>  
					<fieldset class="form-group">
						<label>Editora</label>
						<input name="editora" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>ISBN</label>
						<input name="ISBN" type="text" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Numero de Paginas</label>
						<input name="numeroPaginas" type="number" class="form-control" required/>
					</fieldset> 
					<fieldset class="form-group">
						<label>Ano De Publicacao</label>
						<input name="anoPublicacao" type="number" class="form-control" required/>
					</fieldset> 
					
					<fieldset>
						<label>Tipo de Tema</label>
						  <select class="form-control" name="areaConhecimento" required>
						  	<c:forEach items="${temas}" var="tema">
								<option value="${tema[0]}">${tema[1]}</option>
							</c:forEach>
						  </select>
					</fieldset><br>
					
										
					<fieldset>
						<label>Subtema</label>
						  <select class="form-control" name="tipoTema" required>
						  	<c:forEach items="${subtemas}" var="subtema">
								<option value="${subtema[0]}">${subtema[1]}</option>
							</c:forEach>
						  </select>
					</fieldset><br>
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>
  	