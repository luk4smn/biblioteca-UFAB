<%@ include file="../../common/header.jspf"%>
<%@ include file="../../common/navigation_aluno.jspf"%>

	<div class="container">
		<H1>Livros</H1>
		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>#</th>
				<th>T�tulo</th>
				<th>Edicao</th>
				<th>Autores</th>
				<th>Editora</th>
				<th>ISBN</th>
				<th>N�mero de p�ginas</th>
				<th>Ano De Publicacao</th>
				<th>�rea de Conhecimento</th>
				<th>Tema</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${livros}" var="livro">
				<tr>
					<td>${livro.id}</td>
					<td>${livro.titulo}</td>
					<td>${livro.edicao}</td>
					<td>${livro.autores}</td>
					<td>${livro.editora}</td>
					<td>${livro.ISBN}</td>
					<td>${livro.numeroPaginas}</td>
					<td>${livro.anoPublicacao}</td>
					
					<c:forEach items="${temas}" var="tema">
						<c:if test="${livro.areaConhecimento == tema[0]}">
					      <td>${tema[1]}</td>
					   	</c:if>
					</c:forEach>
					
					<c:forEach items="${subtemas}" var="subtema">
						<c:if test="${livro.tipoTema == subtema[0]}">
					      <td>${subtema[1]}</td>
					   	</c:if>
					</c:forEach>
					
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					    
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		
		</div>
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
	</div>

	
	
	
	
	
	
<%@ include file="../../common/footer.jspf"%>