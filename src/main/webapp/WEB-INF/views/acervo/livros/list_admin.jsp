
<%@ include file="../../common/header.jspf"%>
<%@ include file="../../common/navigation_admin.jspf"%>

	<div class="container">
		<H1>Livros</H1>
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>#</th>
				<th>T�tulo</th>
				<th>Edicao</th>
				<th>Autores</th>
				<th>Editora</th>
				<th>ISBN</th>
				<th>N�mero de p�ginas</th>
				<th>Ano De Publicacao</th>
				<th>�rea de Conhecimento</th>
				<th>Tema</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${livros}" var="livro">
				<tr>
					<td>${livro.id}</td>
					<td>${livro.titulo}</td>
					<td>${livro.edicao}</td>
					<td>${livro.autores}</td>
					<td>${livro.editora}</td>
					<td>${livro.ISBN}</td>
					<td>${livro.numeroPaginas}</td>
					<td>${livro.anoPublicacao}</td>
					
					<c:forEach items="${temas}" var="tema">
						<c:if test="${livro.areaConhecimento == tema[0]}">
					      <td>${tema[1]}</td>
					   	</c:if>
					</c:forEach>
					
					<c:forEach items="${subtemas}" var="subtema">
						<c:if test="${livro.tipoTema == subtema[0]}">
					      <td>${subtema[1]}</td>
					   	</c:if>
					</c:forEach>
								
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					    <ul class="dropdown-menu">
					    
				    	   	<c:if test="${emprestimos.isEmpty() == true}">
								<li>
									<a class="btn create-emprestimo" data-toggle="modal" data-target="#modalEmprestimo" 
									data-nome="${livro.titulo}"
									data-itemId="${livro.id}"
									>
									
										Realizar Empr�stimo
									</a>
								</li>
						    </c:if> 
						    
						    <c:if test="${emprestimos.isEmpty() == false}">
						    
						    	<c:set var = "emprestado" value = "${0}" />
						    
							    <c:forEach items="${emprestimos}" var="emprestimo">
									<c:if test="${emprestimo.tipoItem == tipoItemAcervo && livro.id == emprestimo.itemId}">	
										<li>
											<a class="btn create-reserva" data-toggle="modal" data-target="#modalReserva"
												data-nomeReserva="${livro.titulo}"
												data-itemIdReserva="${livro.id}">
													
												Realizar Reserva
											</a>
										</li>								
									 	<c:set var = "emprestado" value = "${1}" />
								   	</c:if> 	
								</c:forEach>
						    
						     			
									<c:if test="${emprestado == 0}">
										<li>
											<a class="btn create-emprestimo" data-toggle="modal" data-target="#modalEmprestimo" 
												data-nome="${livro.titulo}"
												data-itemId="${livro.id}"
												>
									
												Realizar Empr�stimo
											</a>
										</li>
								   	</c:if>
									  	
							</c:if>
						   							
							<li class="divider"></li>
							<li>
						    
							    <a class="btn edit-from-table" data-toggle="modal" data-target="#modalEdit" 
							    data-id="${livro.id}"
								data-titulo="${livro.titulo}"
								data-edicao="${livro.edicao}"
								data-autores="${livro.autores}"
								data-editora="${livro.editora}"
								data-ISBN="${livro.ISBN}"
								data-numeroPaginas="${livro.numeroPaginas}"
								data-anoPublicacao="${livro.anoPublicacao}"
								data-areaConhecimento="${livro.areaConhecimento}"
								data-tipoTema="${livro.tipoTema}"
							    >
									Editar
								</a>
							</li>
							
							<li class="divider"></li>
							
							<li>
								<a class="btn" onclick="return confirm('Confirmar exclus�o?')" 
									href="/livros/delete/${livro.id}">
									Deletar
								</a>
					    </ul>
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		
		</div>
		
		
		<button class="btn btn-primary" data-toggle="modal" data-target="#modalCreate"><i class="fa fa-plus-circle"></i> Adicionar Novo</button>
		
	</div>

	<!-- Modal Create -->
	<%@ include file="create.jsp" %>
	
	<!-- Modal Edi��o -->
	<%@ include file="edit.jsp" %>
	
	<!-- Modal Empr�stimo -->
	<%@ include file="../../emprestimos/create.jsp" %>
	
	<!-- Modal Reserva -->
	<%@ include file="../../reservas/create.jsp" %>
	
	
<%@ include file="../../common/footer.jspf"%>