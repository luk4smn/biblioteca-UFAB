
<%@ include file="../../common/header.jspf"%>
<%@ include file="../../common/navigation_admin.jspf"%>

	<div class="container">
		<H1>Jornais</H1>
		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>#</th>
				<th>T�tulo</th>
				<th>Edicao</th>
				<th>Data de Publicacao</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${jornais}" var="jornal">
				<tr>
					<td>${jornal.id}</td>
					<td>${jornal.titulo}</td>
					<td>${jornal.edicao}</td>
					<td>${jornal.dataPublicacao}</td>
								
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					    <ul class="dropdown-menu">
				    	   	<c:if test="${emprestimos.isEmpty() == true}">
								<li>
									<a class="btn create-emprestimo" data-toggle="modal" data-target="#modalEmprestimo" 
									data-nome="${jornal.titulo}"
									data-itemId="${jornal.id}"
									>
									
										Realizar Empr�stimo
									</a>
								</li>
						    </c:if> 
						    
						    <c:if test="${emprestimos.isEmpty() == false}">
						    
						    	<c:set var = "emprestado" value = "${0}" />
						    
							    <c:forEach items="${emprestimos}" var="emprestimo">
									<c:if test="${emprestimo.tipoItem == tipoItemAcervo && jornal.id == emprestimo.itemId}">
										<li>
											<a class="btn create-reserva" data-toggle="modal" data-target="#modalReserva"
												data-nomeReserva="${jornal.titulo}"
												data-itemIdReserva="${jornal.id}">
												Realizar Reserva
											</a>
										</li>										
									 	<c:set var = "emprestado" value = "${1}" />
								   	</c:if> 	
								</c:forEach>
						    
						   										
									<c:if test="${emprestado == 0}">
										<li>
											<a class="btn create-emprestimo" data-toggle="modal" data-target="#modalEmprestimo" 
												data-nome="${jornal.titulo}"
												data-itemId="${jornal.id}"
												>
									
												Realizar Empr�stimo
											</a>
										</li>
								   	</c:if>
									  	
							</c:if>
						  
							<li class="divider"></li>
						    <li>
						    	<a class="btn edit-from-table" data-toggle="modal" data-target="#modalEdit" 
							    data-id="${jornal.id}"
								data-titulo="${jornal.titulo}"
								data-edicao="${jornal.edicao}"
								data-dataPublicacao="${jornal.dataPublicacao}"
							    >
									Editar
								</a>
							</li>
							<li class="divider"></li>
							<li>
								<a class="btn" onclick="return confirm('Confirmar exclus�o?')" 
									href="/jornais/delete/${jornal.id}">
									Deletar
								</a>
							</li>
					    </ul>
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		
		</div>
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
		<button class="btn btn-primary" data-toggle="modal" data-target="#modalCreate"><i class="fa fa-plus-circle"></i> Adicionar Novo</button>
		
	</div>

	<!-- Modal Create -->
	<%@ include file="create.jsp" %>
	
	<!-- Modal Edi��o -->
	<%@ include file="edit.jsp" %>
	
	<!-- Modal Empr�stimo -->
	<%@ include file="../../emprestimos/create.jsp" %>
	
	<!-- Modal Reserva -->
	<%@ include file="../../reservas/create.jsp" %>
	
<%@ include file="../../common/footer.jspf"%>