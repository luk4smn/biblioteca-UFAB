	<div id="modalEdit" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/jornais/update" commandName="jornal">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de jornais  </h4>
		      </div>
		      <div class="modal-body">
				


			<script type="text/javascript">
			$('.edit-from-table').on('click', function () {
			    let id = $(this).attr('data-id');
			    let titulo = $(this).attr('data-titulo');
			    let edicao = $(this).attr('data-edicao');
			    let dataPublicacao = $(this).attr('data-dataPublicacao');
 
			    document.getElementById('id').value = id;
			    document.getElementById('titulo').value = titulo;
			    document.getElementById('edicao').value = edicao;
			    document.getElementById('dataPublicacao').value = dataPublicacao;
				   
			});
			
			</script>
   				<input name="id" id="id" type="hidden" class="form-control"/>

					<fieldset class="form-group">
						<label>T�tulo</label>
						<input name="titulo" id="titulo" type="text" class="form-control" required/>
					</fieldset>
					<fieldset class="form-group">
						<label>Edi��o</label>
						<input name="edicao" id="edicao" type="text" class="form-control" required/>
					</fieldset>  
					<fieldset class="form-group">
						<label>Data De Publicacao</label>
						<input name="dataPublicacao" id="dataPublicacao" type="date" class="form-control" required/>
					</fieldset> 
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Atualizar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>
  	