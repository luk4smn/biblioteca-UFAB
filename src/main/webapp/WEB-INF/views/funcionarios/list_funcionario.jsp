<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_funcionario.jspf"%>

	<div class="container">
		<H1>Meus dados</H1>
		
		<table class="table table-striped">
			<thead>
				<th>CPF</th>
				<th>RG</th>
				<th>Naturalidade</th>
				<th>Nome</th>
				<th>Nome da M�e</th>
				<th>Endere�o</th>
				<th>Cargo</th>
				<th>A��es</th>
			</thead>
			<tbody>
				<tr>
					<td>${funcionario.cpf}</td>
					<td>${funcionario.rg}</td>
					<td>${funcionario.naturalidade}</td>
					<td>${funcionario.nome}</td>
					<td>${funcionario.nomemae}</td>
					<td>${funcionario.endereco}</td>
					<td>${funcionario.cargo}</td>
					<td>
						<a class="btn btn-warning"
							href="/funcionarios/edit/${funcionario.id}">Editar
						</a>
					</td>
				</tr>
			</tbody>
		</table>

		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
	</div>
	
<%@ include file="../common/footer.jspf"%>