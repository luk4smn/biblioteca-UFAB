
<div id="modalFuncionario" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

	<form:form method="POST" action="/funcionarios/create" commandName="funcionario">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Cadastro de Funcion�rios</h4>
	      </div>
		      <div class="modal-body">
		       
		       	<p>
					<FONT color="red"><form:errors	path="usuario" /></FONT></td>
				</p>
				
				<div class="panel panel-body">
					
					<fieldset class="form-group">
					<label>CPF</label>
					<input name="cpf" type="text" class="form-control" required/>
					</fieldset>
					
					<fieldset class="form-group">
						<label>RG</label>
						<input name="rg" type="text" class="form-control" required/>
					</fieldset>  
					
					<fieldset class="form-group">
						<label>Naturalidade</label>
						<input name="naturalidade" type="text" class="form-control" required/>
					</fieldset> 
					
					<fieldset class="form-group">
						<label>Nome Completo</label>
						<input name="nome" type="text" class="form-control" required/>
					</fieldset>
					 
					<fieldset class="form-group">
						<label>Nome da M�e</label>
						<input name="nomemae" type="text" class="form-control" required/>
					</fieldset> 
					
					<fieldset class="form-group">
						<label>Endere�o</label>
						<input name="endereco" type="text" class="form-control" required/>
					</fieldset> 
					
					<fieldset class="form-group">
						<label>Cargo</label>
						<input name="cargo" type="text" class="form-control" required/>
					</fieldset> 
				</div>
					
				</div>
		
			<div class="modal-footer">
				<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		   	</div>
	     	
	   	</div>
   	
   		</form:form>
      
    </div>

</div>