<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_admin.jspf"%>

	<div class="container">
		<H1>Funcion�rios</H1>
		
		<table class="table table-striped">
			<thead>
				<th>CPF</th>
				<th>RG</th>
				<th>Naturalidade</th>
				<th>Nome</th>
				<th>Nome da M�e</th>
				<th>Endere�o</th>
				<th>Cargo</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${funcionarios}" var="funcionario">
				<tr>
					<td>${funcionario.cpf}</td>
					<td>${funcionario.rg}</td>
					<td>${funcionario.naturalidade}</td>
					<td>${funcionario.nome}</td>
					<td>${funcionario.nomemae}</td>
					<td>${funcionario.endereco}</td>
					<td>${funcionario.cargo}</td>
					<td>
						<a class="btn btn-warning"
							href="/funcionarios/edit/${funcionario.id}">Editar
						</a>
					</td>
					<td>
						<a class="btn btn-danger" onclick="return confirm('Confirmar exclus�o?')" 
							href="/funcionarios/delete/${funcionario.id}">
							
							Delete
						</a>
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		<p>
			<font color="red">${errorMessage}</font>
		</p>
		<button class="btn btn-primary" data-toggle="modal" data-target="#modalFuncionario"><i class="fa fa-plus-circle"></i> Adicionar Novo Funcion�rio</button>
		
	</div>

	<!-- Modal Aluno -->
	<%@ include file="create.jsp" %>
	
<%@ include file="../common/footer.jspf"%>