<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_admin.jspf"%>

	
	
	<div class="container">
	
	<h1>Atualiza��o de Funcion�rio</h1>
		
		<form:form method="POST" action="/funcionarios/update" commandName="funcionario">
		
			<input name="id" type="hidden" value="${funcionario.id}" class="form-control"/>
		
			<fieldset class="form-group">
				<label>CPF</label>
				<input name="cpf" type="text" class="form-control" value="${funcionario.cpf}" required/>
			</fieldset>
			
			<fieldset class="form-group">
				<label>RG</label>
				<input name="rg" type="text" class="form-control" value="${funcionario.rg}" required/>
			</fieldset>  
			
			<fieldset class="form-group">
				<label>Naturalidade</label>
				<input name="naturalidade" type="text" class="form-control" value="${funcionario.naturalidade}" required/>
			</fieldset> 
			
			<fieldset class="form-group">
				<label>Nome Completo</label>
				<input name="nome" type="text" class="form-control" value="${funcionario.nome}" required/>
			</fieldset>
			 
			<fieldset class="form-group">
				<label>Nome da M�e</label>
				<input name="nomemae" type="text" class="form-control" value="${funcionario.nomemae}" required/>
			</fieldset> 
			
			<fieldset class="form-group">
				<label>Endere�o</label>
				<input name="endereco" type="text" class="form-control" value="${funcionario.endereco}" required/>
			</fieldset> 
			
			<fieldset class="form-group">
				<label>Cargo</label>
				<input name="cargo" type="text" class="form-control" value="${funcionario.cargo}" required/>
			</fieldset> 
			
			<input name="edit" type="submit" value="Submit" class="btn btn-success"/>
			<p></p>
		</form:form>
	</div>

<%@ include file="../common/footer.jspf"%>