<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_login.jspf"%>

	<div class="container">
		<form:form action="/login" method="post" commandName="userAuthenticated">
			<div class="panel panel-body">
				<p>
					<FONT color="red"><form:errors	path="usuario" /></FONT>
				</p>
				
	 <div class="panel panel-default">
      	<div class="panel-heading"align="center">LOGIN</div>
	      <div class="panel-body" align="center">
			<br>
				 <div class="input-container">
				   <i class="fa fa-user icon"></i>
				   <input class="input-field" type="text" placeholder="Usu�rio" name="usuario">
				 </div>
				 
				 <div class="input-container">
				   <i class="fa fa-key icon"></i>
				   <input class="input-field" type="password" placeholder="Senha" name="senha">
				 </div>
				
				 <button type="submit" class="btnLogin">Login</button>
		
				</div>
			</div>
		</div>
			
			
		</form:form>
	</div>

<%@ include file="../common/footer.jspf"%>