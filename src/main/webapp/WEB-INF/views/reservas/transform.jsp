	<div id="modalTransform" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/reservas/transform" commandName="reserva">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Transforma��o de Reserva em Emprestimo</h4>
		      </div>
		      <div class="modal-body">
		      
		      <script type="text/javascript">
			$('.transform').on('click', function () {
			    let id = $(this).attr('data-reserva-id');
			    let alunoNome = $(this).attr('data-aluno-nome');
			    let titulo = $(this).attr('data-item-titulo');		
			    
			    document.getElementById('id').value = id;
			    document.getElementById('alunoNome').value = alunoNome;
			    document.getElementById('titulo').value = titulo;
				   
			});
			
			</script>
					<input name="id" id="id" type="hidden" class="form-control"/>
									
					<fieldset class="form-group">
						<label>T�tulo</label>
						<input id="titulo" type="text" class="form-control" disabled/>
					</fieldset>
					
					<fieldset>
						<label>Aluno</label>
						  <input id=alunoNome type="text" class="form-control" disabled/>
					</fieldset><br>
					
					<fieldset>
						<label>Dias de Reserva</label>
						   <select class="form-control" name="dias" required>
							   <%					       
						        for(int i=1; i<= 30; i++) {   
						        	%><option value="<%=i %>"><%=i%></option><%
				        		} 
				        		%> 
					    	</select>
					</fieldset><br>
										
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>