	<div id="modalReserva" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/reservas/create" commandName="emprestimo">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de Reservas</h4>
		      </div>
		      <div class="modal-body">
		      
		      <script type="text/javascript">
			$('.create-reserva').on('click', function () {
			    let itemId = $(this).attr('data-itemIdReserva');
			    let nome = $(this).attr('data-nomeReserva');
			    
			    document.getElementById('itemIdReserva').value = itemId;
			    document.getElementById('nomeReserva').value = nome;
				   
			});
			
			</script>
			
					<input name="itemId" id="itemIdReserva" type="hidden" class="form-control"/>
					<input name="tipoItem" value="${tipoItemAcervo}" type="hidden" class="form-control"/>
				
					<fieldset class="form-group">
						<label>T�tulo</label>
						<input id="nomeReserva" type="text" class="form-control" disabled/>
					</fieldset>
					
					<fieldset>
						<label>Aluno</label>
						  <select class="form-control" name="alunoId" required>
						   	 <c:forEach items="${alunos}" var="aluno">
								<option value="${aluno.id}">${aluno.nome}</option>
							</c:forEach>
						  </select>
					</fieldset><br>
					
					<fieldset>
					    <input type="checkbox" id="sinalizar" name="sinalizar" value="1">
					    <label for="sinalizar">Receber notifica��o por email</label>
				    </fieldset>
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>