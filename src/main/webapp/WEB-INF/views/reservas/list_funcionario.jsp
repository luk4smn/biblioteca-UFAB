<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_funcionario.jspf"%>

	<div class="container">
		<H1>Reservas</H1>
		
		<div class="alert alert-info">
 			<p> <b>Info:</b> S� � poss�vel transformar a reserva em espr�stimo se o aluno em quest�o n�o possua pendencias .</p> 
		</div>
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		<p>
			<font color="green">${successMessage}</font>
		</p>

		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>C�d. da reserva</th>
				<th>Aluno</th>
				<th>Tipo de Item</th>
				<th>Nome do Item</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${reservas}" var="reserva">
				<tr>
				
					<td># ${reserva.id}</td>
						
					<c:forEach items="${alunos}" var="aluno">
						<c:if test="${reserva.alunoId == aluno.id}">
							<c:set var = "alunoNome" value = "${aluno.nome}" />
							<c:set var = "alunoId" value = "${aluno.id}" />
					      	<td>${alunoNome}</td>
					   	</c:if>			   	
					</c:forEach>
					
				 	<c:forEach items="${alunosSemPendencias}" var="alunoSP">
				   		<c:set var = "pendente" value = "${1}" />
				   		
				   		<c:if test="${alunoId == alunoSP.id}">
				   			<c:set var = "pendente" value = "${0}" />
				   		</c:if>
				   	
				   	</c:forEach>
									
					<c:forEach items="${tipos}" var="tipo">
						<c:if test="${reserva.tipoItem == tipo[0]}">
					      <td>${tipo[1]}</td>
					      
					      	<c:if test="${tipo[0] == 1}">
				    	 	  <c:forEach items="${livros}" var="livro">
								<c:if test="${reserva.itemId == livro.id}">
									<c:set var = "titulo" value = "${livro.titulo}" />
							      </c:if>
						      </c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 2}">
					   			<c:forEach items="${revistas}" var="revista">
									<c:if test="${reserva.itemId == revista.id}">
								      	<c:set var = "titulo" value = "${revista.titulo}" />
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 3}">
					    	 	<c:forEach items="${jornais}" var="jornal">
									<c:if test="${reserva.itemId == jornal.id}">
								      	<c:set var = "titulo" value = "${jornal.titulo}" />
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 4}">
					    	 	<c:forEach items="${anais}" var="anal">
									<c:if test="${reserva.itemId == anal.id}">
								      	<c:set var = "titulo" value = "${anal.titulo}" />
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 5}">
					    	 	<c:forEach items="${tccs}" var="tcc">
									<c:if test="${reserva.itemId == tcc.id}">
								      	<c:set var = "titulo" value = "${tcc.titulo}" />
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 6}">
					    	 	<c:forEach items="${midias}" var="midia">
									<c:if test="${reserva.itemId == midia.id}">
								      	<c:set var = "titulo" value = "${midia.titulo}" />
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<td>${titulo}</td>
					   	</c:if>
					   	
					</c:forEach>
								
				
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					    <ul class="dropdown-menu">
					    
							<c:if test="${pendente == 0}">
								<c:if test="${emprestimosEmAberto.isEmpty() == false}">
							    
							    	<c:set var = "emprestado" value = "${0}" />
							    
								    <c:forEach items="${emprestimosEmAberto}" var="emprestimo">
										<c:if test="${emprestimo.tipoItem == reserva.tipoItem && reserva.itemId == emprestimo.itemId}">	
											<c:set var = "emprestado" value = "${1}" />
										</c:if>
									
									</c:forEach>
									<c:if test="${emprestado == 0}">
										<li>
											<a class="btn transform" data-toggle="modal" data-target="#modalTransform"
												data-reserva-id = "${reserva.id}"
												data-item-titulo = "${titulo}"
												data-aluno-nome = "${alunoNome}"
												
												>
												Transformar em emprestimo
											</a>
										</li>
										<li class="divider"></li>
									</c:if>
								</c:if>
								<c:if test="${emprestimosEmAberto.isEmpty() == true}">
									<li>
											<a class="btn transform" data-toggle="modal" data-target="#modalTransform"
												data-reserva-id = "${reserva.id}"
												data-item-titulo = "${titulo}"
												data-aluno-nome = "${alunoNome}"
												
												>
												Transformar em emprestimo
											</a>
										</li>
									<li class="divider"></li>
								</c:if>
							</c:if>
							<li>
								<a class="btn" onclick="return confirm('Confirmar exclus�o?')" 
									href="/reservas/delete/${reserva.id}">
									Deletar
								</a>
							</li>
					    </ul>
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		
		</div>
		
		<%@ include file="transform.jsp"%>
		
	
<%@ include file="../common/footer.jspf"%>