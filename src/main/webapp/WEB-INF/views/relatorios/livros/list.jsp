<%@ include file="../../common/header.jspf"%>
<%@ include file="../../common/navigation_admin.jspf"%>

	<div class="container">
		<H1>Relat�rio de LIVROS Emprestados</H1>
		
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		<p>
			<font color="green">${successMessage}</font>
		</p>

	
    <div class="panel panel-default">
      <div class="panel-heading"align="center">...</div>
      <div class="panel-body" align="center">
		<br>
	   	
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable-export">
			<thead>
				<th>Data Prevista de Devolucao</th>
				<th>Data de Retirada</th>
				<th>C�d. do emprestimo</th>
				<th>Aluno</th>
				<th>Tipo de Item</th>
				<th>Nome do Item</th>
				<th>Foi Entregue ?</th>
				<th>Pendencia</th>
			</thead>
			<tbody>

			<c:forEach items="${emprestimos}" var="emprestimo">
				<tr>
					<c:set var = "dataDevolucao" value = "${emprestimo.dataDevolucao}" />
					<td>${dataDevolucao}</td>
					
					<c:set var = "dataRetirada" value = "${emprestimo.dataRetirada}" />	
					<td>${dataRetirada}</td>
					
					<td># ${emprestimo.id}</td>	
					
					<c:forEach items="${alunos}" var="aluno">
						<c:if test="${emprestimo.alunoId == aluno.id}">
							<c:set var = "alunoNome" value = "${aluno.nome}" />
					      	<td>${alunoNome}</td>
					   	</c:if>
					</c:forEach>
					
					<c:forEach items="${tipos}" var="tipo">
						<c:if test="${emprestimo.tipoItem == tipo[0]}">
					      <td>${tipo[1]}</td>
					      
					      	<c:if test="${tipo[0] == 1}">
				    	 	  <c:forEach items="${livros}" var="livro">
								<c:if test="${emprestimo.itemId == livro.id}">
									<c:set var = "titulo" value = "${livro.titulo}" />
							      </c:if>
						      </c:forEach>
					   		</c:if>
					   							   		
					   		<td>${titulo}</td>
					   	</c:if>
					   	
					</c:forEach>
				
					<td>${emprestimo.entregue == 1 ? 'Sim' : 'N�o'}</td>
					
					<c:forEach items="${pendencias}" var="pendencia">
									
						<c:if test="${emprestimo.id == pendencia[0]}">										
							<c:if test="${pendencia[1] > 0}">
							 	<c:set var = "valor" value = "${pendencia[1] * 0.50 }" />
					     	</c:if>	
					     	<td>R$ ${valor > 0 ? valor : 0.00 }</td>
					   	</c:if>
					   	
					   	
					</c:forEach>
					
				</tr>
			</c:forEach>
			</tbody>
		</table>
	
	</div>
	</div>
	</div>
	</div>
	
		
		

	
<%@ include file="../../common/footer.jspf"%>