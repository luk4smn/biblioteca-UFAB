<%@ include file="../../common/header.jspf"%>
<%@ include file="../../common/navigation_admin.jspf"%>

	<div class="container">
		<H1>Relat�rio de Alunos com pendencias</H1>
		
	 <div class="panel panel-default">
      <div class="panel-heading"align="center">...</div>
      <div class="panel-body" align="center">
		<br>
		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable-export">
			<thead>
				<th>Matr�cula</th>
				<th>CPF</th>
				<th>RG</th>
				<th>Naturalidade</th>
				<th>Nome</th>
				<th>Nome da M�e</th>
				<th>Endere�o</th>
				<th>Telefone</th>
				<th>Curso</th>
				<th>Nivel</th>
				<th>Ano</th>
				<th>Per�odo</th>
			</thead>
			<tbody>
			<c:forEach items="${alunos}" var="aluno">
				<tr>
					<td>${aluno.matricula}</td>
					<td>${aluno.cpf}</td>
					<td>${aluno.rg}</td>
					<td>${aluno.naturalidade}</td>
					<td>${aluno.nome}</td>
					<td>${aluno.nomemae}</td>
					<td>${aluno.endereco}</td>
					<td>${aluno.telefone}</td>
					<td>${aluno.curso}</td>
					<td>${aluno.nivel}</td>
					<td>${aluno.ano}</td>
					<td>${aluno.periodo}</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

			</div>
			</div>
		</div>
	</div>
	

	
<%@ include file="../../common/footer.jspf"%>