<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_admin.jspf"%>

	<div class="container">
		<H1>Relatórios</H1>
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
	    <div class="panel panel-default">
	      <div class="panel-heading"align="center">...</div>
	      <div class="panel-body" align="center">
			<br>
	   		
	   		<div class="">
	   			<a class="button" href="/relatorios/livros/list">
			  		<i class="fa fa-book"></i> Livros
			 	</a>
			  <a class="button" href="/relatorios/alunos/list">
			  	<i class="fa fa-id-card"></i> Alunos com débitos
			  </a>
					  	 
	   		</div><br><br>
	   		
	   		
	
	      </div>
	      <div class="panel-footer">
	      
		  
	    </div>
	  </div>
	

	</div>
	
	
<%@ include file="../common/footer.jspf"%>