	<div id="modalDevolucao" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/emprestimos/devolver" commandName="emprestimo">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Devolver e Quitar</h4>
		      </div>
		      <div class="modal-body">
		      
		      <script type="text/javascript">
			$('.devolver-emprestimo').on('click', function () {
			    let id = $(this).attr('data-emp-dev-id');
			    let valor = $(this).attr('data-emp-dev-valor');
			    let titulo = $(this).attr('data-emp-dev-titulo');
			    let aluno = $(this).attr('data-emp-dev-aluno');
			    let devolucao = $(this).attr('data-emp-dev-dataDev');
			    let retirada = $(this).attr('data-emp-dev-dataRet');

			    $("#valor").text(""+valor);
			    $("#retirada").text(""+retirada);
			    $("#devolucao").text(""+devolucao);
			    $("#titulo").text(""+titulo);
			    $("#alunoNome").text(""+aluno);   

			    document.getElementById('idEmp').value = id;
			     
			});
			
			</script>
			
			<input name="id" id="idEmp" type="hidden" class="form-control"/>
			
			<div>
	        <div>
	          <h2>UNIVERSIDADE DO ALTO BODOCONG�</h2>
	          <div>Campina Grande - PB</div>
	          <div ><a href="mailto:UFAB@example.com">UFAB@example.com</a></div>
	        </div>
	        <div>
	          <h3>Guia de Empr�stimo </h1>
	          <div>Data de Retirada: <span id="retirada"></span></div>
	          <div>Data de Prevista de Devolu��o: <span id="devolucao"></span></div>
	          <div>Aluno : R$ <span id="alunoNome"></span></div>
	        </div>
	      </div>
	      <br><br>
	      
	      <div class="panel">
	
		    <div>
	          <div>#: 01</div>
	           <div>T�TULO: <span id="titulo"></span></div>
	           <div>MULTA : R$ <span id="valor"></span></div>
	      	</div>
	      </div>
	      
	       <br><br>
	     
	      <div>
	        <div id="thanks">Obrigado e Volte Sempre!</div>
	      </div>
 						
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Devolver e quitar d�bitos" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>
  	
  	
  	
   	
  		