<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_funcionario.jspf"%>

	<div class="container">
		<H1>Emprestimos</H1>
		
		<div class="alert alert-info">
 			<p> <b>Info:</b> S� � poss�vel renovar um espr�stimo se n�o houver <u>reserva</u> para o item .</p> 
		</div>
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		<p>
			<font color="green">${successMessage}</font>
		</p>

		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>Data Prevista de Devolucao</th>
				<th>Data de Retirada</th>
				<th>C�d. do emprestimo</th>
				<th>Aluno</th>
				<th>Tipo de Item</th>
				<th>Nome do Item</th>
				<th>Foi Entregue ?</th>
				<th>Pendencia</th>
				<th>A��es</th>
			</thead>
			<tbody>

			<c:forEach items="${emprestimos}" var="emprestimo">
				<tr>
					<c:set var = "dataDevolucao" value = "${emprestimo.dataDevolucao}" />
					<td>${dataDevolucao}</td>
					
					<c:set var = "dataRetirada" value = "${emprestimo.dataRetirada}" />	
					<td>${dataRetirada}</td>
					
					<td># ${emprestimo.id}</td>	
					
					<c:forEach items="${alunos}" var="aluno">
						<c:if test="${emprestimo.alunoId == aluno.id}">
							<c:set var = "alunoNome" value = "${aluno.nome}" />
					      	<td>${alunoNome}</td>
					   	</c:if>
					</c:forEach>
					
					<c:forEach items="${tipos}" var="tipo">
						<c:if test="${emprestimo.tipoItem == tipo[0]}">
					      <td>${tipo[1]}</td>
					      
					      	<c:if test="${tipo[0] == 1}">
				    	 	  <c:forEach items="${livros}" var="livro">
								<c:if test="${emprestimo.itemId == livro.id}">
									<c:set var = "titulo" value = "${livro.titulo}" />
							      </c:if>
						      </c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 2}">
					   			<c:forEach items="${revistas}" var="revista">
									<c:if test="${emprestimo.itemId == revista.id}">
								      	<c:set var = "titulo" value = "${revista.titulo}" />
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 3}">
					    	 	<c:forEach items="${jornais}" var="jornal">
									<c:if test="${emprestimo.itemId == jornal.id}">
								      	<c:set var = "titulo" value = "${jornal.titulo}" />
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 4}">
					    	 	<c:forEach items="${anais}" var="anal">
									<c:if test="${emprestimo.itemId == anal.id}">
								      	<c:set var = "titulo" value = "${anal.titulo}" />
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 5}">
					    	 	<c:forEach items="${tccs}" var="tcc">
									<c:if test="${emprestimo.itemId == tcc.id}">
								      	<c:set var = "titulo" value = "${tcc.titulo}" />
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 6}">
					    	 	<c:forEach items="${midias}" var="midia">
									<c:if test="${emprestimo.itemId == midia.id}">
								      	<c:set var = "titulo" value = "${midia.titulo}" />
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<td>${titulo}</td>
					   	</c:if>
					   	
					</c:forEach>
				
					<c:if test="${emprestimo.entregue == 0}">
						<td bgcolor="#FF0000">N�o</td>
					</c:if>
					<c:if test="${emprestimo.entregue == 1}">
						<td bgcolor="#00FF00">Sim</td>
					</c:if>
					
					<c:forEach items="${pendencias}" var="pendencia">
									
						<c:if test="${emprestimo.id == pendencia[0]}">										
							<c:if test="${pendencia[1] > 0}">
							 	<c:set var = "valor" value = "${pendencia[1] * 0.50 }" />
					     	</c:if>	
					     	<td>R$ ${valor > 0 ? valor : 0.00 }</td>
					   	</c:if>
					   	
					   	
					</c:forEach>
					
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					    <ul class="dropdown-menu">
												
							<c:if test="${emprestimo.entregue == 0}">
							<li>
								<a class="btn devolver-emprestimo" data-toggle="modal" data-target="#modalDevolucao"
								   data-emp-dev-id = "${emprestimo.id}"
								   data-emp-dev-valor = "${valor}"
								   data-emp-dev-titulo = "${titulo}"
								   data-emp-dev-aluno = "${alunoNome}"
								   data-emp-dev-dataDev = "${dataDevolucao}"
								   data-emp-dev-dataRet = "${dataRetirada}"
								>
									Devolver
								</a>
							</li>
							<li class="divider"></li>
							</c:if>
							
							<c:if test="${emprestimo.entregue == 0 && empty valor }">
							<li>
								<a class="btn renovar-emprestimo" data-toggle="modal" data-target="#modalRenovacao"
								   data-emp-ren-id = "${emprestimo.id}"
								   data-emp-ren-titulo = "${titulo}"
								   data-emp-ren-aluno = "${alunoNome}"
								>
									Renovar
								</a>
							</li>
							<li class="divider"></li>
							</c:if>
					
							<li>
								<a class="btn" onclick="return confirm('Confirmar exclus�o?')" 
									href="/emprestimos/delete/${emprestimo.id}">
									Deletar
								</a>
							</li>
					    </ul>
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	
		</div>
		
		
<%@ include file="devolver.jsp"%>
<%@ include file="renovar.jsp"%>
		
	
<%@ include file="../common/footer.jspf"%>