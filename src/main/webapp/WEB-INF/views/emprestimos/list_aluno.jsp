<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_aluno.jspf"%>

	<div class="container">
		<H1>Meus Emprestimos</H1>
		
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		<p>
			<font color="green">${successMessage}</font>
		</p>

		
		<div class="panel panel-body">
				<table class="table table-striped" id="dataTable">
			<thead>
				<th>Data Prevista de Devolucao</th>
				<th>Data de Retirada</th>
				<th>C�d. do emprestimo</th>
				<th>Aluno</th>
				<th>Tipo de Item</th>
				<th>Nome do Item</th>
				<th>Foi Entregue ?</th>
				<th>Pendencia</th>
				<th>A��es</th>
			</thead>
			<tbody>

			<c:forEach items="${emprestimos}" var="emprestimo">
				<tr>
					<td>${emprestimo.dataDevolucao}</td>	
					<td>${emprestimo.dataRetirada}</td>
					<td># ${emprestimo.id}</td>	
					<td>${aluno.nome}</td>
					
					
					<c:forEach items="${tipos}" var="tipo">
						<c:if test="${emprestimo.tipoItem == tipo[0]}">
					      <td>${tipo[1]}</td>
					      
					      	<c:if test="${tipo[0] == 1}">
				    	 	  <c:forEach items="${livros}" var="livro">
								<c:if test="${emprestimo.itemId == livro.id}">
							      <td>${livro.titulo}</td>
							      </c:if>
						      </c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 2}">
					   			<c:forEach items="${revistas}" var="revista">
									<c:if test="${emprestimo.itemId == revista.id}">
								      	<td>${revista.titulo}</td>
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 3}">
					    	 	<c:forEach items="${jornais}" var="jornal">
									<c:if test="${emprestimo.itemId == jornal.id}">
								      	<td>${jornal.titulo}</td>
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 4}">
					    	 	<c:forEach items="${anais}" var="anal">
									<c:if test="${emprestimo.itemId == anal.id}">
								      	<td>${anal.titulo}</td>
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 5}">
					    	 	<c:forEach items="${tccs}" var="tcc">
									<c:if test="${emprestimo.itemId == tcc.id}">
								      	<td>${tcc.titulo}</td>
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   		
					   		<c:if test="${tipo[0] == 6}">
					    	 	<c:forEach items="${midias}" var="midia">
									<c:if test="${emprestimo.itemId == midia.id}">
								      	<td>${midia.titulo}</td>
								    </c:if>
						      	</c:forEach>
					   		</c:if>
					   	</c:if>
					</c:forEach>
				
					<td>${emprestimo.entregue == 1 ? 'Sim' : 'N�o'}</td>
					
					<c:forEach items="${pendencias}" var="pendencia">
									
						<c:if test="${emprestimo.id == pendencia[0]}">										
							<c:if test="${pendencia[1] > 0}">
							 	<c:set var = "valor" value = "${pendencia[1] * 0.50 }" />
					     	</c:if>	
					   	</c:if>
					   	
					   	<td>R$ ${valor > 0 ? valor : 0.00 }</td>
					</c:forEach>
					
					<td>
					<ul class="nav nav-tabs">
					  <li role="presentation" class="dropdown">
					    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
					      <i class="fa fa-navicon"></i>
					    </a>
					    
					  </li>
					</ul>
						
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		
		</div>
		
	
<%@ include file="../common/footer.jspf"%>