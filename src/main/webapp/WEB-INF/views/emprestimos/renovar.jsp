<div id="modalRenovacao" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/emprestimos/renovar" commandName="emprestimo">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de Emprestimo</h4>
		      </div>
		      <div class="modal-body">
		      
		      <script type="text/javascript">
			$('.renovar-emprestimo').on('click', function () {
			    let id = $(this).attr('data-emp-ren-id');
			    let aluno = $(this).attr('data-emp-ren-aluno');
			    let titulo = $(this).attr('data-emp-ren-titulo');

			    console.log(id);
			    console.log(aluno);
			    console.log(titulo);
			   
			    document.getElementById('idEmp').value = id;
			    document.getElementById('aluno').value = aluno;
			    document.getElementById('title').value = titulo;
				   
			});
			
			</script>
			
					<input name="id" id="idEmp" type="hidden" class="form-control"/>
				
					<fieldset class="form-group">
						<label>T�tulo</label>
						<input id="title" type="text" class="form-control" disabled/>
					</fieldset>
					
					<fieldset>
						<label>Aluno</label>
						 <input id="aluno" type="text" class="form-control" disabled/>
					</fieldset><br>
					
					<fieldset>
						<label>Dias de Renova��o</label>
						   <select class="form-control" name="dias" required>
							   <%					       
						        for(int i=1; i<= 30; i++) {   
						        	%><option value="<%=i %>"><%=i%></option><%
				        		} 
				        		%> 
					    	</select>
					</fieldset><br>
										
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Renovar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>