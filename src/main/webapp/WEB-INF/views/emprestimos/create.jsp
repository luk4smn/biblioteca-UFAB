	<div id="modalEmprestimo" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
		
		<p>
			<font color="red">${errorMessage}</font>
		</p>
	
		<form:form method="POST" action="/emprestimos/create" commandName="emprestimo">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Cadastro de Emprestimo</h4>
		      </div>
		      <div class="modal-body">
		      
		      <script type="text/javascript">
			$('.create-emprestimo').on('click', function () {
			    let itemId = $(this).attr('data-itemId');
			    let nome = $(this).attr('data-nome');
			    
			    document.getElementById('itemId').value = itemId;
			    document.getElementById('nome').value = nome;
				   
			});
			
			</script>
			
					<input name="itemId" id="itemId" type="hidden" class="form-control"/>
					<input name="tipoItem" value="${tipoItemAcervo}" type="hidden" class="form-control"/>
				
					<fieldset class="form-group">
						<label>T�tulo</label>
						<input id="nome" type="text" class="form-control" disabled/>
					</fieldset>
					
					<fieldset>
						<label>Aluno</label>
						  <select class="form-control" name="alunoId" required>
						   	 <c:forEach items="${alunos}" var="aluno">
								<option value="${aluno.id}">${aluno.nome}</option>
							</c:forEach>
						  </select>
					</fieldset><br>
					
					<fieldset>
						<label>Dias de Reserva</label>
						   <select class="form-control" name="dias" required>
							   <%					       
						        for(int i=1; i<= 30; i++) {   
						        	%><option value="<%=i %>"><%=i%></option><%
				        		} 
				        		%> 
					    	</select>
					</fieldset><br>
										
									
			</div>
			
			<div class="modal-footer">
			<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      	</div>
	      	
	     	</div>
	     	
		</form:form>
	      
	    </div>
	
  	</div>