<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_admin.jspf"%>

	
	
	<div class="container">
	
	<h1>Atualiza��o de Alunos</h1>
		
		<form:form method="POST" action="/alunos/update" commandName="aluno">
		
			<input name="id" type="hidden" value="${aluno.id}" class="form-control"/>
		
			<fieldset class="form-group">
				<label>Matr�cula</label>
				<input name="matricula" type="text" value="${aluno.matricula}" class="form-control" readonly/>
			</fieldset>
			<fieldset class="form-group">
				<label>CPF</label>
				<input name="cpf" type="text" value="${aluno.cpf}" class="form-control"/>
			</fieldset>
			<fieldset class="form-group">
				<label>RG</label>
				<input name="rg" type="text" value="${aluno.rg}" class="form-control"/>
			</fieldset>  
			<fieldset class="form-group">
				<label>Naturalidade</label>
				<input name="naturalidade" value="${aluno.naturalidade}" type="text" class="form-control"/>
			</fieldset> 
			<fieldset class="form-group">
				<label>Nome Completo</label>
				<input name="nome" type="text" value="${aluno.nome}" class="form-control"/>
			</fieldset> 
			<fieldset class="form-group">
				<label>Nome da M�e</label>
				<input name="nomemae" type="text" value="${aluno.nomemae}" class="form-control"/>
			</fieldset> 
			<fieldset class="form-group">
				<label>Endere�o</label>
				<input name="endereco" type="text" value="${aluno.endereco}" class="form-control"/>
			</fieldset> 
			<fieldset class="form-group">
				<label>Telefone</label>
				<input name="telefone" type="text" value="${aluno.telefone}" class="form-control"/>
			</fieldset>
			<fieldset class="form-group">
					<label>Email</label>
					<input name="email" type="text" value="${aluno.email}"  class="form-control" required/>
			</fieldset> 
			
			<fieldset>
				<label>Curso</label>
				  <select class="form-control" id="select_aluno" name="curso" required>
			
				  		<c:forEach items="${cursos}" var="curso">
				  		<option value="${curso.nome} ${curso.nome == aluno.curso ? 'selected' : ''}">${curso.nome}</option>
					  	
					  	</c:forEach>
				  </select>
			</fieldset><br>
			
			<fieldset>
				<label for="select_nivel">Tipo do Curso</label>
				  <select class="form-control" id="select_nivel" name="nivel" requied>
				  		<option value="G" ${aluno.nivel == 'G' ? 'selected' : ''}>Gradua��o</option>
					  	<option value="E" ${aluno.nivel == 'E' ? 'selected' : ''}>Especializa��o</option>
					  	<option value="M" ${aluno.nivel == 'M' ? 'selected' : ''}>Mestrado</option>
					  	<option value="D" ${aluno.nivel == 'D' ? 'selected' : ''}>Doutorado</option>
				  </select>
			</fieldset><br>
			
			<fieldset class="form-group">
				<label>Ano de Ingresso</label>
				<input name="ano" type="number" value="${aluno.ano}" class="form-control"/>
			</fieldset> 
			<fieldset class="form-group">
				<label>Per�odo</label>
				<input name="periodo" type="number" value="${aluno.periodo}" class="form-control"/>
			</fieldset> 
			
			<input name="edit" type="submit" value="Submit" class="btn btn-success"/>
			<p></p>
		</form:form>
	</div>

<%@ include file="../common/footer.jspf"%>