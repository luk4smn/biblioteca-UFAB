<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_aluno.jspf"%>

	<div class="container">
		<H1>Meus dados</H1>
		
		<table class="table table-striped">
			<thead>
				<th>Matr�cula</th>
				<th>CPF</th>
				<th>RG</th>
				<th>Naturalidade</th>
				<th>Nome</th>
				<th>Nome da M�e</th>
				<th>Endere�o</th>
				<th>Telefone</th>
				<th>Curso</th>
				<th>Nivel</th>
				<th>Ano</th>
				<th>Periodo</th>
				<th>A��es</th>
			</thead>
			<tbody>
				<tr>
					<td>${aluno.matricula}</td>
					<td>${aluno.cpf}</td>
					<td>${aluno.rg}</td>
					<td>${aluno.naturalidade}</td>
					<td>${aluno.nome}</td>
					<td>${aluno.nomemae}</td>
					<td>${aluno.endereco}</td>
					<td>${aluno.telefone}</td>
					<td>${aluno.curso}</td>
					<td>${aluno.nivel}</td>
					<td>${aluno.ano}</td>
					<td>${aluno.periodo}</td>
					<td>
						<a class="btn btn-warning"
							href="/alunos/edit/${aluno.id}">
							Editar
						</a>
					</td>
				</tr>
			</tbody>
		</table>

		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
	</div>
	
<%@ include file="../common/footer.jspf"%>