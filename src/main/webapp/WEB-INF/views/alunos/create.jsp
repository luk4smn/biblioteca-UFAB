	<div id="modalAluno" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
	
	
	<form:form method="POST" action="/alunos/create" commandName="aluno">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Cadastro de Alunos</h4>
	      </div>
	      <div class="modal-body">
	       
		
			
				<fieldset class="form-group">
					<label>Matr�cula</label>
					<input name="matricula" type="text" class="form-control" disabled/>
				</fieldset>
				<fieldset class="form-group">
					<label>CPF</label>
					<input name="cpf" type="text" class="form-control" required/>
				</fieldset>
				<fieldset class="form-group">
					<label>RG</label>
					<input name="rg" type="text" class="form-control" required/>
				</fieldset>  
				<fieldset class="form-group">
					<label>Naturalidade</label>
					<input name="naturalidade" type="text" class="form-control" required/>
				</fieldset> 
				<fieldset class="form-group">
					<label>Nome Completo</label>
					<input name="nome" type="text" class="form-control" required/>
				</fieldset> 
				<fieldset class="form-group">
					<label>Nome da M�e</label>
					<input name="nomemae" type="text" class="form-control" required/>
				</fieldset> 
				<fieldset class="form-group">
					<label>Endere�o</label>
					<input name="endereco" type="text" class="form-control" required/>
				</fieldset> 
				<fieldset class="form-group">
					<label>Telefone</label>
					<input name="telefone" type="text" class="form-control" required/>
				</fieldset> 
				
				<fieldset class="form-group">
					<label>Email</label>
					<input name="email" type="text" class="form-control" required/>
				</fieldset> 
				
				<fieldset>
					<label>Curso</label>
					  <select class="form-control" id="select_aluno" name="curso" required>
					  		<c:forEach items="${cursos}" var="curso">
					  	
					   		<option value="${curso.nome}">${curso.nome}</option>
						  	
						  	</c:forEach>
					  </select>
				</fieldset><br>
				
				<fieldset>
					<label for="select_nivel">N�vel do Curso</label>
					  <select class="form-control" id="select_nivel" name="nivel" required>
					   	  <option value="G">Gradua��o</option>
						  <option value="E">Especializa��o</option>
						  <option value="M">Mestrado</option>
						  <option value="D">Doutorado</option>
					  </select>
				</fieldset><br>
				
				<fieldset class="form-group">
					<label>Ano de Ingresso</label>
					<input name="ano" type="text" class="form-control" required/>
				</fieldset> 
				<fieldset class="form-group">
					<label>Per�odo</label>
					<input name="periodo" type="text" class="form-control" required/>
				</fieldset> 
			
		</div>
		
		<div class="modal-footer">
		<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      	</div>
      	
     	</div>
     	
	</form:form>
	      
	    </div>
	
  	</div>