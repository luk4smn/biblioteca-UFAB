<div id="modalUsuario" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

	<form:form method="post"  action="/users/create" commandName="usuario">
	
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Cadastro de Usu�rios</h4>
	      </div>
		      <div class="modal-body">
		       
		       	<p>
					<font color="red">${errorMessage}</font>
				</p>
				
				<div class="panel panel-body">
					
					<fieldset class="form-group">
						<label>Usuario</label>
						<input name="usuario" type="text" class="form-control" required/>
					</fieldset> 
					
					<fieldset class="form-group">
						<label>Senha</label>
						<input name="senha" type="password" class="form-control" required/>
					</fieldset>
					
					<fieldset>
						<label for="select_role">N�vel de acesso</label>
						  <select class="form-control" id="select_role" name="role" onchange="processarTipo();" required>
						  		<option>SELECIONE UMA OP��O</option>
						   		<option value="1">ADMINISTRADOR</option>
							  	<option value="2">FUNCION�RIO</option>
							  	<option value="3">ALUNO</option>
						  </select>
					</fieldset><br>
		
					<fieldset id="select_aluno" style="display:none">
						<label>Alunos sem perfis de usu�rio</label>
						  <select class="form-control" name="aluno">
						  		<option value = 0>Selecione um Aluno </option>
						  		<c:forEach items="${alunos}" var="aluno">
						  	
						   		<option value="${aluno.id}">${aluno.nome}</option>
							  	
							  	</c:forEach>
						  </select>
					</fieldset><br>
					
					<fieldset id="select_funcionario" style="display:none">
						<label>Funcion�rios sem perfis de usu�rio</label>
						  <select class="form-control"  name="funcionario" >
						  		<option  value = 0>Selecione um Funcion�rio</option>
						  		<c:forEach items="${funcionarios}" var="funcionario">
						  	
						   		<option value="${funcionario.id}">${funcionario.nome}</option>
							  	
							  	</c:forEach>
						  </select>
					</fieldset>
				</div>
					
				</div>
		
			<div class="modal-footer">
				<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		   	</div>
	     	
	   	</div>
   	
   		</form:form>
      
    </div>

</div>

<script type="text/javascript">
		function processarTipo(){
			var tipo = document.getElementById("select_role").value;
			
			if(tipo == 3){
				$('#select_aluno').show();
				$('#select_funcionario').hide();
			}		
			else{
				$('#select_aluno').hide();
				$('#select_funcionario').show();
			}
			
			console.log(tipo);
		}
	
</script>


