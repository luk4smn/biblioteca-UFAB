<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_admin.jspf"%>

	<div class="container">
		<h1>Atualiza��o de Usu�rios</h1>
		
		<form:form method="POST" action="/users/update" commandName="usuario">
			
			<input name="dadoDeReferenciaId" type="hidden" value="${usuario.dadoDeReferenciaId}" class="form-control"/>
			<input name="id" type="hidden" value="${usuario.id}" class="form-control"/>
				
			<fieldset class="form-group">
				<label>Usu�rio</label>
				<input name="usuario" type="text" value="${usuario.usuario}" class="form-control" readonly/>
			</fieldset> 
			
			<fieldset class="form-group">
				<label>Senha</label>
				<input name="senha" type="password" value="${usuario.senha}" class="form-control" required/>			
			</fieldset>
			
			<fieldset>
				<label for="select_role">N�vel de acesso</label>
				  <select class="form-control" id="select_role" name="role" required">
				   	  <option value="1" ${usuario.role == '1' ? 'selected' : ''}>ADMINISTRADOR</option>
					  <option value="2" ${usuario.role == '2' ? 'selected' : ''}>FUNCION�RIO</option>
					  <option value="3" ${usuario.role == '3' ? 'selected' : ''}>ALUNO</option>
				  </select>
			</fieldset><br>
			 
			<input name="edit" type="submit" value="Submit" class="btn btn-success"/>
			<p></p>
		</form:form>
	</div>

<%@ include file="../common/footer.jspf"%>