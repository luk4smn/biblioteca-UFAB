<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_aluno.jspf"%>

	<div class="container">
		<H1>Dados do usu�rio</H1>
		
		<table class="table table-striped">
	
			<thead>
				<th>#</th>
				<th>Usu�rio</th>
				<th>Senha</th>
				<th>Perfil de Acesso</th>
				<th># ID de referencia</th>
				<th>A��es</th>
			</thead>
			<tbody>
				<tr>
					<td>${usuario.id}</td>
					<td>${usuario.usuario}</td>
					<td>${usuario.senha}</td>
					<td>${usuario.role  == 3 ? "Aluno" : ''}</td>
					<td>${usuario.dadoDeReferenciaId}</td>
					<td>
						<a class="btn btn-warning"
							href="edit/${usuario.id}">Editar
						</a>
					</td> 
				</tr>
			</tbody>
		</table>

		<p>
			<font color="red">${errorMessage}</font>
		</p>
	</div>
	
<%@ include file="../common/footer.jspf"%>