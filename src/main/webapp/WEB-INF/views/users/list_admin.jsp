<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_admin.jspf"%>

	<div class="container">
		<H1>Usu�rios do Sistema</H1>
		
		<div class="panel panel-body">
			<table class="table table-striped">
			<thead>
				<th>#</th>
				<th>Usu�rio</th>
				<th>Senha</th>
				<th>Perfil de Acesso</th>
				<th># ID de referencia</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${usuarios}" var="usuario">
				<tr>
					<td>${usuario.id}</td>
					<td>${usuario.usuario}</td>
					<td>${usuario.senha}</td>
					<td>${usuario.role  == 3 ? "Aluno" : usuario.role  == 2 ? "Funcion�rio" : "Administrador"}</td>
					<td>${usuario.dadoDeReferenciaId}</td>
					<td>
						<a class="btn btn-warning"
							href="edit/${usuario.id}">Editar
						</a>
					</td> 
					<td>
					<a class="btn btn-danger" onclick="return confirm('Confirmar exclus�o?')"  
						href="delete/${usuario.id}">Delete
					</a>
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
			
		</div>
	
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
		
		<button class="btn btn-primary" data-toggle="modal" data-target="#modalUsuario"><i class="fa fa-plus-circle"></i> Adicionar Novo Usu�rio</button>
		
	</div>
	
	<!-- Modal usu�rio -->
	<%@ include file="create.jsp" %>
	
<%@ include file="../common/footer.jspf"%>