<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_aluno.jspf"%>

	<div class="container">
	
		<h1>Edite seus dados de login</h1>
	
		<form:form method="POST" action="/users/update" commandName="usuario">
			<div class="panel panel-body">
				<input name="dadoDeReferenciaId" type="hidden" value="${usuario.dadoDeReferenciaId}" class="form-control"/>
				<input name="role" type="hidden" value="${usuario.role}" class="form-control"/>
				<input name="id" type="hidden" value="${usuario.id}" class="form-control"/>
				
				<fieldset class="form-group">
					<label>Usu�rio</label>
					<input name="usuario" type="text" value="${usuario.usuario}" class="form-control" readonly/>
				</fieldset> 
				
				<fieldset class="form-group">
					<label>Senha</label>
					<input name="senha" type="password" value="${usuario.senha}" class="form-control"/>				
				</fieldset> 
				<input name="edit" type="submit" value="Submit" class="btn btn-success"/>
			</div>
			
			<p></p>
		</form:form>
	</div>

<%@ include file="../common/footer.jspf"%>