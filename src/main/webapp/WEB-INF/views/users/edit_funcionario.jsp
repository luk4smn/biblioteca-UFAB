<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_funcionario.jspf"%>

	<div class="container">
		<h1>Atualiza��o de Usu�rios</h1>
		
		<form:form method="POST" action="/users/update" commandName="usuario">
			
			<input name="dadoDeReferenciaId" type="hidden" value="${usuario.dadoDeReferenciaId}" class="form-control"/>
			<input name="id" type="hidden" value="${usuario.id}" class="form-control"/>
			<input name="role" type="hidden" value="${usuario.role}" class="form-control"/>
				
			<fieldset class="form-group">
				<label>Usu�rio</label>
				<input name="usuario" type="text" value="${usuario.usuario}" class="form-control" readonly/>
			</fieldset> 
			
			<fieldset class="form-group">
				<label>Senha</label>
				<input name="senha" type="password" value="${usuario.senha}" class="form-control"/>			
			</fieldset>			
			
			<fieldset>
				<label>N�vel de acesso</label>
					<input type="text" 
					value="${usuario.role  == 3 ? "ALUNO" :
						usuario.role  == 2 ? "FUNCION�RIO" :
					  	"ADMINISTRADOR"}" class="form-control" readonly/>
			</fieldset><br>
			 
			<input name="edit" type="submit" value="Submit" class="btn btn-success"/>
			<p></p>
		</form:form>
	</div>

<%@ include file="../common/footer.jspf"%>