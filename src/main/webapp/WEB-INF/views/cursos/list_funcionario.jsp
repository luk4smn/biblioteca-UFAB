<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_funcionario.jspf"%>

	<div class="container">
		<H1>Cursos da Institu��o</H1>
		
		<div class="panel panel-body">
			<table class="table table-striped">
			<thead>
				<th>#</th>
				<th>Nome</th>
				<th>�rea</th>
				<th>Tipo</th>
			</thead>
			<tbody>
			<c:forEach items="${cursos}" var="curso">
				<tr>
					<td>${curso.id}</td>
					<td>${curso.nome}</td>
					<td>${curso.area}</td>
					<td>${curso.tipo}</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
			
		</div>
	
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
	</div>

	<!-- Modal Curso -->
	<%@ include file="create.jsp" %>
	
<%@ include file="../common/footer.jspf"%>