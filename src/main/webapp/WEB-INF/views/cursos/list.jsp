<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_admin.jspf"%>

	<div class="container">
		<H1>Cursos da Institu��o</H1>
		
		<div class="panel panel-body">
			<table class="table table-striped">
			<thead>
				<th>#</th>
				<th>Nome</th>
				<th>�rea</th>
				<th>Tipo</th>
				<th>A��es</th>
			</thead>
			<tbody>
			<c:forEach items="${cursos}" var="curso">
				<tr>
					<td>${curso.id}</td>
					<td>${curso.nome}</td>
					<td>${curso.area}</td>
					<td>${curso.tipo}</td>
					<td>
						<a class="btn btn-warning"
							href="/cursos/edit/${curso.id}">
							Editar
						</a>
					</td> 
					<td>
					<a class="btn btn-danger" onclick="return confirm('Confirmar exclus�o?')"  
						href="/cursos/delete/${curso.id}">Delete
					</a>
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
			
		</div>
	
		<p>
			<font color="red">${errorMessage}</font>
		</p>
		
			<button class="btn btn-primary" data-toggle="modal" data-target="#modalCurso"><i class="fa fa-plus-circle"></i> Adicionar Novo Curso</button>
		
	</div>

	<!-- Modal Curso -->
	<%@ include file="create.jsp" %>
	
<%@ include file="../common/footer.jspf"%>