<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navigation_funcionario.jspf"%>

	
	
	<div class="container">
	
	<h1>Atualiza��o de Curso</h1>
		
		<form:form method="POST" action="/cursos/update" commandName="curso">
		
			<input name="id" type="hidden" value="${curso.id}" class="form-control"/>
		
			<fieldset class="form-group">
				<label>Nome</label>
				<input name="nome" type="text" value="${curso.nome}" class="form-control" readonly/>
			</fieldset>
			
			<fieldset class="form-group">
				<label>�rea</label>
				<input name="area" type="text" value="${curso.area}" class="form-control"/>
			</fieldset>
			
			<fieldset>
				<label >Tipo</label>
				  <select class="form-control" name="tipo" required>
				  		<option value="Gradua��o" ${curso.tipo == 'Gradua��o' ? 'selected' : ''}>Gradua��o</option>
					  	<option value="P�s-Gradua��o" ${curso.tipo == 'P�s-Gradua��o' ? 'selected' : ''}>P�s-Gradua��o</option>
				  </select>
			</fieldset><br>
			
			<input name="edit" type="submit" value="Submit" class="btn btn-success"/>
			<p></p>
		</form:form>
	</div> 

<%@ include file="../common/footer.jspf"%>