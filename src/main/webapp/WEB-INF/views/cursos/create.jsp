
<div id="modalCurso" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

	<form:form method="POST" action="/cursos/create" commandName="curso">
	
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Cadastro de Cursos</h4>
	      </div>
		      <div class="modal-body">
		       
		       	<p>
					<font color="red">${errorMessage}</font>
				</p>
				
				<div class="panel panel-body">
					
					<fieldset class="form-group">
						<label>Nome</label>
						<input name="nome" type="text" class="form-control" required/>
					</fieldset> 
					
					<fieldset class="form-group">
						<label>�rea</label>
						<input name="area" type="text" class="form-control" required/>
					</fieldset>
					
					<fieldset id="select_aluno">
						<label>Tipo</label>
						  <select class="form-control" name="tipo">
						   		<option value="Gradua��o">Gradua��o</option>
						   		<option value="P�s-Gradua��o">P�s-Gradua��o</option>
						  </select>
					</fieldset><br>
				</div>
				
			</div>
		
			<div class="modal-footer">
				<input name="add" type="submit" value="Cadastrar" class="btn btn-success"/>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		   	</div>
	     	
	   	</div>
   	
   	</form:form>
      
    </div>

</div>